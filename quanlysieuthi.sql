-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 24, 2020 lúc 04:49 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quanlysieuthi`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Điện tử', 'dien-tu', 'tất cả các loại đồ điện tử', '2017-08-02 06:58:16', NULL),
(2, NULL, 'Gia dụng', 'trai-cay', 'gia-dung', '2017-08-02 06:58:16', NULL),
(3, 1, 'Ti vi', 'ti-vi', 'Các hãng ti vi', '2017-08-02 06:58:25', NULL),
(4, 1, 'Máy tính', 'may-tinh', 'Các loại máy tính\r\n', '2017-08-02 06:58:25', NULL),
(5, 1, 'Điện thoại', 'dien-thoai', 'Các hãng điện thoại', '2017-08-02 06:58:25', NULL),
(6, 1, 'Máy tính linh kiện', 'linh-kien', 'Các loại linh kiện - phụ kiện cho các máy tính và điện thoại', '2017-08-02 06:58:25', NULL),
(7, 1, 'Tablet', 'tablet', 'Tablet là máy tính bảng được ra đời những năm 2008-2009 nhằm chỉ những thiết bị di động có màn hình cảm ứng từ 7 inch trở lên có chức năng như một chiếc điện thoại cộng với một chiếc máy tính.', '2017-08-02 06:58:25', NULL),
(8, 1, 'Tủ lạnh', 'tu-lanh', 'Tủ lạnh các loại phù hợp với mọi điều kiện gia đình', '2017-08-02 06:58:25', NULL),
(9, 1, 'Máy giặt', 'may-giat', 'Các hãng máy giặt nổi tiếng', '2017-08-02 06:58:25', NULL),
(11, 2, 'Quạt', 'quat', NULL, '2017-09-21 02:24:21', '2017-09-21 02:24:21'),
(12, 2, 'Lò vi sóng', 'lo-vi-song', NULL, '2017-09-21 02:46:46', '2017-09-21 02:46:46');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `product_id`, `content`, `created_at`, `updated_at`) VALUES
(7, 1, 62, 'Demo Comment Real Time 01: Trông Ngon Thế Nhỉ?', '2017-09-27 01:15:55', '2017-09-27 01:15:55'),
(8, 2, 62, 'Demo Comment Real Time 02: Chưa chắc chất lượng đã được như hình đâu Admin ơi!!!', '2017-09-27 01:17:53', '2017-09-27 01:17:53'),
(10, 3, 62, 'Admin với Author đẹp trai thế (love)', '2017-09-27 01:19:59', '2017-09-27 01:19:59'),
(12, 2, 62, 'abcde', '2017-11-16 04:55:27', '2017-11-16 04:55:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(71, '2014_10_12_000000_create_users_table', 1),
(72, '2014_10_12_100000_create_password_resets_table', 1),
(73, '2017_08_01_134824_create_slides_table', 1),
(74, '2017_08_01_135146_create_news_table', 1),
(75, '2017_08_01_135410_create_categories_table', 1),
(76, '2017_08_01_135956_create_products_table', 1),
(77, '2017_08_01_140433_create_orders_table', 1),
(78, '2017_08_01_141439_create_order_product_table', 1),
(79, '2017_08_01_142625_create_comments_table', 1),
(80, '2017_08_25_234837_edit1_categories_table', 2),
(81, '2017_08_25_234857_edit1_products_table', 2),
(82, '2017_08_25_234905_edit1_news_table', 2),
(83, '2017_08_25_234917_edit1_orders_table', 2),
(84, '2017_08_29_141625_edit2_slides_table', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `user_id`, `title`, `slug`, `content`, `image`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 'Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony', 'cach-kich-hoat-goi-film', '<p><strong>Từ ng&agrave;y 1/11 - 3/12/2017 người d&ugrave;ng mua Android tivi v&agrave; Internet Tivi Sony tại Điện m&aacute;y Trần Anh sẽ được tặng g&oacute;i xem Fim+ miễn ph&iacute; trong 12 th&aacute;ng. Để nhận ưu đ&atilde;i n&agrave;y, bạn h&atilde;y k&iacute;ch hoạt t&agrave;i khoản Fim+ tr&ecirc;n tivi qua b&agrave;i hướng dẫn sau đ&acirc;y.</strong></p>\r\n\r\n<p><span style=\"color:rgb(85, 85, 85); font-family:sans-serif; font-size:14px\">Từ ng&agrave;y 1/11 - 3/12/2017 người d&ugrave;ng mua Android tivi v&agrave; Internet Tivi Sony tại Điện m&aacute;y T&acirc;n Hường sẽ được tặng g&oacute;i xem Fim+ miễn ph&iacute; trong 12 th&aacute;ng. Để nhận ưu đ&atilde;i n&agrave;y, bạn h&atilde;y k&iacute;ch hoạt t&agrave;i khoản Fim+ tr&ecirc;n tivi qua b&agrave;i hướng dẫn sau đ&acirc;y. [dropcap]1[/dropcap] L&agrave;m sao để nhận được code Fim+ miễn ph&iacute; - Khi mua smart tivi hoặc android tivi Sony trong thời gian 1/11 - 3/12/2017 tại Điện m&aacute;y T&acirc;n Hường kh&aacute;ch h&agrave;ng sẽ được tặng 1 code xem phim miễn ph&iacute; trong v&ograve;ng 12 th&aacute;ng. - Những d&ograve;ng Android tivi Sony sẽ được t&iacute;ch hợp sẵn App Fim+ trong kho ứng dụng, những d&ograve;ng smart tivi Sony th&ocirc;ng thường (kh&ocirc;ng chạy hệ điều h&agrave;nh Android) sẽ c&oacute; bản cập nhật ứng dụng Fim+ v&agrave;o ng&agrave;y 3/11/2017. - Khi mua tivi về sau khi được k&iacute;ch hoạt bảo h&agrave;nh điện tử tự động, kh&aacute;ch h&agrave;ng sẽ nhận được một m&atilde; code ưu đ&atilde;i từ tổng đ&agrave;i Sony gửi qua SMS của số điện thoại đăng k&yacute; mua h&agrave;ng.</span></p>\r\n\r\n<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--13.jpg\" /></p>\r\n\r\n<table align=\"center\" class=\"picture\" style=\"-webkit-font-smoothing:antialiased; background-attachment:initial; background-clip:initial; background-image:initial; background-origin:initial; background-position:initial; background-repeat:initial; background-size:initial; border-collapse:collapse; border-spacing:0px; border:0px; box-sizing:border-box; color:rgb(85, 85, 85); float:none; font-family:sans-serif; font-size:14px; line-height:20px; margin:0px 0px 14px; outline:0px; padding:0px; text-rendering:geometricPrecision; text-size-adjust:100%; vertical-align:baseline; width:660px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:baseline\">\r\n			<p>G&oacute;i Fim+ tr&ecirc;n tivi Sony c&oacute; g&igrave; đặc biệt - Thư viện phim phong ph&uacute;, với h&agrave;ng ngh&igrave;n phim đủ c&aacute;c thể loại: h&agrave;nh động, h&agrave;i hước, t&igrave;nh cảm, t&acirc;m l&yacute;... - H&igrave;nh ảnh sắc n&eacute;t, c&oacute; thể xem phim chất lượng l&ecirc;n đến HD, Full HD. - Giao diện ứng dụng th&acirc;n thiện, dễ d&ugrave;ng..</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"picture\" style=\"-webkit-font-smoothing:antialiased; background-attachment:initial; background-clip:initial; background-image:initial; background-origin:initial; background-position:initial; background-repeat:initial; background-size:initial; border-collapse:collapse; border-spacing:0px; border:0px; box-sizing:border-box; color:rgb(85, 85, 85); font-family:sans-serif; font-size:14px; line-height:20px; margin:14px 0px; outline:0px; padding:0px; text-rendering:geometricPrecision; text-size-adjust:100%; vertical-align:baseline; width:660px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:baseline\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:baseline\">\r\n			<p>C&aacute;ch k&iacute;ch hoạt g&oacute;i Fim+ miễn ph&iacute; tr&ecirc;n tivi Sony</p>\r\n\r\n			<p>Bước 1: Nhấn n&uacute;t Home tr&ecirc;n remote tivi.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony-.jpg\" /></p>\r\n\r\n			<p>Bước 2: Tại giao diện trang chủ chọn v&agrave;o mục FIm+.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--1.jpg\" /></p>\r\n\r\n			<p>Bước 3: Nếu Fim+ chưa được tải về tivi, nhấn v&agrave;o c&agrave;i đặt để tải ứng dụng. Sau đ&oacute; mở ứng dụng Fim+ l&ecirc;n.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--2.jpg\" /></p>\r\n\r\n			<p>Bước 4: Tạo m&agrave;n h&igrave;nh ch&iacute;nh của Fim+ bạn nhấn ph&iacute;m điều hướng h&igrave;nh mũi t&ecirc;n l&ecirc;n tr&ecirc;n remote tivi, sau đ&oacute; di chuyển qua mục t&agrave;i khoản.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--3.jpg\" /></p>\r\n\r\n			<p>Bước 5: Nhấn v&agrave;o n&uacute;t đăng k&yacute; để đăng k&yacute; một t&agrave;i khoản xem phim.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--4.jpg\" /></p>\r\n\r\n			<p>Bước 6: Nhập v&agrave;o số điện thoại để đăng k&yacute;.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--6.jpg\" /></p>\r\n\r\n			<p>Bước 7: Sau khi nhập số điện thoại, tổng đ&agrave;i Fim+ sẽ trả về một m&atilde; x&aacute;c nhận qua tin nhắn sms, bạn nhập m&atilde; n&agrave;y để đăng k&yacute;.</p>\r\n\r\n			<p>Lưu &yacute;: Nếu kh&ocirc;ng nhận được m&atilde; x&aacute;c nhận, bạn nhấn v&agrave;o mục gửi lại m&atilde; x&aacute;c nhận tr&ecirc;n m&agrave;n h&igrave;nh.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--5.jpg\" /></p>\r\n\r\n			<p>Bước 8: Nhập mật khẩu cho t&agrave;i khoản</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--7.jpg\" /></p>\r\n\r\n			<p>Bước 9: Sau khi nhập mật khẩu xong, bạn sẽ nhận được th&ocirc;ng b&aacute;o đăng k&yacute; th&agrave;nh c&ocirc;ng. Tiếp đến bạn nhập v&agrave;o m&atilde; code ưu đ&atilde;i được nhận từ h&atilde;ng v&agrave;o.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--8.jpg\" /></p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--9.jpg\" /></p>\r\n\r\n			<p>Bước 10: Kết th&uacute;c qu&aacute; tr&igrave;nh đăng k&yacute; bạn sẽ nhận được th&ocirc;ng b&aacute;o đăng k&yacute; th&agrave;nh c&ocirc;ng.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--10.jpg\" /></p>\r\n\r\n			<p>Sau khi đăng k&yacute; th&agrave;nh c&ocirc;ng bạn c&oacute; thể thoả sức coi những bộ phim với đầy đủ thể loại trong app Fim+ rồi.</p>\r\n\r\n			<p><img alt=\"Cách kích hoạt gói Fim+ miễn phí trên Smart Tivi Sony\" src=\"https://cdn.tgdd.vn/Files/2017/10/24/1035405/cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--11.jpg\" /></p>\r\n\r\n			<p><strong>Lưu &yacute;:</strong>&nbsp;Một code ưu đ&atilde;i chỉ được đăng k&yacute; duy nhất một số điện thoại, v&agrave; sau khi đăng k&yacute; th&agrave;nh c&ocirc;ng 1 t&agrave;i khoản Fim+ sẽ đăng nhập được tr&ecirc;n 5 tivi kh&aacute;c nhau. Để xem Fim+ tr&ecirc;n những tivi kh&aacute;c, bạn chỉ cần đăng nhập lại số điện thoại v&agrave; mật khẩu đ&atilde; đăng k&yacute; l&agrave; được.</p>\r\n\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"picture\" style=\"-webkit-font-smoothing:antialiased; background-attachment:initial; background-clip:initial; background-image:initial; background-origin:initial; background-position:initial; background-repeat:initial; background-size:initial; border-collapse:collapse; border-spacing:0px; border:0px; box-sizing:border-box; color:rgb(85, 85, 85); font-family:sans-serif; font-size:14px; line-height:20px; margin:14px 0px; outline:0px; padding:0px; text-rendering:geometricPrecision; text-size-adjust:100%; vertical-align:baseline; width:660px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:baseline\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:baseline\">\r\n			<p>&nbsp;</p>\r\n			&nbsp;\r\n\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 'cach-kich-hoat-goi-fim-mien-phi-tren-smart-tivi-sony--13 (1).jpg', 2, '2017-09-19 08:13:25', '2017-11-15 09:17:04'),
(3, 2, 'Cách chọn mua máy say sinh tố cho bé đang ăn dặm?', 'cach-chon-mua-may-say-sinh-to-cho-be-dang-an-dam', '<p><strong>C&oacute; rất nhiều nhu cầu khi mua m&aacute;y say sinh tố kh&aacute;c nhau. C&oacute; thể kể đến những nhu cầu cơ bản như: Say sinh tố, say đồ kh&ocirc;, say ch&aacute;o, say thịt,say hoa quả, say bột, say đ&aacute; nhỏ&hellip; Một trong số đ&oacute; c&oacute; thể kể đến nhu cầu say đồăn dặm cho trẻ nhỏ. Th&ocirc;ng thường nếu ch&uacute;ng ta cứ sử dụng chiếc m&aacute;y say sinh tố th&ocirc;ng thường, chưa c&oacute; đủ số cối say để say đồ ăn dặm cho trẻ sẽ dễ ảnh hưởng đến tuổi thọ của m&aacute;y. Vậy để chọn mua cho đ&uacute;ng, c&aacute;c bạn cần đọc qua b&agrave;i viết dưới đ&acirc;y để c&oacute; thể hiểu ch&iacute;nh x&aacute;c v&agrave; đ&uacute;ng nhất về mục đ&iacute;ch sử dụng n&agrave;y.</strong></p>\r\n\r\n<table align=\"center\" class=\"picture\" style=\"-webkit-font-smoothing:antialiased; background-attachment:initial; background-clip:initial; background-image:initial; background-origin:initial; background-position:initial; background-repeat:initial; background-size:initial; border-collapse:collapse; border-spacing:0px; border:0px; box-sizing:border-box; color:rgb(85, 85, 85); float:none; font-family:sans-serif; font-size:14px; line-height:20px; margin:0px 0px 14px; outline:0px; padding:0px; text-rendering:geometricPrecision; text-size-adjust:100%; vertical-align:baseline; width:660px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:baseline\"><img src=\"http://img.trananh.vn/trananh/2017/09/18/c4282dd5-bf26-491f-9ac9-4cc8404d3710_may-xay-sinh-to-may-xay-thit-bluestone-blb-5335w.jpg\" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:baseline\">\r\n			<p><br />\r\n			<strong><em>&nbsp;</em>Phải chọn đ&uacute;ng số cối xay</strong><em>. Mỗi cối xay sẽ c&oacute; một t&aacute;c dụng ri&ecirc;ng. V&iacute; dụ: C&aacute;c m&aacute;y xay th&ocirc;ng thường sẽ c&oacute; hai cối bao gồm một cối xay sinh tố v&agrave; một cối xay đồ kh&ocirc;. Hai cối say n&agrave;y chưa thể say đầy đủ cho việc say đồ ăn dặm.Nếu muốn xay đồ ăn dặm th&igrave; ch&uacute;ng ta cần th&ecirc;m cối say thịt. V&igrave; sao lại như vậy?V&igrave; những cối say thịt c&oacute; lưỡi dao say chuy&ecirc;n dụng, rất sắc b&eacute;n, c&oacute; thể cắt nhỏv&agrave; say nhuyễn thịt một c&aacute;ch dễ d&agrave;ng. Nếu ch&uacute;ng ta sử dụng m&aacute;y say th&ocirc;ng thường sử dụng cối say sinh tố sẽ dẫn đến hiện tượng ch&aacute;y doăng ở dưới, rất kh&oacute; để mua v&agrave; sửa chữa, ngo&agrave;i ra khi say thịt bằng cối say sinh tố, cối say sẽ kh&oacute; rửa sạch v&agrave; nếu kh&ocirc;ng rửa kỹ trong cối say sẽ xuất hiện m&ugrave;i kh&oacute; chịu. Trường hợp c&aacute;c bạn say thịt bằng cối say đồ kh&ocirc; sẽ dẫn đến hiện tượng r&ograve; nước ra ngo&agrave;i, v&agrave; cũng dễ dẫn đến hiện tượng bị ch&aacute;y doăng cao xu b&ecirc;n trong. Ch&iacute;nh v&igrave; vậy, khi ch&uacute;ng ta mua m&aacute;y say để say đồ ăn dặm cho trẻ cầnmua m&aacute;y say c&oacute; chức năng say thịt để đảm bảo độ bền cho m&aacute;y v&agrave; cối xay. </em></p>\r\n\r\n			<p><em>Hiện tại tr&ecirc;n thị trường c&ograve;n c&oacute; nhiều d&ograve;ng m&aacute;y say c&oacute; chức năng tự bật, tắt khi sử dụng. Nếu kết hợp th&ecirc;m t&iacute;nh năng ngắt tự động th&igrave; việc say đồ ăn dặm sẽ tiết kiệm thao t&aacute;c hơn, m&aacute;y sẽ tự động tắt sau 30 gi&acirc;y hoạt động. </em></p>\r\n\r\n			<p><strong>N&ecirc;n chọn những cối xay c&oacute; thương hiệu tr&ecirc;n thị trường như</strong><em>: Phlilip, Bluestone, Panasonic, Braun&hellip; V&igrave; khi c&aacute;c bạn mua được những chiếc m&aacute;y từ những h&atilde;ng c&oacute; t&ecirc;n tuổi n&agrave;y sẽ rất đảm bảo chất lượng khi sử dụng v&agrave; chế độ bảo h&agrave;nh l&acirc;u d&agrave;i. Tr&ecirc;n đ&acirc;y l&agrave; một số đặc điểm khi bạn chọn m&aacute;y say để say đồ ăn dặm cho trẻ nhỏ. C&aacute;c bạn lưu &yacute; khi sử dụng cần nhớ bật tắt trong khoảng thờigian 15 &ndash; 30 gi&acirc;y. Khi say thịt hoặc những đồ cứng n&ecirc;n cho th&ecirc;m một ch&uacute;t nướcth&igrave; m&aacute;y sẽ say nhanh hơn v&agrave; đảm bảo chất lượng hơn. Hi vọng với những kiến thức tr&ecirc;n sẽ hữu &iacute;ch hơn cho c&aacute;c bạn. Ch&uacute;c c&aacute;c bạn t&igrave;m được chiếc m&aacute;y say ph&ugrave; hợp với gia đ&igrave;nh m&igrave;nh.</em></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>&nbsp;</div>', 'c4282dd5-bf26-491f-9ac9-4cc8404d3710_may-xay-sinh-to-may-xay-thit-bluestone-blb-5335w1_(1).jpg', 2, '2017-09-19 09:10:44', '2017-11-15 09:10:44'),
(4, 2, 'Top 3 bếp điện đôi giá mềm có thay thế bếp ga', 'top-3-bep-dien-doi-gia-mem-co-the-thay-the-bep-ga', '<p><strong>Hiện nay, bếp điện từ, bếp hồng ngoại dần trở n&ecirc;n phổ biến hơn trong mỗi gia đ&igrave;nh. Kh&ocirc;ng những chỉ c&oacute; bếp điện đơn, bếp từ, hồng ngoại, bếp hỗn hợp cũng l&agrave; sự lựa chọn s&aacute;ng gi&aacute; trong việc thay thế bếp ga. Trong rất nhiều model gi&aacute; mềm, vừa tiền, gi&aacute; th&agrave;nh hợp l&yacute; th&igrave; hiện nay đang c&oacute; 3 model ph&ugrave; hợp với nhu cầu n&agrave;y. H&ocirc;m nay, ch&uacute;ng ta h&atilde;y c&ugrave;ng t&igrave;m hiểu những model bếp n&agrave;y nh&eacute;! B&ecirc;́p đ&ocirc;i đi&ecirc;̣n từ SUNHOUSE SHB9101</strong></p>\r\n\r\n<p>B&ecirc;́p đ&ocirc;i đi&ecirc;̣n từ SUNHOUSE SHB 9101 c&ocirc;ng xuất 4400W với 2 mặt nấu ph&ugrave; hợp với mọi k&iacute;ch cỡ nồi, bảng điều khiển cảm ứng gi&uacute;p bạn linh hoạt hơn trong qu&aacute; tr&igrave;nh nấu ăn. B&ecirc;́p được thiết kế kiểu d&aacute;ng hiện đại, sang trọng ph&ugrave; hợp với nhiều kh&ocirc;ng gian bếp mang đến sự tiện lợi khi sử dụng. B&ecirc;́p c&oacute; chất liệu mặt bếp bằng k&iacute;nh chịu lực, chịu nhiệt, độ bền cao, s&aacute;ng b&oacute;ng đem tới sự sang trọng, dễ d&agrave;ng vệ sinh sau khi sử dụng. B&ecirc;́p c&ograve;n c&oacute; c&oacute; c&ocirc;ng suất hoạt động l&ecirc;n tới 4400W đem lại khả năng nấu nướng nhanh, tiết kiệm thời gian v&agrave; c&ocirc;ng sức. B&ecirc;́p c&oacute; nhiều chế độ nấu nướng đa năng gi&uacute;p bạn thoảim&aacute;i tạo ra nhiều m&oacute;n ăn thơm ngon, hấp dẫn cho gia đ&igrave;nh. Ngo&agrave;i ra bếp c&ograve;n đượctrang bị kh&oacute;a an to&agrave;n trẻ em đảm bảo sự y&ecirc;n t&acirc;m, kh&ocirc;ng g&acirc;y hại cho trẻ. B&ecirc;́p c&oacute;chế độ tự động ngắt khi qu&aacute; n&oacute;ng gi&uacute;p bạn kh&ocirc;ng lo về c&aacute;c sự cố như ch&aacute;y, nổ...</p>\r\n\r\n<p>B&ecirc;́p đ&ocirc;i đi&ecirc;̣n từ - hồng ngoạiSHB9100 B&ecirc;́p đ&ocirc;i Sunhouse SHB9100 thiết kế hiện đại với 2 l&ograve; nấu 1 từ v&agrave; 1 hồng ngoại ph&ugrave; hợp với mọi loại nồi nấu kh&aacute;c nhau trong từng nhu cầu nấu ăn kh&aacute;c nhau. B&ecirc;́p được trang bị mặt k&iacute;nh to&agrave;n phần cao cấp c&oacute; khả năng chịu nhiệt, chịu lực tốt gi&uacute;p tăng th&ecirc;m tuổi thọ cho bếp v&agrave; an to&agrave;n với người d&ugrave;ng. B&ecirc;́p đ&ocirc;i c&oacute; hệ thống ph&iacute;m bấm cảm ứng cho từng l&ograve; nấu kh&aacute;c nhau dễ d&agrave;ng sử dụng v&agrave; điều chỉnh,bộ điều khiển 2 v&ograve;ng tiết kiệm điện khisử dụng. Hiện tại bếp c&oacute; c&ocirc;ng suất ti&ecirc;u thụ 4300W tiết kiệm thời gian nấu ăn.V&agrave; kh&oacute;a trẻ em an to&agrave;n cho những gia đ&igrave;nh c&oacute; trẻ nhỏ kh&ocirc;ng lo trẻ động v&agrave;o bếp g&acirc;y mất an to&agrave;n. Chức năng hẹn giờ nấu th&ocirc;ng minh gi&uacute;p người sử dụng c&oacute; thể vừa nấu ăn, vừa c&oacute; thể l&agrave;m c&aacute;c c&ocirc;ng việc kh&aacute;c m&agrave; kh&ocirc;ng phỉa lo lắng về thức ăn nấu tr&ecirc;n bếp. B&ecirc;́p c&oacute; hệ thống cảm biến tự động ngắt khi qu&aacute; tải điện hay kh&ocirc;ng may xảy ra c&aacute;c hiện tượng kh&ocirc;ng mong muốn trong qu&aacute; tr&igrave;nh nấu ăn đảm bảo tuổi thọ cho bếp.</p>\r\n\r\n<p>B&ecirc;́p đ&ocirc;i h&ocirc;̀ng ngoại Sunhouse SHB9102MT B&ecirc;́p đ&ocirc;i h&ocirc;̀ng ngoại Sunhouse SHB9102MT thiết kế hiện đại, sang trọng, chất liệu bền đẹp. C&oacute; 2 v&ugrave;ng nấu nướng gi&uacute;p c&aacute;c b&agrave; nội trợ c&oacute; thể chế biến nhiều m&oacute;n c&ugrave;ng một l&uacute;c. Bếp c&oacute; thiết kế mặt k&iacute;nh cao cấp c&oacute; khả năng chịu được t&aacute;c động của lực, chịu sốc nhiệt rất tốt. Mặt k&iacute;nh của bếp s&aacute;ng b&oacute;ng sang trọng, dễ d&agrave;ng vệ sinh. Bảng điều khiển sử dụng đơn giản với bảng điều khiển cảm ứng. Người sử dụng c&oacute; thể dễ d&agrave;ng chọn lựa chế độ nấu theo &yacute; muốn.Bếp c&ograve;n được trang bị nhiều chế độ nấu kh&aacute;c nhau gi&uacute;p nấu đượcnhiều m&oacute;n v&agrave; tiết kiệm nhiệt năng. Bếp đ&ocirc;i hồng ngoại c&oacute; thể sử dụng c&aacute;c loại xoong chảo bằng nhiều chất liệu như: nh&ocirc;m, hợp kim, inox, gốm, sứ, đất nung&hellip; để đun nấu rất tiện lợi.</p>\r\n\r\n<p>Tr&ecirc;n đ&acirc;y l&agrave; ba model gi&aacute; c&oacute; tầm gi&aacute; vừa tiền. C&oacute; thể thay thế đun nấu cho bếp ga trong bếp. T&ugrave;y theo nhu cầu sử dụng thực tế m&agrave; c&aacute;c bạn chọn bếp c&oacute; chức năng cho ph&ugrave; hợp. Nếu bạn muốn đun nhanh th&igrave; n&ecirc;n mua hai bếp từ, nếu c&aacute;c bạn muốn đun tất cả c&aacute;c loại xong nồi th&igrave; sử dụng bếp hồng ngoại đ&ocirc;i. Muốn tận dụng ưu điểm của hai loại bếp tr&ecirc;n ta chọn bếp hỗn hợp, kết hợp cả hai ưu điểm tr&ecirc;n c&aacute;c bạn chọn bếp một từ một hồng ngoại. Tr&ecirc;n đ&acirc;y l&agrave; một số gợi &yacute; cho c&aacute;c bạn khi chọn mua bếp điện với 3 mẫu cơ bản phổ th&ocirc;ng. Hi vọng qua b&agrave;i viết n&agrave;y c&aacute;c bạn sẽ hiểu hơn về t&iacute;nh năng, Cũng như trang bị cho m&igrave;nh được kiến thức khi ch&uacute;ng ta chuẩn bị mua bếp điện.</p>', 'jkUR_bep-doi-dien-tu-sunhouse-shb9101-cs-4400w-1(1).jpg', 2, '2017-09-19 09:12:44', '2017-11-15 14:01:52'),
(5, 2, 'Lưu ý khi sử dụng và bảo quản lò vi sóng?', 'luu-y-khi-su-dung-va-bao-quan-lo-vi-song', '<div>03:27 18-09-2017</div>\r\n\r\n<div>\r\n<p><strong>Chiếc l&ograve; vi s&oacute;ng đ&atilde; trở n&ecirc;n quen thuộc với gia đ&igrave;nh việt. Tuy nhi&ecirc;n việc sử dụng v&agrave; bảo quản chiếc l&ograve;vi s&oacute;ng th&igrave; kh&ocirc;ng phải ai cũng biết. Đ&ocirc;i khi việc chưa biết c&aacute;ch sử dụng hoặc chưa biết c&aacute;ch bảo quản sẽ dẫn đến giảm tuổi thọ l&ograve; vi s&oacute;ng v&agrave; c&oacute; nguy cơ mất an to&agrave;n. Vậy l&agrave;m như thế n&agrave;o mới đ&uacute;ng, l&agrave;m như thế n&agrave;o mới đem lại hiệu quả cao th&igrave; phải cần sự hiểu biết, sự t&igrave;m hiểu.Ch&uacute;ng ta h&atilde;y c&ugrave;ng t&igrave;m hiểu qua b&agrave;i viết dưới đ&acirc;y.</strong></p>\r\n\r\n<p><strong>Lưu &yacute; khi sử dụng:</strong></p>\r\n\r\n<p>Sử dụng l&ograve; vi s&oacute;ng cần nhớ&nbsp; những điểm sau đ&acirc;y: Tuyệt đối kh&ocirc;ng cho đồ kim loại v&agrave;o l&ograve; vi s&oacute;ng. V&iacute; dụ như b&aacute;t, đĩa kim loại, b&aacute;t đĩa c&oacute; hoa văn bằng kim loại. Khi sử dụng ch&uacute;ng ta kh&ocirc;ng được đậy thức ăn k&iacute;n qu&aacute;. Nếu l&agrave; hộp đựng cần mở ra, hoặc trường hợp ta muốn hấp trứng, nướng trứng th&igrave; phải đục lỗ tr&ecirc;n quả trứng để tho&aacute;t hơi, hoặc đập trứng ra cho v&agrave;o b&aacute;t, sau đ&oacute; mới đem hấp để tr&aacute;nh hiện tượng mất an to&agrave;n khi sử dụng.</p>\r\n\r\n<p><img alt=\"\" src=\"http://img.trananh.vn/trananh/2017/09/18/lo-vi-song-electrolux-co-nuong-23l-800w-emm2318x-3.jpg\" style=\"border:0px solid; height:186px; vertical-align:top; width:284px\" /></p>\r\n\r\n<p><strong>Sử dụng đ&uacute;ng chức năng:</strong></p>\r\n\r\n<p>Khi sử dụng l&ograve; vi s&oacute;ng sẽ chia l&agrave;m ba loại: D&ograve;ng cơ, d&ograve;ng điện tử v&agrave; d&ograve;ng b&aacute;n điện tử. Việc sử dụng d&ograve;ng cơ th&igrave; rất đơn giản. Ch&uacute;ng ta chỉ cần vặn n&uacute;m c&ocirc;ng xuất v&agrave; n&uacute;m thời gian. Một số d&ograve;ng cơ c&ograve;n k&yacute; hiệu lu&ocirc;n số trọng lượng r&atilde; đ&ocirc;ng cạnh biểu tượng thời gian rất tiện lợi.Chỉ cần chọn đ&uacute;ng c&ocirc;ng xuất v&agrave; trọng lượng gi&atilde; đ&ocirc;ng l&agrave; c&oacute; thể thực hiện việc r&atilde; đ&ocirc;ng một c&aacute;ch nhanh ch&oacute;ng. D&ograve;ng điện tử th&igrave; kh&aacute;c, d&ograve;ng điện tử c&oacute; rất nhiều chứcnăng như r&atilde; đ&ocirc;ng tự động, nấu ăn tự động, nướng tự động. C&oacute; nhiều m&oacute;n ăn cho người việt.</p>\r\n\r\n<p><strong>Tr&ecirc;n l&ograve; vi s&oacute;ng điện tử sẽ c&oacute; hai c&aacute;ch sử dụng:</strong>&nbsp;một l&agrave; d&ugrave;ng ph&iacute;m chọn chức năng lập tr&igrave;nh sẵn như nấu c&aacute;c m&oacute;n ăn,r&atilde; đ&ocirc;ng, nướng&hellip;, hai l&agrave; tự đặt c&ocirc;ng xuất v&agrave; thời gian bằng tay. Lưu &yacute; khi sử dụng hai loại n&agrave;y ch&uacute;ng ta n&ecirc;n sử dụng c&ocirc;ng xuất trung b&igrave;nh, nếu sử dụng c&ocirc;ng xuất qu&aacute; lớn thức ăn sẽ kh&ocirc;ng đều, nếu ta chọn c&ocirc;ng xuất lớn th&igrave; cần thường xuy&ecirc;n kiểm tra xem thức ăn đ&atilde; ch&iacute;n chưa, đảo đều thức ăn cho thức ăn ch&iacute;nh đều hơn. Cần lưu &yacute; đọc kỹ s&aacute;ch hướng dẫn sử dụng trước khi sử dụng để đảm bảo sử dụng đ&uacute;ng v&agrave; đủ c&ocirc;ng xuất, thời gian đun nấu. Nắm được c&aacute;c t&iacute;nh năng mới như quạt đối lưu, hấp bằng hơi nước si&ecirc;u nhiệt.</p>\r\n\r\n<p><img alt=\"\" src=\"http://img.trananh.vn/trananh/2017/09/18/lo-vi-song-sharp-co-co-nuong-20l-r-g221vn-w-1_(2).jpg\" style=\"border:0px solid; height:186px; vertical-align:top; width:284px\" /></p>\r\n\r\n<p><strong>Về việc bảo quản</strong></p>\r\n\r\n<p>Sau mỗi lần sử dụng, c&aacute;c bạn cần vệsinh. C&oacute; thể l&agrave;m theo c&aacute;ch n&agrave;y:</p>\r\n\r\n<p>Cho một th&igrave;a dấm v&agrave;o một nửa b&aacute;t nước, bật chế độ trung b&igrave;nh trong v&ograve;ng 5 ph&uacute;t. Nếu l&ograve; c&ocirc;ng xuất lớn th&igrave; n&ecirc;n đặt &iacute;t thời gan đi, ch&uacute;ng ta đặt khoảng 2 ph&uacute;t rồi quan s&aacute;t lượng hơi nước trong l&ograve; để điều chỉnh cho ph&ugrave; hợp. Dung dịch dấm trắng bốc hơi b&ecirc;n trong l&ograve; sẽ l&agrave;m bong c&aacute;c vết bẩn cứng đầu trong l&ograve;. Lấy t&ocirc; dấm ra rồi d&ugrave;ng rẻ vệ sinh l&ograve; một c&aacute;ch dễd&agrave;ng. Chiếc l&ograve; của bạn sẽ tr&aacute;nh được m&ugrave;i h&ocirc;i, vết bẩn cứng đầu b&ecirc;n trong l&ograve;.</p>\r\n\r\n<p><strong>Về vị tr&iacute; đặt:</strong>&nbsp;Để l&ograve; được bền l&acirc;u, ch&uacute;ng ta n&ecirc;n đặt l&ograve; ở nơi kh&ocirc; r&aacute;o tho&aacute;ng m&aacute;t. Nếu đặt trong tủ bếp th&igrave; phải c&oacute; kh&ocirc;ng gian cho l&ograve; vi s&oacute;ng tho&aacute;t hơi. Nhằm gi&uacute;p khi sử dụng đảm bảo an to&agrave;n, bền l&acirc;u. Kh&ocirc;ng đặt l&ograve; vi s&oacute;ngở tr&ecirc;n n&oacute;c tủ lạnh. V&igrave; khi sử dụng l&ograve; sẽ ph&aacute;t nhiệt l&agrave;m tủ lạnh bị ảnh hưởng bởi nhiệt độ l&ograve; vi s&oacute;ng.</p>\r\n\r\n<p><em>Tr&ecirc;n đ&acirc;y l&agrave; một số lưu &yacute; khi sử dụng v&agrave; bảo quản. Người ta thường n&oacute;i,của bền tại người cũng c&oacute; phần đ&uacute;ng. Bởi chỉ c&oacute; phương ph&aacute;p sử dụng đ&uacute;ng, c&aacute;ch bảo quản đ&uacute;ng, chiếc l&ograve; vi s&oacute;ng mới sử dụng được bền l&acirc;u được.</em></p>\r\n</div>', 'QYg3_g9yI_lo-vi-song-sharp-co-co-nuong-20l-r-g221vn-w-1_(2).jpg', 2, '2017-11-15 14:00:28', '2017-11-15 14:01:49');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `amount` int(10) UNSIGNED NOT NULL,
  `payment` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `form_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `form_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `form_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `active`, `amount`, `payment`, `message`, `form_name`, `status`, `created_at`, `updated_at`, `form_email`, `form_address`, `form_phone`) VALUES
(5, 3, 4900000, 'COD', NULL, 'Vũ Đồng', 1, '2018-06-17 08:04:28', '2018-06-24 14:14:00', 'nguoihungcuaem1996@gmail.com', 'Hà Nội', '098692550'),
(6, 0, 1200000, 'COD', NULL, 'Hưng', 1, '2018-06-17 10:10:55', '2018-06-24 13:58:28', 'linhhip1610@gmail.com', 'Hà Nội, Việt Nam', '01667882846'),
(7, 0, 2450000, 'COD', NULL, 'Hưng', 1, '2018-06-17 10:17:40', '2018-06-24 13:58:59', 'linhhip1610@gmail.com', 'Hà Nội, Việt Nam', '01667882846'),
(8, 3, 2450000, 'COD', NULL, 'Hưng', 0, '2018-06-23 08:20:26', '2018-06-24 14:14:23', 'linhhip1610@gmail.com', 'Hà Nội, Việt Nam', '01667882846'),
(9, 3, 8990000, 'COD', NULL, 'Hưng', 1, '2018-06-23 08:20:43', '2018-06-24 14:15:24', 'linhhip1610@gmail.com', 'Hà Nội, Việt Nam', '01667882846');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_product`
--

CREATE TABLE `order_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_price` int(10) UNSIGNED NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL,
  `product_unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `product_price`, `product_quantity`, `product_unit`, `created_at`, `updated_at`) VALUES
(5, 5, 4, 4900000, 1, 'cái', '2018-06-17 08:04:28', '2018-06-17 08:04:28'),
(6, 6, 61, 400000, 3, 'cái', '2018-06-17 10:10:55', '2018-06-17 10:10:55'),
(7, 7, 62, 2450000, 1, 'cái', '2018-06-17 10:17:40', '2018-06-17 10:17:40'),
(8, 8, 62, 2450000, 1, 'cái', '2018-06-23 08:20:26', '2018-06-23 08:20:26'),
(9, 9, 20, 8990000, 1, 'cái', '2018-06-23 08:20:43', '2018-06-23 08:20:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(10) UNSIGNED NOT NULL,
  `discount` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `number` int(11) NOT NULL,
  `unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `slug`, `price`, `discount`, `number`, `unit`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 7, 'Máy tính bảng Ipad Air 2', 'may-tinh-bang-ipad-air-2', 12300000, 12000000, 100, 'cái', '[\"dZdI_ipad-air-2-cellular-32g-300-300x300.jpg\",\"6UJp_ipad-air-2-cellular-32g-300-300x300.jpg\",\"tFUp_ipad-air-2-cellular-32g-300-300x300.jpg\"]', 'Máy tính bảng Ipad Air 2 Cellular 32GB', '2017-08-02 06:59:10', '2017-11-08 07:47:05'),
(2, 7, 'Máy tính bảng iPad Mini 4', 'may-tinh-bang-ipad-mini-4', 12900000, 0, 100, 'cái', '[\"X8FM_ipad-mini-4-wifi-cellular-32gb-300-200x200.jpg\",\"MaIc_ipad-mini-4-wifi-cellular-32gb-300-200x200.jpg\",\"DHV0_ipad-mini-4-wifi-cellular-32gb-300-200x200.jpg\"]', 'Máy tính bảng iPad Mini 4 Wifi Cellular 32GB', '2017-08-02 06:59:10', '2017-11-08 07:37:44'),
(3, 7, 'Máy tính bảng iPad Pro 9.7', 'may-tinh-bang-ipad-pro-97', 11900000, 0, 100, 'cái', '[\"Frlp_ipad-pro-97-inch-21-300x300.jpg\",\"vHcK_ipad-pro-97-inch-21-300x300.jpg\",\"PDCJ_ipad-pro-97-inch-21-300x300.jpg\"]', 'Máy tính bảng iPad Pro 9.7 inch Wifi 32GB', '2017-08-02 06:59:10', '2017-11-08 07:48:28'),
(4, 7, 'Samsung Galaxy Tab E 9.6', 'samsung-galaxy-tab-e-96', 4900000, 0, 95, 'cái', '[\"F2wg_samsung-galaxy-tab-e-96-sm-t561-300x300.jpg\",\"nyJu_samsung-galaxy-tab-e-96-sm-t561-300x300.jpg\",\"E6dh_samsung-galaxy-tab-e-96-sm-t561-300x300.jpg\"]', 'Máy tính bảng Samsung Galaxy Tab E 9.6 (SM-T561)', '2017-08-02 06:59:10', '2018-06-24 14:13:04'),
(5, 7, 'Máy tính bảng iPad Wifi', 'may-tinh-bang-ipad-wifi', 12900000, 12800000, 100, 'cái', '[\"7vjI_ipad-wifi-cellular-32gb-2017-300-300x300.jpg\",\"DKSV_ipad-wifi-cellular-32gb-2017-300-300x300.jpg\",\"ISMg_ipad-wifi-cellular-32gb-2017-300-300x300.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 07:44:22'),
(6, 7, 'Lenovo Yoga Book Carbon', 'lenovo-yoga-book-carbon', 13000000, 12500000, 97, 'cái', '[\"2JtY_2uUN_lenovo.jpg.png\",\"YPMA_2uUN_lenovo.jpg.png\",\"XpJV_2uUN_lenovo.jpg.png\"]', 'Thiết kế gọn nhẹ\r\nĐiểm nổi bật nhất của máy chính là trọng lượng chỉ 700g giúp bạn có thể dễ dàng mang theo đi bất cứ nơi đâu mà mình muốn.\r\n\r\nLớp vỏ của Lenovo Yoga Book Carbon được chế tác từ hợp kim magie mang lại sự chắc chắc và tôn lên sự sang trọng cho người dùng.', '2017-08-02 06:59:10', '2018-06-17 10:16:37'),
(7, 7, 'Samsung Galaxy Tab A 8.0', 'samsung-galaxy-tab-a-80', 6490000, 0, 97, 'cái', '[\"HsLg_samsung-galaxy-tab-a-80-2017-anhava-300x300.jpg\",\"U4YY_samsung-galaxy-tab-a-80-2017-anhava-300x300.jpg\",\"dG5u_samsung-galaxy-tab-a-80-2017-anhava-300x300.jpg\"]', 'Máy tính bảng Samsung Galaxy Tab A 8.0 (2017)', '2017-08-02 06:59:10', '2018-06-17 10:24:49'),
(8, 7, 'iPad Wifi 32GB', 'ipad-wifi-32gb', 8900000, 8000000, 100, 'cái', '[\"UTRl_ipad-wifi-32gb-2017-300-1-300x300.jpg\",\"XX2m_ipad-wifi-32gb-2017-300-1-300x300.jpg\",\"ofXH_ipad-wifi-32gb-2017-300-1-300x300.jpg\"]', 'Máy tính bảng iPad Wifi 32GB (2017)', '2017-08-02 06:59:10', '2017-11-08 07:51:42'),
(9, 7, 'Samsung Galaxy Tab S2', 'samsung-galaxy-tab-s2', 10900000, 10830000, 100, 'cái', '[\"SRTj_samsung-galaxy-tab-s2-ve-97-t819-1-300x300.jpg\",\"BsTB_samsung-galaxy-tab-s2-ve-97-t819-1-300x300.jpg\",\"6XKr_samsung-galaxy-tab-s2-ve-97-t819-1-300x300.jpg\"]', 'Máy tính bảng Samsung Galaxy Tab S2 VE 9.7&quot; T819', '2017-08-02 06:59:10', '2017-11-08 07:50:09'),
(10, 6, 'Vỏ máy tính Jetek', 'vo-may-tinh-jetek', 360000, 0, 100, 'cái', '[\"16Zo_vo-may-tinh-jetek-a901b-300-300x300.jpg\",\"wljo_vo-may-tinh-jetek-a901b-300-300x300.jpg\",\"gwv8_vo-may-tinh-jetek-a901b-300-300x300.jpg\"]', 'Vỏ máy tính Jetek A901B', '2017-08-02 06:59:10', '2017-11-08 08:01:05'),
(11, 6, 'PSJET0015.Nguồn JeTek Q350', 'psjet0015nguon-jetek-q350', 380000, 0, 100, 'cái', '[\"rFtI_nguon-may-tinh-jetek-q350--350w.jpg\",\"aoT5_nguon-may-tinh-jetek-q350--350w.jpg\",\"ANyp_nguon-may-tinh-jetek-q350--350w.jpg\"]', 'PSJET0015.Nguồn JeTek Q350 (350W) - 24pin/ Công suất thực', '2017-08-02 06:59:10', '2017-11-14 08:51:14'),
(12, 6, 'Bộ nhớ trong máy tính để bàn (NGỪNG KINH DOANH)', 'bo-nho-trong-may-tinh-de-ban-ngung-kinh-doanh', 710000, 700000, 100, 'cái', '[\"rDzg_bo-nho-trong-may-tinh-de-ban-dato-4g-ddr3-bus-1600-300x300.jpg\",\"RZQa_bo-nho-trong-may-tinh-de-ban-dato-4g-ddr3-bus-1600-300x300.jpg\",\"FL4J_bo-nho-trong-may-tinh-de-ban-dato-4g-ddr3-bus-1600-300x300.jpg\"]', 'Bộ nhớ trong máy tính để bàn DATO 4G DDR3 bus 1600', '2017-08-02 06:59:10', '2017-12-03 15:43:23'),
(13, 9, 'Máy giặt LG inverter 8 kg FC1408S4W2', 'may-giat-lg-inverter-8-kg-fc1408s4w2', 10490000, 1000000, 100, 'cái', '[\"bc9S_maygiatlg-fc1408s4w2-300x300.png\",\"Bw8Q_may-giat-lg-fc1408s4w2-org-2-org.jpg\",\"mojK_may-giat-lg-fc1408s4w2-org-4-org.jpg\"]', 'CỬA TRƯỚC8 KGĐỘNG CƠ TRUYỀN ĐỘNG TRỰC TIẾP BỀN & ÊM, INVERTER\r\nCông nghệ giặt 6 chuyển động bảo vệ quần áo bền đẹp với thời gian.\r\nTiêu diệt 99,9% bụi bẩn và các tác nhân gây dị ứng với công nghệ giặt hơi nước Spa Steam.\r\nMáy giặt Inverter tiết kiệm điện năng, vận hành êm ái, hiệu quả.\r\nTính năng chẩn đoán thông minh qua điện thoại, tiết kiệm thời gian, chi phí.', '2017-08-02 06:59:10', '2017-11-08 08:31:05'),
(14, 9, 'Máy giặt Toshiba 9kg AW-G1000GV WG', 'may-giat-toshiba-9kg-aw-g1000gv-wg', 6390000, 0, 100, 'cái', '[\"lOnb_may-giat-toshiba-aw-g1000gv-wg-300x300.png\",\"MQYQ_toshiba-aw-g1000gv-wg3-org.jpg\",\"6mJw_toshiba-aw-g1000gv-wg9-org.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 08:28:28'),
(15, 5, 'Điện thoại Samsung Galaxy S8 Plus', 'dien-thoai-samsung-galaxy-s8-plus', 20490000, 0, 100, 'cái', '[\"hUMH_samsung-galaxy-s8-plus-20-300x300.jpg\",\"8Avt_samsung-galaxy-s8-plus-xanhduong-1-6-org.jpg\",\"lggb_samsung-galaxy-s8-plus-xanhduong-4-3-org.jpg\"]', 'Đột phá về thiết kế\r\nNăm nay, Samsung ra mắt thêm phiên bản Samsung S8 Plus với màn hình và pin lớn hơn. Kế thừa màn hình cong tràn 2 cạnh của Samsung S7 Edge, thay đổi tỉ lệ màn hình cho máy trông dài hơn, tối ưu cả về hiển thị lẫn kích thước máy không quá lớn.\r\n\r\nPhần viền máy 2 cạnh bên được làm dày hơn khắc phục hạn chế khi cầm nắm như trên thế hệ trước.', '2017-08-02 06:59:10', '2017-11-14 09:39:06'),
(17, 4, 'Laptop Asus X441NA N4200 (GA070T)', 'laptop-asus-x441na-n4200-ga070t', 7690000, 0, 100, 'cái', '[\"CxZv_asus-x441na-n4200-den-1-1-org.jpg\",\"iqQt_asus-x441na-n4200-den-2-1-org.jpg\",\"XO0D_asus-x441na-n4200-den-5-1-org.jpg\"]', 'Kiểu dáng văn phòng\r\nLaptop ASUS X441NA có thiết kế phù hợp cho người làm văn phòng, hay đặc biệt là sinh viên và học sinh. Chất liệu vỏ nhựa nhẹ nhàng chỉ khoảng 1.7 kg thích hợp cho sử dụng cố định hay phải di chuyển đều thuận tiện. Bù lại máy khá dày. Họa tiết vân xước giả kim loại trên lưng máy được thể hiện đẹp mắt.', '2017-08-02 06:59:10', '2017-11-14 08:44:33'),
(18, 4, 'Lenovo IdeaPad', 'lenovo-ideapad', 5190000, 0, 100, 'cái', '[\"vhjl_lenovo-ideapad-120s-11iap-450x300-1-450x300.jpg\",\"mZe8_lenovo-ideapad-120s-11iap-450x300-1-450x300.jpg\",\"iYVI_lenovo-ideapad-120s-11iap-450x300-1-450x300.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 07:34:00'),
(19, 4, 'Laptop ACER AS', 'laptop-acer-as', 6000000, 0, 100, 'cái', '[\"2I5o_acer-as-es1-533-c5ts-nxgftsv001-celeron-n3350-4gd3-1-450x300.jpg\",\"Qtkp_acer-as-es1-533-c5ts-nxgftsv001-celeron-n3350-4gd3-1-450x300.jpg\",\"0gXG_acer-as-es1-533-c5ts-nxgftsv001-celeron-n3350-4gd3-1-450x300.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 07:35:35'),
(20, 4, 'Laptop Acer Aspire ES1-572-32GZ', 'laptop-acer-aspire-es1-572-32gz', 8990000, 0, 96, 'cái', '[\"T5jG_acer-aspire-es1-572-32gz-nxgkqsv001-core-i3-7100u-1-450x300.jpg\",\"CUSj_acer-aspire-es1-572-32gz-nxgkqsv001-core-i3-7100u-1-h13.jpg\",\"nvjq_laptop-acer-as-es1-572-32gz.jpg\"]', 'Laptop Acer Aspire ES1-572-32GZ NX.GKQSV.001 Core i3-7100U/4G/500G5/15.6HD/Đen/LNX/không túi\r\n\r\nThiết kế chắc chắn\r\nAcer Aspire ES1-572-32GZ là dòng laptop có màn hình lớn, thiết kế khá chắn chắn va đầy đủ tính năng, kết nối phục vụ cho nhu cầu làm việc và giải trí, học tập. Tuy nhiên máy hơi dày  25 mm và nặng 3.2 Kg không phù hợp nhu cầu cần mang, di chuyển nhiều.', '2017-08-04 06:59:10', '2018-06-24 14:15:24'),
(21, 3, 'Ti vi Led Sony', 'ti-vi-led-sony', 16000000, 15000000, 100, 'cái', '[\"pMHb_tivi-led-sony-kd-43x7500e-550x340.png\",\"VO7f_tivi-led-sony-kd-43x7500e-550x340.png\",\"pNWi_tivi-led-sony-kd-43x7500e-550x340.png\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 07:15:57'),
(22, 3, 'Ti vi Led TCL 32 inch L32S6100', 'ti-vi-led-tcl-32-inch-l32s6100', 4900000, 0, 100, 'cái', '[\"oOye_tivi-led-tcl-l32s6100-550x340.jpg\",\"H4de_tivi-led-tcl-l32s6100-550x340.jpg\",\"5P8k_tivi-led-tcl-l32s6100-550x340.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 07:22:57'),
(23, 3, 'Smart Tivi Samsung 43 inch', 'smart-tivi-samsung-43-inch', 12390000, 0, 100, 'cái', '[\"JmVQ_smarttivisamsung-ua43m5500-550x340.png\",\"vX6A_smarttivisamsung-ua43m5500-550x340.png\",\"7pQc_smarttivisamsung-ua43m5500-550x340.png\"]', 'Smart Tivi Samsung 43 inch UA43M5500', '2017-08-02 06:59:10', '2017-11-08 07:59:01'),
(24, 11, 'Quạt lửng Midea FS40-15VD', 'quat-lung-midea-fs40-15vd', 400000, 0, 100, 'cái', '[\"EbF2_quat-midea-fs40-15vd-1-org-1.jpg\",\"kpki_quat-midea-fs40-15vd-1-org-4.jpg\",\"hYxK_quat-midea-fs40-15vd-1-org-8.jpg\"]', 'Thiết kế đơn giản\r\n\r\nQuạt Midea FS40-15VD thiết kế đơn giản nhưng hiệu quả. Quạt có màu xanh thanh mát, kích cỡ nhỏ gọn, phù hợp với nhiều không gian phòng ốc.\r\nSải cánh rộng\r\n\r\nQuạt lửng Midea FS40-15VD được trang bị 3 cánh quạt với sải cánh dài 40 cm, mang đến hiệu quả làm mát tối ưu cho diện tích rộng.\r\nCông suất 45W\r\n\r\nCông suất 45 W, không chỉ nhanh chóng tạo ra làn gió mát xua tan cái nóng mà còn rất tiết kiệm điện năng.', '2017-08-02 06:59:10', '2017-11-13 17:45:16'),
(25, 12, 'Lò vi sóng Electrolux EMM2022MW 20 lít', 'lo-vi-song-electrolux-emm2022mw-20-lit', 1300000, 0, 100, 'cái', '[\"lgCm_lo-vi-song-electrolux-emm2022mw-org-1.jpg\",\"GyAK_lo-vi-song-electrolux-emm2022mw-org-4.jpg\",\"my1I_lo-vi-song-electrolux-emm2022mw-org-6.jpg\"]', 'Kiểu dáng hiện đại, sang trọng\r\n\r\nLò vi sóng cơ Electrolux EMM2022MW có thiết kế hiện đại, kiểu dáng nhỏ gọn, màu trắng trang nhã phù hợp với mọi không gian nội thất của nhiều gia đình.\r\n	\r\n\r\nDung tích 20 Lít\r\n\r\nElectrolux EMM2022MW có dung tích lên đến 20 Lít đáp ứng mọi nhu cầu nấu nướng cho gia đình bạn. Sản phẩm đặc biệt phù hợp với những gia đình có từ 3 - 4 người. \r\nBảng điều khiển cơ dễ thao tác\r\n\r\nElectrolux EMM2022MW có bảng điều khiển cơ với 2 nút xoay để chỉnh hẹn giờ và chọn mức năng lượng vi sóng, dễ hiểu, dễ thao tác.\r\nĐĩa quay tròn tiện dụng\r\n\r\nLò vi sóng cơ Electrolux thiết kế đĩa quay tròn bên trong lò giúp thức ăn chín nhanh và đều hơn, tiết kiệm thời gian và cho những món ăn ngon.', '2017-08-02 06:59:10', '2017-11-13 17:50:17'),
(26, 11, 'Quạt treo Midea FW40-15VF', 'quat-treo-midea-fw40-15vf', 360000, 350000, 100, 'cái', '[\"lLgx_quat-midea-fw40-15vf-org-1.jpg\",\"7SI3_quat-midea-fw40-15vf-org-3.jpg\",\"cGZr_quat-midea-fw40-15vf-org-4.jpg\"]', 'Thiết kế đơn giản\r\n\r\nQuạt treo tường Midea FW40-15VF 3 cánh trong suốt, kiểu dáng đơn giản nhưng không kém phần tinh tế. Phù hợp với mọi không gian nhà.\r\nCông suất 55W\r\n\r\nQuạt treo tường Midea FW40-15VF có công suất 55W giúp làm mát không khí nhanh chóng, quạt xoay mọi hướng giúp không khí trong lành, thoáng mát.', '2017-08-02 06:59:10', '2017-11-13 17:43:08'),
(27, 12, 'Lò vi sóng Midea 20 lít MMO-20KE1', 'lo-vi-song-midea-20-lit-mmo-20ke1', 1190000, 0, 100, 'cái', '[\"iefp_lo-vi-song-midea-mmo-20ke1-org-1.jpg\",\"aqls_lo-vi-song-midea-mmo-20ke1-1-org-2.jpg\",\"3IdI_lo-vi-song-midea-mmo-20ke1-org-3.jpg\"]', 'Kiểu dáng hiện đại, sang trọng\r\n\r\nLò vi sóng cơ Midea MMO-20KE1 có thiết kế hiện đại, kiểu dáng nhỏ gọn, màu trắng trang nhã phù hợp với mọi không gian nội thất của nhiều gia đình.\r\nMidea MMO-20KE1 có dung tích lên đến 20 Lít đáp ứng mọi nhu cầu nấu nướng cho gia đình bạn. Sản phẩm đặc biệt phù hợp với những gia đình có từ 3 - 4 người.\r\nBảng điều khiển cơ dễ thao tác\r\n\r\nMidea MMO-20KE1 có bảng điều khiển cơ với 2 nút xoay để chỉnh hẹn giờ và chọn mức năng lượng vi sóng, dễ hiểu, dễ thao tác.', '2017-08-02 06:59:10', '2017-11-13 17:48:04'),
(28, 6, 'Nguồn Huntkey GS600', 'nguon-huntkey-gs600', 1100000, 0, 100, 'cái', '[\"f58y_nguon-huntkey-gs600--600w--80plus.jpg\",\"7orq_f58y_nguon-huntkey-gs600--600w--80plus.jpg\",\"prBR_f58y_nguon-huntkey-gs600--600w--80plus.jpg\"]', 'Nguồn Huntkey GS600 - 600W - 24 pin', '2017-08-02 06:59:10', '2017-11-08 08:05:46'),
(29, 9, 'Máy giặt Electrolux Inverter 8 kg EWF10844', 'may-giat-electrolux-inverter-8-kg-ewf10844', 9990000, 0, 100, 'cái', '[\"uVXN_may-gia-telectrolux-ewf108444-300x300.jpg\",\"ERw2_may-giat-electrolux-ewf10844-org-3-org.jpg\",\"Yb5A_may-giat-electrolux-ewf10844-org-2-org.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 08:49:43'),
(30, 3, 'Internet Tivi Sony 43 inch KDL-43W750E (HẾT HÀNG)', 'internet-tivi-sony-43-inch-kdl-43w750e-het-hang', 12400000, 0, 100, 'cái', '[\"THYE_pMHb_tivi-led-sony-kd-43x7500e-550x340.png\",\"p9qC_tivi-sony-kdl-43w750e-2-1-org.jpg\",\"chGJ_tivi-sony-kdl-43w750e-9-org.jpg\"]', '43\"FULL HDINTERNET TIVIKẾT NỐI 2 HDMI, 2 USB, 1 LAN, WIFI\r\nHình ảnh sắc nét chi tiết với độ phân giải Full HD.\r\nỨng dụng phong phú với chợ ứng dụng Opera TV Store.\r\nDải màu sắc sống động, chân thực với công nghệ Dynamic Color.\r\nCông nghệ hiển thị trung thực TRILUMINOS Display.\r\nCông nghệ HDR cho dải tầng nhạy sáng cao.', '2017-08-02 06:59:10', '2017-12-03 15:44:25'),
(31, 6, 'Bộ vi xử lý Intel® Pentium® (CÒN HÀNG)', 'bo-vi-xu-ly-intel-pentium-con-hang', 1290000, 0, 100, 'cái', '[\"8pPi_bo-vi-xu-ly-intel-pentium-g4400-330ghz-2-2-3mb-300x300.jpg\",\"juwK_bo-vi-xu-ly-intel-pentium-g4400-330ghz-2-2-3mb-300x300.jpg\",\"8Ldr_bo-vi-xu-ly-intel-pentium-g4400-330ghz-2-2-3mb-300x300.jpg\"]', 'Bộ vi xử lý Intel® Pentium® G4400 3.30GHz / (2/2) / 3MB / Intel® HD Gr', '2017-08-02 06:59:10', '2017-12-03 15:43:53'),
(32, 6, 'Vỏ máy tính Tiger Jetek', 'vo-may-tinh-tiger-jetek', 400000, 0, 100, 'cái', '[\"6NJo_catg01vo-may-tinh-tiger-jetek-v1208b-a8b08-a8b22a-300x300.jpg\",\"tyoe_catg01vo-may-tinh-tiger-jetek-v1208b-a8b08-a8b22a-300x300.jpg\",\"EogN_catg01vo-may-tinh-tiger-jetek-v1208b-a8b08-a8b22a-300x300.jpg\"]', 'CATG01.Vỏ máy tính Tiger Jetek V1208B/A8B08/A8B22', '2017-08-02 06:59:10', '2017-11-08 08:02:45'),
(33, 6, 'Tủ lạnh Panasonic inverter', 'tu-lanh-panasonic-inverter', 5900000, 5850000, 100, 'cái', '[\"FrW8_tu-lanh-panasonic-nr-ba178psv11-300x300.png\",\"foaW_tu-lanh-panasonic-nr-ba178psv1-2-org.jpg\",\"POKh_tu-lanh-panasonic-nr-ba178psv1-3-org.jpg\"]', 'Tủ lạnh Panasonic inverter 152 lít NR-BA178PSV1', '2017-08-02 06:59:10', '2017-11-08 08:09:33'),
(34, 6, 'Bo mạch chủ Biostar (CÒN HÀNG)', 'bo-mach-chu-biostar-con-hang', 1200000, 0, 100, 'cái', '[\"ndmg_mainboard-biostar-h81mhv3.jpg\",\"DxjW_mainboard-biostar-h81mhv3(1).jpg\",\"P6Da_mainboard-biostar-h81mhv3(2).jpg\"]', 'Bo mạch chủ Biostar H81MHV3', '2017-08-02 06:59:10', '2017-12-03 15:42:58'),
(35, 6, 'Bộ nhớ trong máy tính để bàn DATO (CÒN HÀNG)', 'bo-nho-trong-may-tinh-de-ban-dato-con-hang', 1600000, 1589000, 100, 'cái', '[\"XJgD_bo-nho-trong-may-tinh-de-ban-dato-8gb-ddr4-2400mhz-300x300.jpg\",\"Hpeh_bo-nho-trong-may-tinh-de-ban-dato-8gb-ddr4-2400mhz-300x300.jpg\",\"Y7sX_bo-nho-trong-may-tinh-de-ban-dato-8gb-ddr4-2400mhz-300x300.jpg\"]', 'Bộ nhớ trong máy tính để bàn DATO 8GB DDR4 2400Mhz', '2017-08-02 06:59:10', '2017-12-03 15:43:41'),
(36, 6, 'Bo mạch chủ ASUS PRIME B250M (CÒN HÀNG)', 'bo-mach-chu-asus-prime-b250m-con-hang', 2000000, 1890000, 100, 'cái', '[\"z7jg_bo-mach-chu-asus-prime-b250m-k-kaby-lake-2-x-dimm-300x300.jpg\",\"eB9K_bo-mach-chu-asus-prime-b250m-k-kaby-lake-2-x-dimm-300x300.jpg\",\"5wZX_bo-mach-chu-asus-prime-b250m-k-kaby-lake-2-x-dimm-300x300.jpg\"]', 'Bo mạch chủ ASUS PRIME B250M-K KABY LAKE (2 x DIMM, Max.32GB, DDR4 240', '2017-08-02 06:59:10', '2018-06-17 10:01:28'),
(37, 6, 'Máy giặt Panasonic 8 kg NA-F80VS9GRV', 'may-giat-panasonic-8-kg-na-f80vs9grv', 5190000, 0, 100, 'cái', '[\"tCbZ_may-giat-panasonic-na-f80vs9grv-300x300.png\",\"UMMZ_may-giat-panasonic-na-f80vs9grv-org-4.jpg\",\"1bIW_may-giat-panasonic-na-f80vs9grv-org-6.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 08:33:53'),
(38, 5, 'Điện thoại iPhone 7 Plus 128GB', 'dien-thoai-iphone-7-plus-128gb', 22900000, 0, 100, 'cái', '[\"UrOD_AUwO_iphone-7-plus-256gb-300x300.jpg\",\"eSy8_AUwO_iphone-7-plus-256gb-300x300.jpg\",\"KY8m_AUwO_iphone-7-plus-256gb-300x300.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 08:47:04'),
(39, 5, 'Điện thoại iPhone 7 Plus 256GB', 'dien-thoai-iphone-7-plus-256gb', 23990000, 0, 100, 'cái', '[\"f8vE_iphone-7-plus-256gb-300x300.jpg\",\"AUwO_iphone-7-plus-256gb-300x300.jpg\",\"DXqe_iphone-7-plus-256gb-300x300.jpg\"]', NULL, '2017-08-02 06:59:10', '2017-11-08 08:40:26'),
(40, 8, 'Tủ lạnh Samsung 208 lít', 'tu-lanh-samsung-208-lit', 150000, 130000, 100, 'cái', '[\"LNcW_5Hsl_toshiba-gr-tg41vpdz-xk1-300-300x300.jpg\",\"Srtv_VIPl_toshiba-gr-tg41vpdz-xk1-300-300x300.jpg\",\"Anps_tQcO_pEYq_5Hsl_toshiba-gr-tg41vpdz-xk1-300-300x300.jpg\"]', 'Thịt bò xay, ngô, sốt BBQ, phô mai mozzarella', '2017-08-02 06:59:10', '2017-11-08 06:51:59'),
(41, 8, 'Tủ lạnh Toshiba 226 lít GR-M28VHBZ(UKG)', 'tu-lanh-toshiba-226-lit-gr-m28vhbzukg', 7790000, 0, 100, 'cái', '[\"n1Qo_Srtv_VIPl_toshiba-gr-tg41vpdz-xk1-300-300x300.jpg\",\"KVk9_tu-lanh-toshiba-gr-m28vhbz-ukg-2-2-org.jpg\",\"ZBem_tu-lanh-toshiba-gr-m28vhbz-ukg-7-1-org.jpg\"]', '226 LÍT TỦ LẠNH INVERTER ĐIỆN NĂNG TIÊU THỤ ~ 1 KW/NGÀY\r\nCông nghệ inverter thế hệ mới tiết kiệm đến 40% điện năng tiêu thụ.\r\nBộ khử mùi Hybrid Bio diệt khuẩn khử mùi hiệu quả.\r\nCông nghệ làm lạnh tuần hoàn luân chuyển luồng khí lạnh đến mọi ngóc ngách.\r\nThiết kế sang trọng\r\nTủ lạnh Toshiba 226 lít GR-M28VHBZ là chiếc tủ lạnh vừa ra mắt trong năm 2017 của hãng sản xuất Toshiba. Chiếc tủ lạnh nổi bật với kiểu dáng tinh tế, sang trọng cùng mức dung tích phù hợp với những gia đình từ 3 tới 5 thành viên.', '2017-08-02 06:59:10', '2017-11-14 12:53:28'),
(42, 8, 'Tủ lạnh Sharp 626 lít SJ-FX630V-ST', 'tu-lanh-sharp-626-lit-sj-fx630v-st', 19900000, 0, 100, 'cái', '[\"xDRZ_tu-lanh-sharp-sj-fx630v-st-300-300x300.jpg\",\"QJPR_tu-lanh-sharp-sj-fx630v-st-6-org-1.jpg\",\"mjTT_tu-lanh-sharp-sj-fx630v-st-org-11.jpg\"]', '626 LÍTTỦ LẠNH INVERTERĐIỆN NĂNG TIÊU THỤ ~ 1.25 KW/NGÀY\r\nCông nghệ J-Tech Inverter tiết kiệm điện tối ưu, vận hành bền bỉ.\r\nBộ khử mùi Nano Bạc Đồng giúp tủ lạnh loại bỏ vi khuẩn, mùi hôi.\r\nHệ thống làm lạnh kép đảm bảo cho thực phẩm luôn giữ được độ tươi lâu.\r\nLàm lạnh nhanh chóng và giữ được độ lạnh lâu dài.\r\nThiết kế lịch lãm, sáng tạo\r\nTủ lạnh Sharp SJ-FX630V-ST có thiết kế 4 cửa tinh tế và hiện đại, tông màu bạc trang nhã, tích hợp bảng điều khiển bên ngoài cửa tủ sẽ mang đến cho không gian nội thất của gia đình bạn sang trọng và đẳng cấp hơn bao giờ hết.', '2017-08-01 07:26:10', '2017-11-14 13:01:10'),
(43, 8, 'Tủ lạnh Sharp 196 lít SJ-X201E-DS', 'tu-lanh-sharp-196-lit-sj-x201e-ds', 5590000, 0, 100, 'cái', '[\"0SlK_tu-lanh-sharp-sj-x201e-ds-org-1-org.jpg\",\"LGw7_tu-lanh-sharp-sj-x201e-ds-org-3-org.jpg\",\"M1pW_tu-lanh-sharp-sj-x201e-ds-org-8-org.jpg\"]', '196 LÍT TỦ LẠNH INVERTER\r\nCông nghệ Inverter tiết kiệm điện năng.\r\nBộ lọc Nano Ag+Cu tăng cường khả năng khử mùi.\r\nChức năng Extra Eco – Tiết kiệm năng lượng tối ưu.\r\nThiết kế hiện đại, ngăn tủ rộng rãi\r\nTủ lạnh Sharp 196 lít SJ-X201E-DS có thiết kế 2 cửa, bao gồm 1 ngăn đá và 1 ngăn lạnh. Dung tích sử dụng thực tế của tủ là 182 lít, phù hợp với những gia đình có khoảng 3 thành viên.', '2017-08-02 06:59:10', '2017-11-14 12:59:01'),
(44, 8, 'Tủ lạnh Samsung 380 lít RT38K5982SL/SV', 'tu-lanh-samsung-380-lit-rt38k5982slsv', 15290000, 15000000, 100, 'cái', '[\"KPW7_tu-lanh-samsung-rt38k5982sl-sv-300x300.jpg\",\"dmZQ_tu-lanh-samsung-rt38k5982sl-sv-99-2.jpg\",\"BfGz_tu-lanh-panasonic-nr-ba178psv1-1-org.jpg\"]', '380 LÍT TỦ LẠNH INVERTERĐIỆN NĂNG TIÊU THỤ ~ 1.03 KW/NGÀY.\r\nHai dàn lạnh độc lập giữ độ ẩm và bảo quản thực phẩm tối ưu, không bị lẫn mùi.\r\nCông nghệ Digital Inverter vận hành êm ái, tiết kiệm điện và bền bỉ.\r\nChế độ làm lạnh và làm đá nhanh chóng, đánh tan cái nóng mùa hè.\r\nChức năng làm đá tự động tiện lợi. Thiết kế màu bạc ấn tượng và sang trọng\r\nTủ lạnh Samsung 380 lít RT38K5982SL/SV được thiết kế sang trọng với tông màu xám đầy hiện đại. Sở hữu dung tích khá lớn lên đến 380 lít, chiếc tủ lạnh này sẽ rất phù hợp với những gia đình đông thành viên (5 tới 7 người) hoặc có thói quen lưu trữ thực phẩm nhiều (ít đi chợ).', '2017-08-02 06:59:10', '2017-11-14 09:47:33'),
(45, 8, 'Tủ lạnh Toshiba inverter 305 lít GR-MG36VUBZ(XB)', 'tu-lanh-toshiba-inverter-305-lit-gr-mg36vubzxb', 9990000, 0, 100, 'cái', '[\"g5w0_tu-lanh-toshiba-gr-mg36vubz-xb-1-org.jpg\",\"YGuD_tu-lanh-toshiba-gr-mg36vubz-xb-11-org.jpg\",\"FJSo_tu-lanh-toshiba-gr-mg36vubz-xb-9-org.jpg\"]', '305 LÍT TỦ LẠNH INVERTER\r\nCông nghệ Inverter tiết kiệm 50% điện năng.\r\nHệ thống khử mùi và diệt khuẩn Hybrid Bio.\r\nNgăn rau quả rộng, tăng thêm 75%.\r\nNgăn làm lạnh kép làm lạnh cực nhanh thực phẩm.\r\nTủ lạnh ngăn đá trên quen thuộc với người tiêu dùng\r\nTủ lạnh Toshiba GR-MG36VUBZ(XB) có thiết kế 2 cửa ngăn đá trên quen thuộc giúp người tiêu dùng cảm thấy dễ dàng trong việc sử dụng. Hơn nữa, ngăn đông của tủ còn có tính năng khử mùi, giúp cho mùi của thực phẩm không bị lẫn vào nước đá, cho bạn những viên đá mát lạnh, tinh khiết, không mùi.', '2017-08-02 06:59:10', '2017-11-14 13:06:16'),
(46, 8, 'Tủ lạnh Hitachi inverter 365 lít R-VG440PGV3 GBK', 'tu-lanh-hitachi-inverter-365-lit-r-vg440pgv3-gbk', 13790000, 13700000, 100, 'cái', '[\"27v8_tu-lanh-hitachi-r-vg440pgv3-4-org-2.jpg\",\"2xtH_tu-lanh-hitachi-r-vg440pgv3-3-org-3.jpg\",\"4FsN_tu-lanh-hitachi-r-vg440pgv3-3-org-9.jpg\"]', '365 LÍT TỦ LẠNH INVERTER ĐIỆN NĂNG TIÊU THỤ ~ 1.2 KW/NGÀY\r\nCông nghệ Inverter kết hợp cảm biến Eco tiết kiệm điện năng vô cùng hiệu quả.\r\nHệ thống quạt kép 2 ngăn riêng biệt đưa hơi lạnh đến mọi ngóc ngách bên trong tủ.\r\nMàng lọc Nano Titanium khử mùi và kháng khuẩn mạnh mẽ.\r\nThiết kế cửa thủy tinh sang trọng\r\nTủ lạnh Hitachi R-VG440PGV3 365 lít được thiết kế cửa thủy tinh sang trọng, rất phù hợp với không gian bếp gia đình bạn. Tủ lạnh với dung tích 365 lít, phù hợp với những gia đình từ 5-7 người.', '2017-08-02 06:59:10', '2017-11-14 13:03:43'),
(47, 9, 'Máy giặt Beko inverter 8 kg WTV 8634 XS0', 'may-giat-beko-inverter-8-kg-wtv-8634-xs0', 9990000, 0, 100, 'cái', '[\"xB21_may-giat-beko-wtv-8634-xs0-1-org-1.jpg\",\"xkcm_may-giat-beko-wtv-8634-xs0-org-4.jpg\",\"Xynp_may-giat-beko-wtv-8634-xs0-org-10.jpg\"]', 'Thiết kế hiện đại, sang trọng\r\nMáy giặt Beko WTV 8634 XS0 sở hữu thiết kế hiện đại mang phong cách mới mẻ từ Châu Âu, với vẻ ngoài tinh tế, độc đáo cùng gam màu trắng sang trọng phù hợp với không gian nội thất của gia đình bạn. Có khối lượng giặt 8 kg, máy giặt Beko WTV 8634 XS0 là lựa chọn tối ưu cho những gia đình có 4 - 5 thành viên.', '2017-08-02 06:59:10', '2017-11-14 09:10:34'),
(48, 8, 'Tủ lạnh Samsung 208 lít RT19M300BGS/SV', 'tu-lanh-samsung-208-lit-rt19m300bgssv', 5590000, 0, 100, 'cái', '[\"xJH0_tu-lanh-samsung-rt19m300bgs-sv-1-1-org.jpg\",\"Dscm_tu-lanh-samsung-rt19m300bgs-sv-3-1-org (1).jpg\",\"asYl_tu-lanh-samsung-rt19m300bgs-sv-4-org.jpg\"]', 'Thiết kế tinh tế, sáng tạo\r\nTủ lạnh Samsung RT19M300BGS/SV với thiết kế tinh tế, sáng tạo, chiếc tủ lạnh này sẽ làm cho không gian nhà bạn thêm hiện đại. Tủ lạnh có dung tích 208 lít, phù hợp với gia đình có từ 3-5 thành viên.', '2017-08-02 06:59:10', '2017-11-14 09:30:52'),
(49, 8, 'Tủ lạnh Panasonic inverter 152 lít NR-BA178PSV1', 'tu-lanh-panasonic-inverter-152-lit-nr-ba178psv1', 5590000, 0, 100, 'cái', '[\"ycMZ_tu-lanh-panasonic-nr-ba178psv11-300x300.png\",\"yXFJ_tu-lanh-panasonic-nr-ba178psv1-3-org.jpg\",\"SFtK_tu-lanh-panasonic-nr-ba178psv1-7-org.jpg\"]', '152 LÍTTỦ LẠNH INVERTERĐIỆN NĂNG TIÊU THỤ ~ 0.96 KW/NGÀY.\r\nHộc rau quả cung cấp độ ẩm cho rau quả tươi lâu trong thời gian dài.\r\nCông nghệ kháng khuẩn khử mùi bằng tinh thể bạc tiêu diệt vi khuẩn và mùi hôi khó chịu.\r\nTủ lạnh tiết kiệm điện năng hiệu quả với công nghệ Inverter kết hợp cảm biến Econavi. Thiết kế đơn giản, ngăn đá trên quen thuộc\r\nTủ lạnh Panasonic NR-BA178PSV1 đến từ thương hiệu Panasonic có thiết kế 2 cửa, ngăn đá trên quen thuộc với người tiêu dùng Việt Nam. Với dung tích 152 lít, tủ lạnh sẽ là sự lựa chọn phù hợp cho những gia đình ít thành viên (1-3 người) hoặc đối tượng là người độc thân.', '2017-08-02 06:59:10', '2017-11-14 09:33:26'),
(50, 9, 'Máy giặt Toshiba Inverter', 'may-giat-toshiba-inverter', 9290000, 0, 100, 'cái', '[\"rMh8_may-giat-toshiba-aw-de1100gv-ws-2-org-1.jpg\",\"WVEB_may-giat-toshiba-aw-de1100gv-ws-2-org-5.jpg\",\"aYL7_may-giat-toshiba-aw-de1100gv-ws-2-org-6.jpg\"]', 'CỬA TRÊN10 KG ĐỘNG CƠ TRUYỀN ĐỘNG TRỰC TIẾP BỀN & ÊM, INVERTER - \r\nLồng giặt ngôi sao pha lê nâng cao hiệu quả chà xát mà hạn chế hư sợi vải.\r\nCông nghệ S-DD Inverter giúp mâm giặt quay nhanh nhưng vẫn êm ái.\r\nFragrance Course lưu giữ hương thơm lâu trên sợi vải.\r\nGiặt cô đặc bằng bọt khí giúp đánh tan mọi vết bẩn.Máy giặt cửa trên sang trọng.\r\nVới thiết kế nhỏ gọn và hiện đại, cùng những đường viền tinh tế, máy giặt Toshiba AW - DE1100GV (WS) hứa hẹn sẽ mang đến cho gia đình bạn một góc phòng thật sang trọng. Nhờ có khối lượng giặt 10 kg, máy giặt Toshiba sẽ phù hợp cho những gia đình có từ 6 thành viên trở lên.', '2017-08-02 06:59:10', '2017-11-14 09:03:39'),
(51, 9, 'Máy giặt Electrolux Inverter 9 kg EWF12942', 'may-giat-electrolux-inverter-9-kg-ewf12942', 12900000, 0, 100, 'cái', '[\"djFW_electrolux-ewf12942-nkk-12-300x300.jpg\",\"m3tY_may-giat-electrolux-ewf10844-org-2-org.jpg\",\"AVLZ_may-giat-electrolux-ewf12942-4-1-org.jpg\"]', 'Thiết kế nổi bật và sang trọng\r\nMáy giặt Electrolux Inverter 9 kg EWF1294 là thế hệ máy giặt lồng ngang sở hữu phong cách thiết kế nổi bật và sang trọng. Máy sử dụng gam màu trắng tinh tế là nét chấm phá ấn tượng cho mọi không gian nội thất. Sản phẩm có khối lượng giặt tẩy quần áo lên đến 9 kg rất thích hợp với những gia đình có từ 6 thành viên trở lên.', '2017-08-02 06:59:10', '2017-11-14 08:56:27'),
(53, 9, 'Máy giặt Samsung AddWash inverter', 'may-giat-samsung-addwash-inverter', 10990000, 0, 100, 'cái', '[\"MeUo_may-giat-samsung-ww80k5410ww-sv-8-org-1.jpg\",\"07IZ_may-giat-samsung-ww80k5410ww-sv-2-org-4.jpg\",\"fe6B_may-giat-samsung-ww80k5410ww-sv-2-org-7.jpg\"]', 'Thiết kế hiện đại, sang trọng\r\nMáy giặt Samsung AddWash Inverter WW80K5410WW/SV là dòng sản phẩm máy giặt AddWash của Samsung mới ra mắt cuối năm 2016, sở hữu thiết kế hiện đại, đẳng cấp cùng đường nét tinh tế, gam màu trắng sang trọng phù hợp với không gian nội thất sang trọng của gia đình bạn. Sản phẩm này có khối lượng giặt 8 kg, thích hợp với những gia đình có 4 - 5 thành viên.', '2017-08-02 06:59:10', '2017-11-14 08:59:10'),
(54, 9, 'Máy giặt LG Inverter 8 kg FC1408S3Ei', 'may-giat-lg-inverter-8-kg-fc1408s3ei', 11790000, 0, 100, 'cái', '[\"E36c_may-giat-lg-fc1408s3e-org-1-org.jpg\",\"iHEU_may-giat-lg-fc1408s3e-org-2-org.jpg\",\"mEqv_may-giat-lg-fc1408s3e-org-4-org.jpg\"]', 'CỬA TRƯỚC8 KG ĐỘNG CƠ TRUYỀN ĐỘNG TRỰC TIẾP BỀN & ÊM, INVERTER\r\nCông nghệ biến tần Inverter giúp tiết kiệm điện năng.\r\nCông nghệ giặt 6 chuyển động giúp quần áo sạch sẽ, thơm tho.\r\nTruyền động trực tiếp bền bỉ và êm ái.\r\nCông nghệ kháng khuẩn khử mùi giúp làm sạch quần áo. Thiết kế tinh tế, sang trọng\r\nMáy giặt LG Inverter 8 kg FC1408S3E sở hữu thiết kế tinh tế, sang trọng, chiếc máy giặt này sẽ làm cho không gian gia đình bạn thêm sang trọng, quý phái. Máy giặt này phù hợp với gia đình có từ 4-5 người sử dụng.', '2017-08-02 06:59:10', '2017-11-14 09:23:28'),
(55, 8, 'Tủ lạnh Sharp 165 lít SJ-16VF3-CMS', 'tu-lanh-sharp-165-lit-sj-16vf3-cms', 3990000, 0, 100, 'cái', '[\"kEGW_tu-lanh-sharp-sj-16vf3-cms-1-org.jpg\",\"RU7F_tu-lanh-sharp-sj-16vf3-cms-5-org.jpg\",\"vviN_tu-lanh-sharp-sj-16vf3-cms-6-org.jpg\"]', '165 LÍT TỦ LẠNH THƯỜNGĐIỆN NĂNG TIÊU THỤ ~ 1.11 KW/NGÀY.\r\nCông nghệ làm lạnh gián tiếp.\r\nBộ khử mùi phân tử bạc Nano Ag+ cho thực phẩm tươi ngon mỗi ngày.\r\nHệ thống đèn Led giúp tiết kiệm điện năng, tăng cường tuổi thọ cho tủ lạnh. Thiết kế ấn tượng, đẹp\r\nTủ lạnh Sharp SJ-16VF3-CMS sở hữu thiết kế khá ấn tượng và đẹp, chiếc tủ lạnh này sẽ làm cho căn nhà của bạn thêm phần hiện đại. Tủ lạnh sở hữu dung tích 165 lít, rất phù hợp với gia đình có từ 1-3 người.', '2017-08-02 06:59:10', '2017-11-14 09:35:30'),
(56, 11, 'Quạt bàn Midea FT30-Y8BA', 'quat-ban-midea-ft30-y8ba', 300000, 0, 100, 'cái', '[\"QNec_quatquat-ban-midea-ft30-y8ba-thumb-300x300.jpg\",\"ninK_quatquat-ban-midea-ft30-y8ba-org-2-org.jpg\",\"Tiim_quatquat-ban-midea-ft30-y8ba-org-3-org.jpg\"]', 'Thiết kế thời trang\r\n\r\nQuạt bàn Midea FT30-Y8B màu sắc trang nhã sẽ là một điểm nhấn cho góc bàn làm việc, góc học tập hay phòng ngủ… \r\n3 cấp độ gió khác nhau\r\n\r\nQuạt có 3 cấp độ gió giúp người tiêu dùng có thể thay đổi tốc độ gió theo sở thích và mục đích sử dụng dễ dàng hơn.\r\nChức năng hẹn giờ\r\n\r\nQuạt bàn Midea FT30-Y8B chức năng hẹn giờ lên tới 120 phút, giúp bạn chủ động hơn về thời gian sử dụng, đồng thời tiết kiệm điện năng một cách hiệu quả.\r\nTút đảo chiều\r\n\r\nNút bấm điều khiển trạng thái quạt cây hoặc quay, nếu muốn chuyển hướng trái, phải để luồng gió trải rộng hơn chỉ cần một động tác nhấn nút đơn giản.', '2017-08-02 06:59:10', '2017-11-13 17:38:59'),
(57, 8, 'Tủ lạnh Beko 90 lít RS9050P', 'tu-lanh-beko-90-lit-rs9050p', 2890000, 0, 100, 'cái', '[\"a7rP_tu-lan-hbeko-rs9050p-1-1-org.jpg\",\"YEsk_tu-lan-hbeko-rs9050p-3-1-org.jpg\",\"NZC0_tu-lan-hbeko-rs9050p-8-1-org.jpg\"]', 'Thiết kế đơn giản, nhỏ gọn\r\nTủ lạnh Beko RS9050P được thiết kế khá đơn giản nhưng không kém phần hiện đại. Tủ có màu xám tinh tế, sang trọng. Dung tích 90 lít đáp ứng tốt nhu cầu sử dụng của những gia đình có khoảng 1 - 3 thành viên.', '2017-08-02 06:59:10', '2017-11-14 09:37:02'),
(58, 4, 'Laptop HP 15-ay538TU', 'laptop-hp-15-ay538tu', 9000000, 0, 100, 'cái', '[\"cOaE_hp-15-bs554tu-core-i3-6006u-4gb-500gb-dvdrw-intel-450x300.jpg\",\"nVu1_hp-15-ay538tu-1ac62pa-core-i3-6006u-4gb-500gb-dv-1-2.jpg\",\"VJ22_hp-15-ay538tu-1ac62pa-core-i3-6006u-4gb-500gb-dv-2-3.jpg\"]', 'Thiết kế thanh lịch, nhỏ gọn\r\nHP 15 có thiết kế khá đơn giản, tinh tế. Với đặc điểm là dòng laptop phổ thông nên không đầu tư nhiều về mặt ngoại hình.\r\n\r\nMặt lưng phây xướt giả kim loại tạo nên vẻ sang trọng.', '2017-08-02 06:59:10', '2017-11-14 09:42:30'),
(59, 4, 'Laptop HP 14-am060TU', 'laptop-hp-14-am060tu', 6500000, 0, 100, 'cái', '[\"zhOp_hp-14-am060tu-pqc-n3710-x1h09pa=1-450x300.jpg\",\"Rk7t_cafe.jpg\",\"14oR_childhoods.jpg\"]', 'Laptop HP 14-am060TU (X1H09PA) PQC N3710/4G/500G5/DVDRW/14.0HD/BT4/4C41WHr/bạc/DOS', '2017-08-02 06:59:10', '2017-11-08 07:56:49'),
(60, 4, 'Laptop HP 15-bs554TU Core i3-6006U', 'laptop-hp-15-bs554tu-core-i3-6006u', 9500000, 0, 100, 'cái', '[\"tjXY_hp-15-bs554tu-core-i3-6006u-4gb-500gb-dvdrw-intel-450x300.jpg\",\"jNIl_hp-15-bs554tu-core-i3-6006u-4gb-500gb-dvdrw-intel-450x300.jpg\",\"Ftif_hp-15-bs554tu-core-i3-6006u-4gb-500gb-dvdrw-intel-450x300.jpg\"]', 'Laptop HP 15-bs554TU Core i3-6006U/4GB/500GB/DVDRW/Intel HD Graphics/15.6 HD/Wlan bgn+BT/4cell/Do. Thiết kế chắc chắn\r\nHP 15-bs554TU thuộc dòng laptop phổ thông. Về phần thiết kế khá đơn giải nổi bật với lớp vỏ nhựa nhưng nắp máy được trang trí những họa tiết vân sần tạo nên vẻ cứng cáp cho sản phẩm.\r\n\r\nVới kiểu thiết kế trẻ trung, chắc chắn sẽ phù hợp với các bạn trẻ năng động, những người làm việc văn phòng.', '2017-08-02 06:59:10', '2017-11-14 08:48:37'),
(61, 11, 'Quạt hộp Asia F12004-TV1', 'quat-hop-asia-f12004-tv1', 400000, 0, 91, 'cái', '[\"eO8z_quat-hop-asia-f12004-tv1-1-1.jpg\",\"htIf_quat-hop-asia-f12004-tv1-2.jpg\",\"Zaht_quat-hop-asia-f12004-tv1-4.jpg\"]', 'Thiết kế nhỏ gọn, đẹp mắt, thích hợp dùng cho không gian hẹp như bàn ăn, bàn học, phòng ngủ... Khung quạt chắc chắn bằng chất liệu nhựa cao cấp, bền tốt, hạn chế bám bụi bẩn dễ vệ sinh\r\nLồng quạt an toàn cho tiếp xúc gần. Quạt thiết kế 6 cánh quạt đường kính 30 cm, cùng công suất 40 W giúp làm mát hiệu quả. Điều khiển quạt đơn giản bằng nút xoay hiển thị rõ chế độ\r\nCó 3 tốc độ gió để lựa chọn, chế độ đảo gió làm mát linh hoạt và tiện lợi hơn.', '2017-09-21 02:26:14', '2018-06-24 13:58:23'),
(62, 12, 'Lò vi sóng Panasonic NN-SM332MYUE 25 lít', 'lo-vi-song-panasonic-nn-sm332myue-25-lit', 2450000, 0, 88, 'cái', '[\"jOMp_lo-vi-song-panasonic-nn-sm332myue-25-lit-org-1.jpg\",\"gc8N_lo-vi-song-panasonic-nn-sm332myue-25-lit-org-3.jpg\",\"FSiq_lo-vi-song-panasonic-nn-sm332myue-25-lit-org-5.jpg\"]', 'Thiết kế hiện đại - \r\nLò vi sóng Panasonic NN-SM332MYUE 25L chất liệu cao cấp, thiết kế sang trọng với hai màu trắng đen bắt mắt và dễ chùi rửa, tôn thêm vẻ hiện đại cho không gian bếp lớn. \r\nKhoang lò 25L - \r\n\r\nPanasonic NN-SM332MYUE có khoang lò 25L rộng rãi, cho phép người dùng nấu được nhiều thức ăn cùng lúc, giúp chuẩn bị bữa ăn cho gia đình đông người chỉ trong nháy mắt. \r\nBảng điều khiển dễ sử dụng - \r\n\r\nLò vi sóng Panasonic NN-SM332MYUE 25L với thiết kế bảng điều khiển cơ học có hai nút xoay thật đơn giản và dễ sử dụng.', '2017-09-21 02:48:24', '2018-06-24 14:14:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slides`
--

INSERT INTO `slides` (`id`, `title`, `slug`, `image`, `active`, `created_at`, `updated_at`) VALUES
(19, 'Silde 1', 'silde-1', 'FH3h_31_10_2017_11_32_10_Pana-Dieu-Hoa-690-300.png', 1, '2017-11-07 16:40:04', '2017-11-07 16:44:54'),
(20, 'Slide 2', 'slide-2', '6arW_31_10_2017_11_47_53_Big-Pana-690-300.png', 1, '2017-11-07 16:40:50', '2017-11-07 16:44:48'),
(21, 'Slide 3', 'slide-3', 'PR87_31_10_2017_13_14_41_Samsung-Tv-UHD-690-300.png', 1, '2017-11-07 16:40:59', '2017-11-07 16:44:31'),
(22, 'Slide 4', 'slide-4', 'vtJD_31_10_2017_13_21_59_Pana-Tv-690-300.png', 1, '2017-11-07 16:41:10', '2017-11-07 16:44:24'),
(23, 'Slide 5', 'slide-5', 'Iji8_31_10_2017_13_45_57_MayGiat-690-300.png', 1, '2017-11-07 16:41:20', '2017-11-07 16:44:18'),
(24, 'Slide 6', 'slide-6', 'OXer_31_10_2017_14_00_11_LG-May-Giat-9kg-690-300.png', 1, '2017-11-07 16:41:29', '2017-11-07 16:44:03'),
(26, 'Slide 7', 'slide-7', 'VLCb_31_10_2017_14_45_49_Ariston-Binh-Nong-Lanh-690-300.png', 1, '2017-11-07 16:43:54', '2017-11-07 16:43:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Male',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default-avatar.jpg',
  `role` enum('admin','author','member') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'member',
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `gender`, `email`, `password`, `address`, `phone`, `image`, `role`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hưng', 'Male', 'linhhip1610@gmail.com', '$2y$12$2DIqdv/IvVVCh5GpJbQOQuDVIEMJ4oQSRdvmoDsSdaeXKwuE3l87e', 'Hà Nội, Việt Nam', '01667882846', 'hip.jpg', 'admin', 1, 'DdI2b8jPAwcuYggk9XkMnrKo70KSnm108ZxgrUH6Ri4VtDwyAhhzjPsybbXv', '2017-09-27 01:02:10', '2018-06-24 14:09:35'),
(2, 'Author', 'Female', 'hunhuagtvt@gmail.com', '$2y$10$lOT/O6ghfdW9MVA5BmTMq.qPGVWgcGR/cETNPbvSBDXFp7rmG68he', 'Hà Nội Việt Nam', '01667882846', 'yWuH_edit1-2.jpg', 'author', 1, 'eVqZoqBCHNKCrbXgm5QQLZNpp6uFHdy8rVKhcXh8cnZoZG5b3bOU2oi016bb', '2017-08-02 06:58:03', '2018-06-17 10:42:28'),
(3, 'Member', 'Male', 'member@gmail.com', '$2y$10$nUWzzGv.KRztJQTYYhamwuqc34V0RyxdMaS2cfC2bC./pdDMgZy0W', 'Hà Nội Việt Nam', '01234567891', 'default-avatar.jpg', 'member', 1, '1qzelRv4Mk9RuOUx8X7twAGRehjMqlruICVJbM8tVwNrrRd8wlenJxlKk7j6', '2017-09-27 01:05:31', '2018-06-17 10:42:28'),
(7, 'abc', 'Male', 'adminabc@gmail.com', '$2y$10$JobBCKvCZ.2IQKnfBhjDq.tO1G6cvyYVGtFHjwyuJGqZLSLNAXlvO', 'Hà Nội, Việt Nam', '0123456744', 'default-avatar.jpg', 'member', 1, NULL, '2017-10-11 18:31:24', '2018-06-17 10:42:28'),
(8, 'linh', 'Male', 'abc1@gmail.com', '$2y$10$OJsMeFGrH4/NGTa1Y8IlC.23XkZoF2Wu9O.57ag/vBpRjQsWYYnty', 'quốc oai hà nội', '01683317782', 'default-avatar.jpg', 'member', 1, NULL, '2017-12-04 03:25:16', '2018-06-17 10:42:28'),
(9, 'hưng', 'Male', 'hunghuagtvt@gmail.com', '$2y$10$xJF6luai2Y9GZme9u8GNVOEX4AzrFwRconVGpzxbA3h2C0d32hQee', 'shaudsucjhsjjd', '01667882846', 'default-avatar.jpg', 'member', 1, 'Jf7cpsqXDoBNnbzr5LiXToYVT1uwebKIM7NpC9BVbQoaFvMUMCZI2Av0pjsZ', '2018-03-01 09:30:42', '2018-06-17 10:42:28'),
(10, 'hung', 'Male', 'hung1996@gmail.com', '$2y$10$bRcV8DGNYfBfBi0lsLJaQOXN6QzoWChvb1B45AeNE1MaOrDK0guDK', 'wffg12dkfjđj2332vfv', '0123455688', 'default-avatar.jpg', 'member', 1, 'wDRbmBmX4EjtnAQy8bbUCy4cM5DLr3FTkmrmVz1nG0iavhpmJhrii9I8fJHn', '2018-05-05 14:32:20', '2018-06-17 10:42:28'),
(11, 'hung', 'Male', 'hhh@gmail.com', '$2y$10$MM47qxpp/2ak70l5Yen8ZObwRUFQeMLtqmgI31WkuYWio1Nsg9/JC', 'thanh xuân,hà nội', '01667882886', 'default-avatar.jpg', 'member', 0, 'hBWhhQ1q39a897Wxc66CqQGZMGEIrkfd4bcYAvMvhwqH7ydW0if2NdkTvsBJ', '2018-06-13 07:24:55', '2018-06-17 10:42:28'),
(12, 'Vũ Đồng', 'Male', 'vdong75@gmail.com', '$2y$10$ZXUYYf4xtNw.5w.afeXii.mMYYjJjWjafdqBbByQgx/0ctHfpnEe6', 'Khánh Thượng Yên Mô Ninh Bình', '0986792550', 'default-avatar.jpg', 'member', 1, NULL, '2018-06-17 10:43:50', '2018-06-17 10:44:02');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_title_unique` (`title`),
  ADD UNIQUE KEY `news_slug_unique` (`slug`),
  ADD KEY `news_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_product_order_id_foreign` (`order_id`),
  ADD KEY `order_product_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Chỉ mục cho bảng `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT cho bảng `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`);

--
-- Các ràng buộc cho bảng `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD CONSTRAINT `password_resets_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`);

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
