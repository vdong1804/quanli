$(document).ready(function ($) {
//main slider
  $('.slides-data-for').slick({
      dots: false,
      arrows: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // slidesPerRow: 1,
      // rows: 1,
      autoplay: true,
      autoplaySpeed: 6000,
      speed: 1000,
      fade: true,
      asNavFor: '.slides-data-nav'
  });
  $('.slides-data-nav').slick({
      dots: false,
      arrows: false,
      slidesToShow: 4,
      speed: 1000,
      asNavFor: '.slides-data-for',
      focusOnSelect: true
  });
//end main slider

// view('front.pages.child_home_pages.new_products')
  $('.new-products-data').slick({
      dots: false,
      arrows: true,
      infinite: false,
      // slidesToShow: 4,
      // slidesToScroll: 4,
      slidesPerRow: 4,
      rows: 1,
      autoplay: true,
      autoplaySpeed: 6000,
      speed: 1000,
      adaptiveHeight: true,
      responsive: [
      {
      breakpoint: 1024,
        settings: {
          slidesPerRow: 3,
          rows: 1,
        }
      },
      {
      breakpoint: 800,
        settings: {
          slidesPerRow: 3,
          rows: 1,
        }
      },
      {
        breakpoint: 478,
        settings: {
          slidesPerRow: 2,
          rows: 1,
        }
      }
    ]
  });

// view('front.pages.child_home_pages.discount_products')
  $('.discount-data').slick({
      dots: false,
      arrows: true,
      infinite: false,
      slidesPerRow: 4,
      rows: 2,
      autoplay: true,
      autoplaySpeed: 6000,
      speed: 1000,
      adaptiveHeight: true,
      responsive: [
      {
      breakpoint: 1024,
        settings: {
          slidesPerRow: 3,
          rows: 1,
        }
      },
      {
      breakpoint: 800,
        settings: {
          slidesPerRow: 3,
          rows: 1,
        }
      },
      {
        breakpoint: 478,
        settings: {
          slidesPerRow: 2,
          rows: 1,
        }
      }
    ]
  });

// view('front.pages.product_detail')
  $('.product-detail-image-for').slick({
      dots: false,
      arrows: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      fade: true,
      asNavFor: '.product-detail-image-nav'
  });
  $('.product-detail-image-nav').slick({
      dots: false,
      arrows: false,
      slidesToShow: 3,
      speed: 1000,
      asNavFor: '.product-detail-image-for',
      focusOnSelect: true
  });

  $('.similar-products-data').slick({
      dots: false,
      arrows: true,
      infinite: false,
      slidesPerRow: 4,
      rows: 1,
      autoplay: true,
      autoplaySpeed: 6000,
      speed: 1000,
      responsive: [
      {
      breakpoint: 1024,
        settings: {
          slidesPerRow: 3,
          rows: 1,
        }
      },
      {
      breakpoint: 800,
        settings: {
          slidesPerRow: 3,
          rows: 1,
        }
      },
      {
        breakpoint: 478,
        settings: {
          slidesPerRow: 2,
          rows: 1,
        }
      }
    ]
  });

  // view('front.pages.category_product')
  $('.other-products-data').slick({
      dots: false,
      arrows: true,
      infinite: false,
      slidesPerRow: 4,
      rows: 1,
      autoplay: true,
      autoplaySpeed: 6000,
      speed: 1000,
      responsive: [
      {
      breakpoint: 1024,
        settings: {
          slidesPerRow: 3,
          rows: 1,
        }
      },
      {
      breakpoint: 800,
        settings: {
          slidesPerRow: 3,
          rows: 1,
        }
      },
      {
        breakpoint: 478,
        settings: {
          slidesPerRow: 2,
          rows: 1,
        }
      }
    ]
  });

});


