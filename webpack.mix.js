const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

// css
// mix.styles('resources/assets/tq17_source/front/css/bootstrap.min.css', 'public/css/bootstrap.min.css')
//    .styles('resources/assets/tq17_source/front/css/font-awesome.min.css', 'public/css/font-awesome.min.css')
//    .styles('resources/assets/tq17_source/front/vendors/colorbox/example3/colorbox.css', 'public/css/colorbox.css')
//    .styles('resources/assets/tq17_source/front/rs-plugin/css/settings.css', 'public/css/settings.css')
//    .styles('resources/assets/tq17_source/front/rs-plugin/css/responsive.css', 'public/css/responsive.css')
//    .styles('resources/assets/tq17_source/front/css/style.css', 'public/css/style.css')
//    .styles('resources/assets/tq17_source/front/css/animate.css', 'public/css/animate.css')
//    .styles('resources/assets/tq17_source/front/css/trungquan-style.css', 'public/css/trungquan-style.css')
//    ;

// javascript
// mix.scripts('resources/assets/tq17_source/front/js/jquery.js', 'public/js/jquery.js')
//    .scripts('resources/assets/tq17_source/front/vendors/jqueryui/jquery-ui-1.10.4.custom.min.js', 'public/js/jquery-ui-1.10.4.custom.min.js')
//    .scripts('resources/assets/tq17_source/front/js/bootstrap.min.js', 'public/js/bootstrap.min.js')
//    .scripts('resources/assets/tq17_source/front/vendors/bxslider/jquery.bxslider.min.js', 'public/js/jquery.bxslider.min.js')
//    .scripts('resources/assets/tq17_source/front/vendors/colorbox/jquery.colorbox-min.js', 'public/js/jquery.colorbox-min.js')
//    .scripts('resources/assets/tq17_source/front/vendors/animo/animo.js', 'public/js/animo.js')
//    .scripts('resources/assets/tq17_source/front/vendors/dug/dug.js', 'public/js/dug.js')
//    .scripts('resources/assets/tq17_source/front/js/scripts.min.js', 'public/js/scripts.min.js')
//    .scripts('resources/assets/tq17_source/front/rs-plugin/js/jquery.themepunch.tools.min.js', 'public/js/jquery.themepunch.tools.min.js')
//    .scripts('resources/assets/tq17_source/front/rs-plugin/js/jquery.themepunch.revolution.min.js', 'public/js/jquery.themepunch.revolution.min.js')
//    .scripts('resources/assets/tq17_source/front/js/waypoints.min.js', 'public/js/waypoints.min.js')
//    .scripts('resources/assets/tq17_source/front/js/wow.min.js', 'public/js/wow.min.js')
//    .scripts('resources/assets/tq17_source/front/js/custom2.js', 'public/js/custom2.js')
//    .scripts('resources/assets/tq17_source/front/jquery-3.2.1/jquery-3.2.1.min.js', 'public/js/jquery-3.2.1.min.js')
//    ;
