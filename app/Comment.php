<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    //1 comment thuộc về 1 user
    public function to_user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    //1 comment thuộc về 1 sản phẩm
    public function to_product(){
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
}
