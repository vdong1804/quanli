<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class GetLoginDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $role_check = array('admin','author');
            if(in_array(Auth::user()->role, $role_check)){
                return $next($request);
            }
            else{
                return redirect()->route('home');
            }
        }
        else{
            return $next($request);
        }
    }
}
