<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PostLoginDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $role_check = array('admin','author');
            if(in_array(Auth::user()->role, $role_check)){
                return $next($request);
            }
            else{
                return redirect()->route('home');
            }
        }
        else{
            return redirect()->route('get-login-dashboard')->withErrors('Chưa đăng nhập.');
        }
    }
}
