<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\Product;
use App\Category;
use App\Cart;
use App\User;
use App\Order;
use App\Order_Product;
use App\Comment;
use App\News;
use Session;
use Auth;
use Hash;
use App\Events\CommentPusherEvent;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
use Validator;
use DB;

class FrontEndController extends Controller
{
    function __construct(){
        $slides = Slide::where('active',1)->orderBy('id', 'desc')->limit(4)->get();
        view()->share('slides',$slides);
    }
    public function get_home(Request $request){
        // $new_products = my_paginate($new_products_data, 4, $request);
        $new_products      = Product::orderBy('created_at', 'desc')->take(12)->get();
        $discount_products = Product::where('discount','<>',0)->orderBy('id', 'desc')->take(24)->get();
        return view('front.pages.home',compact('new_products', 'discount_products'));
    }

    public function get_category_product($slug)
    {
        $current_category  = Category::where('slug',$slug)->first();
        $similarCategory = Category::where('parent_id', $current_category->parent_id)->get();
        $category_products = Product::where('category_id', $current_category->id)->paginate(6);
        $other_products    = Product::where('category_id', '<>', $current_category->id)->take(8)->get();
        return view('front.pages.category_product', compact('current_category','category_products','other_products','similarCategory'));
    }
    public function get_product_detail($slug)
    {
        $current_product  = Product::where('slug',$slug)->first();
        // dd(gettype($current_product));
        $new_products     = Product::orderBy('created_at', 'desc')->take(4)->get();

        $sql = "SELECT p.id, p.category_id, p.name, p.slug, p.image, p.discount, p.price FROM products AS p INNER JOIN order_product AS op ON p.id = op.product_id GROUP BY p.id, p.category_id, p.name, p.slug, p.image, p.discount, p.price ORDER BY SUM(op.product_quantity) DESC LIMIT 4";
        $best_seller = DB::select(DB::raw($sql));
        // dd($best_seller);

        $similar_products = Product::where('category_id', $current_product->category_id)->whereNotIn('id',[$current_product->id])->take(9)->get();
        return view('front.pages.product_detail',compact('current_product','new_products','best_seller','similar_products'));
    }
    public function get_show_comment($productID, Request $request){
        if($request->ajax()){
            $display = "";
            $comments = Comment::where('product_id', $productID)->get();
            $totalComments = "(".count($comments).")";

            if(Auth::check()){
                if(count($comments)>0){
                    foreach ($comments as $comment) {
                        $display .= '
                            <div class="comments">
                                <div class="user-avatar">
                                    <img src="images/user/'.$comment->to_user->image.'" alt="">
                                </div>
                                <div class="user-name">
                                    <p>
                                        '.$comment->to_user->name.'
                                    </p>
                                </div>
                                <div class="user-comment">
                                    <p>'.$comment->content.'</p>
                                    <span>'.$comment->created_at->format('H:i, d/m/Y').'</span>
                                </div>
                            </div>';
                    }
                }
                else{
                    $display .= '
                                <p>Hãy là người đầu tiên bình luận về sản phẩm này.</p>
                                <hr>';
                }
            }
            else{
                if(count($comments)>0){
                    foreach ($comments as $comment) {
                        $display .= '
                            <div class="comments">
                                <div class="user-avatar">
                                    <img src="images/user/'.$comment->to_user->image.'" alt="">
                                </div>
                                <div class="user-name">
                                    <p>
                                        '.$comment->to_user->name.'
                                    </p>
                                </div>
                                <div class="user-comment">
                                    <p>'.$comment->content.'</p>
                                    <span>'.$comment->created_at->format('H:i, d/m/Y').'</span>
                                </div>
                            </div>';
                    }
                }
                else{
                    $display .= '';
                }
            }
        }
        //lưu những gì cần vào mảng kết quả sau đó json_encode trả về
        $results = array(
            'comments'          => $display,
            'totalComments'     => $totalComments,
        );
        return json_encode($results);
    }
    public function post_add_comment($productID, Request $request){
        if($request->ajax()){
            $this->validate($request,[
                    'content'=>'required|min:3|max:100'
                ],[
                    'content.required' => 'yêu cầu nội dung comment.',
                    'content.min'      => 'Nội dung comment giới hạn 3-100 ký tự.',
                    'content.max'      => 'Nội dung comment giới hạn 3-100 ký tự.'
                ]);
            $comment = new Comment();
            $comment->user_id = Auth::user()->id;
            $comment->product_id = $productID;
            $comment->content = $request->content;
            $check = $comment->save();
            if($check > 0){
                // đẩy comment lên pusher rồi truyền về tất cả client
                $display = "";
                $display .= '
                    <div class="comments">
                        <div class="user-avatar">
                            <img src="images/user/'.$comment->to_user->image.'" alt="">
                        </div>
                        <div class="user-name">
                            <p>
                                '.$comment->to_user->name.'
                            </p>
                        </div>
                        <div class="user-comment">
                            <p>'.$comment->content.'</p>
                            <span>'.$comment->created_at->format('H:i, d/m/Y').'</span>
                        </div>
                    </div>';
                // tổng comments đã có
                $totalComments = "(".count(Comment::where('product_id', $productID)->get()).")";

                event(new CommentPusherEvent($display, $totalComments));
                return 'Đã thêm bình luận.';
            }
        }
    }

    public function showNews()
    {
        $news = News::where('active',2)->orderBy('created_at','desc')->simplePaginate(5);
        return view('front.pages.news.show_news', compact('news'));
    }
    public function readNews($slug)
    {
        $currentNews = News::where('slug', $slug)->where('active',2)->first();
        return view('front.pages.news.read_news', compact('currentNews'));
    }

    public function get_about(){
        return view('front.pages.about');
    }
    public function get_contact(){
        return view('front.pages.contact');
    }
    public function get_register(){
        return view('front.pages.register');
    }
    public function post_register(Request $request){
        $this->validate($request,
            [
                'form_email'       =>'required|email|unique:users,email',
                'form_password'    =>'required|min:6|max:20',
                'form_re_password' =>'required|same:form_password',
                'form_fullname'    =>'required|min:2|max:50',
                'form_gender'      =>'required|in:Male,Female',
                'form_phone'       =>'required|unique:users,phone|regex:/^(0)[0-9]{9,10}$/',
                'form_address'     =>'required|min:10'
            ],
            [
                'form_email.required'   =>'Email là bắt buộc',
                'form_email.email'      =>'Không đúng định dạng example@domain.com',
                'form_email.unique'     =>'Email này đã có người sử dụng',

                'form_password.required'=>'Mật khẩu là bắt buộc',
                'form_password.min'     =>'Mật khẩu tối thiểu 6 ký tự',
                'form_password.max'     =>'Mật khẩu tối đa 20 ký tự',

                'form_re_password.required' =>'Nhập lại mật khẩu',
                'form_re_password.same'     =>'Nhập lại mật khẩu chưa đúng',

                'form_fullname.required'=>'Họ tên là bắt buộc',
                'form_fullname.min'     =>'Họ tên tối thiểu 2 ký tự',
                'form_fullname.max'     =>'Họ tên tối đa 50 ký tự',

                'form_gender.required'  =>'Giới tính là bắt buộc',
                'form_gender.in'        =>'Sai giá trị giới tính',

                'form_phone.required' =>'Số điện thoại sử dụng để liên hệ với bạn. Hãy nhập chính xác',
                'form_phone.regex'    =>'Số điện thoại bắt đầu bằng 0, gồm 10 hoặc 11 ký tự số',
                'form_phone.unique'   =>'Số điện thoại này đã có người sử dụng',

                'form_address.required' =>'Địa chỉ sử dụng để liên hệ với bạn. Hãy nhập chính xác',
                'form_address.min'      =>'Địa chỉ tối thiểu 10 ký tự'
            ]
        );
        $user = new User();
        $user->name     = $request->form_fullname;
        $user->gender   = $request->form_gender;
        $user->email    = $request->form_email;
        $user->password = Hash::make($request->form_password);
        $user->address  = $request->form_address;
        $user->phone    = $request->form_phone;
        if($request->hasFile('form_image')){
            $allowed_type = array('image/jpeg', 'image/png');
            $allowed_size = '2097152';

            $file = $request->file('form_image');

            $image_type = $file->getClientMimeType();
            $image_size = $file->getClientSize();

            if(in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)){
                $dir = 'images/user/';
                $image_name = $file->getClientOriginalName();
                $image_name_random = str_random(4).'_'.$image_name;
                while(file_exists($dir.$image_name_random)){
                    $image_name_random = str_random(4).'_'.$image_name;
                }
                $file->move($dir,$image_name_random);
                $user->image = $image_name_random;
            }
            else{
                return redirect()->back()->withInput()->withErrors('Chỉ cho phép
                     ảnh jpg/png với dung lượng tối đa 2mb');
            }
        }
        //$user->role mặc định là member rồi, tiến hành lưu (O.o)
        $user->save();
        return redirect()->route('login')->with('register_success','Tạo tài
             khoản thành công, hãy đăng nhập.');
    }
    public function get_login(){
        return view('front.pages.login');
    }
    public function post_login(Request $request){
        $this->validate($request,
            [
                'form_email' => 'required|email',
                'form_password' => 'required|min:6|max:20'
            ],
            [
                'form_email.required' => 'Email là bắt buộc',
                'form_email.email' =>'Không đúng định dạng example@domain.com',
                'form_password.required' => 'Mật khẩu là bắt buộc',
                'form_password.min' => 'Mật khẩu tối thiểu 6 ký tự',
                'form_password.max' => 'Mật khẩu tối đa 20 ký tự'
            ]
        );
        $credentials = array('email' => $request->form_email, 'password' => $request->form_password);
        if (Auth::attempt($credentials)) {
             return redirect()->back()->with('login_success','Đăng nhập thành công.');
        }
        else{
            return redirect()->back()->withErrors('Sai email hoặc mật khẩu.');
        }
    }
    public function get_logout(){
        Auth::logout();
        return redirect()->route('login')->with('logout_success','Đăng xuất thành công.');
    }
    public function getProfile()
    {
        if (Auth::check()) {
            $orders = Order::where('user_id', Auth::user()->id)->get();
            return view('front.pages.profile', compact('orders'));
        } else {
            return redirect()->route('home');
        }
    }
    public function getProfileOrderDetail($orderProductId)
    {
        $currentOrder = Order::find($orderProductId);
        $orderDetails = Order_Product::where('order_id', $orderProductId)->get();
        return view('front.pages.order_product_detail', compact('currentOrder', 'orderDetails'));
    }
    public function get_add_to_cart(Request $request, $id){
        $product = Product::find($id);
        $oldCart = Session('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);

        if(isset($request->form_qty)){
            $this->validate($request,[
                    'form_qty' => 'required|regex:/^[1-5]{1}$/'
                ],[
                    'form_qty.required' => 'Số lượng là bắt buộc.',
                    'form_qty.regex' => 'Dữ liệu đầu vào có vấn đề!'
                ]);
            $cart->add($product, $id, $request->form_qty);
        }
        else{
            $cart->add($product, $id);
        }

        $request->session()->put('cart', $cart);
        return redirect()->back();
    }

    public function get_reduce_item_cart($id){
        $oldCart = Session::has('cart')?Session::get('cart'):null;
        $cart    = new Cart($oldCart);
        $cart->reduceByOne($id);

        if(count($cart->items) > 0){
            Session::put('cart',$cart);
        }
        else{
            Session::forget('cart');
        }
        return redirect()->back();
    }

    public function get_delete_item_cart($id){
        $oldCart = Session::has('cart')?Session::get('cart'):null;
        $cart    = new Cart($oldCart);
        $cart->removeItem($id);
        if(count($cart->items) > 0){
            Session::put('cart',$cart);
        }
        else{
            Session::forget('cart');
        }
        return redirect()->back();
    }
    public function get_checkout(){
        return view('front.pages.checkout');
    }
    public function post_checkout(Request $request){
        if(Session::has('cart')){
            $this->validate($request,[
                    'form_notes'    => 'max:100',
                    'payment_method'=> 'required|in:COD,ATM',
                    'form_name'=> 'required|max:255',
                    'form_email'=> 'required|max:255',
                    'form_phone'=> 'required|max:11',
                    'form_address'=> 'required|max:255',
                ],[
                    'form_notes.max'          => 'Ghi chú vượt quá giới hạn 100 ký tự',
                    'form_name.max'          => 'Họ và tên vượt quá giới hạn 255 ký tự',
                    'form_email.max'          => 'Email vượt quá giới hạn 255 ký tự',
                    'form_phone.max'          => 'Số điện thoại vượt quá giới hạn 11 ký tự',
                    'form_address.max'          => 'Địa chỉ vượt quá giới hạn 255 ký tự',
                    'payment_method.required' => 'Phương thức thanh toán là bắt buộc',
                    'form_name.required' => 'Họ và tên là bắt buộc',
                    'form_email.required' => 'Email là bắt buộc',
                    'form_phone.required' => 'Số điện thoại là bắt buộc',
                    'form_address.required' => 'Địa chỉ là bắt buộc',
                    'payment_method.in'       => 'Sai giá trị phương thức thanh toán'
                ]);
            $cart = Session::get('cart');
            $order = new Order();
            // $order->active : default là 0
            $order->fill($request->all());
            $order->amount  = $cart->totalPrice;

            if ($request->form_notes != NULL) {
                $order->message = $request->form_notes;
            }

            $order->payment = $request->payment_method;

            if ($order->payment == "ATM") {
                $this->validate($request,[
                    'card_no' => 'required',
                    'exp_month' => 'required',
                    'exp_year' => 'required',
                    'CVC' => 'required'
                ],[
                    'card_no.required' => 'Chưa nhập số thẻ Visa',
                    'exp_month.required' => 'Chưa nhập tháng hết hạn của thẻ',
                    'exp_year.required' => 'Chưa nhập năm hết hạn của thẻ',
                    'CVC.required' => 'Chưa nhập số CVC'
                ]);
                $stripe = Stripe::make('sk_test_TwwY13NtjjKjyTgXWA7qhlJx');
                try {
                    $token = $stripe->tokens()->create([
                        'card' => [
                            'number'    => $request->card_no,
                            'exp_month' => $request->exp_month,
                            'exp_year'  => $request->exp_year,
                            'cvc'       => $request->CVC,
                        ],
                    ]);
                    if (!isset($token['id'])) {
                        return redirect()->route('checkout')->withErrors('Có lỗi
                         khi kết nối tới Stripe!');
                    }
                    $charge = $stripe->charges()->create([
                        'card' => $token['id'],
                        'currency' => 'vnd',
                        'amount' => ($order->amount/100),
                        'description' => 'User: '.Auth::user()->name.' with ID:
                         '.Auth::user()->id,
                    ]);
                    if($charge['status'] == 'succeeded') {
                        $order->save();
                    } else {
                        return redirect()->route('checkout')->withErrors('Opps!
                             Tiền chưa thanh toán thành công');
                    }
                } catch (Exception $e) {
                    return redirect()->route('checkout')->withErrors($e->getMessage());
                } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                    return redirect()->route('checkout')->withErrors($e->getMessage());
                } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                    return redirect()->route('checkout')->withErrors($e->getMessage());
                }
            }
            if ($order->payment == "COD") {
                $order->save();
            }

            foreach ($cart->items as $key => $value) {
                $order_product = new Order_Product();
                $order_product->order_id         = $order->id;
                $order_product->product_id       = $key;
                $order_product->product_price    = $value['price']/$value['qty'];
                $order_product->product_quantity = $value['qty'];
                $order_product->product_unit     = $value['unit'];
                $order_product->save();
            }
            Session::forget('cart');
            return redirect()->back()->with('checkout_success','Đặt hàng thành công, hệ thống sẽ liên hệ qua điện thoại để xác nhận đơn hàng, xin cảm ơn quý khách!');
        }
        else{
            return redirect()->back()->withErrors('Giỏ hàng rỗng!');
        }
    }

    public function get_search(Request $request){
        $validator = Validator::make($request->all(), [
            'form_search' => 'required|regex:/^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$/'
        ], [
            'form_search.required' => 'Chưa nhập nội dung tìm kiếm',
            'form_search.regex' => 'Giá trị đầu vào không hợp lệ.'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('search_error', 'Opps! Có lỗi đầu vào.');
        }
        $keyword = $request->form_search;
        $search_data = Product::where('name','like','%'.$keyword.'%')->take(30)->get();
        $search_products = my_paginate($search_data, 8, $request);
        return view('front.pages.search', compact('search_products', 'keyword'));
    }
}
