<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\User;

class UserController extends Controller
{
//xử lý backend đăng nhập
    public function get_admin_login(){
    	if(Auth::check()){
    		if(Auth::user()->role == 'admin'){
        		return redirect()->route('get-dashboard');
        	}
            if(Auth::user()->role == 'author'){
                return redirect()->route('get-author-dashboard');
            }
            if(Auth::user()->role == 'member'){
                return redirect()->route('home');
            }
    	}
    	else{
    		return view('back.login');
    	}
    }
    public function post_admin_login(Request $request){
    	$this->validate($request,[
            'form_email'    => 'required',
            'form_password' => 'required'
        ],[
            'form_email.required' => 'Chưa nhập email.',
            'form_password.required' => 'Chưa nhập mật khẩu'
        ]);

        $credentials = array('email' => $request->form_email, 'password' => $request->form_password);
        if(Auth::attempt($credentials)){
            if(Auth::user()->role == 'admin'){
                return redirect()->route('get-dashboard');
            }
            if(Auth::user()->role == 'author'){
                return redirect()->route('get-author-dashboard');
            }
            if(Auth::user()->role == 'member'){
                return redirect()->route('home');
            }
        }
        else{
            return redirect()->route('get-login-dashboard')->withErrors('Email hoặc mật khẩu chưa chính xác');
        }
    }
    public function get_admin_logout(){
        Auth::logout();
        return redirect()->route('get-login-dashboard')->with('logout_success','Đăng xuất thành công.');
    }
//xử lý backend
    public function get_list(){
        $admin_users  = User::where('role','admin')->get();
        $author_users = User::where('role','author')->get();
        $member_users = User::where('role','member')->get();
        // User::where('status',0)->update(['status' => 1]);
        return view('back.pages.user.list',compact('admin_users','author_users','member_users'));
    }
    public function get_add(){
        return view('back.pages.user.add');
    }
    public function post_add(Request $request){
        $this->validate($request,
            [
                'form_email'       =>'required|email|unique:users,email',
                'form_password'    =>'required|min:6|max:20',
                'form_re_password' =>'required|same:form_password',
                'form_name'        =>'required|min:2|max:50',
                'form_gender'      =>'required|in:Male,Female',
                'form_role'        =>'required|in:admin,author,member',
                'form_phone'       =>'required|unique:users,phone|regex:/(0)[0-9]{9,10}/',
                'form_address'     =>'required|min:10'
            ],
            [
                'form_email.required'   =>'Email là bắt buộc',
                'form_email.email'      =>'Không đúng định dạng example@domain.com',
                'form_email.unique'     =>'Email này đã có người sử dụng',

                'form_password.required'=>'Mật khẩu là bắt buộc',
                'form_password.min'     =>'Mật khẩu tối thiểu 6 ký tự',
                'form_password.max'     =>'Mật khẩu tối đa 20 ký tự',

                'form_re_password.required' =>'Nhập lại mật khẩu',
                'form_re_password.same'     =>'Nhập lại mật khẩu chưa đúng',

                'form_name.required'=>'Họ tên là bắt buộc',
                'form_name.min'     =>'Họ tên tối thiểu 2 ký tự',
                'form_name.max'     =>'Họ tên tối đa 50 ký tự',

                'form_gender.required'  =>'Giới tính là bắt buộc',
                'form_gender.in'        =>'Sai giá trị giới tính',

                'form_role.required'  =>'Phân quyền là bắt buộc',
                'form_role.in'        =>'Sai giá trị phân quyền',

                'form_phone.required' =>'Số điện thoại sử dụng để liên hệ với bạn. Hãy nhập chính xác',
                'form_phone.regex'    =>'Số điện thoại bắt đầu bằng 0, gồm 10 hoặc 11 ký tự số',
                'form_phone.unique'   =>'Số điện thoại này đã có người sử dụng',

                'form_address.required' =>'Địa chỉ sử dụng để liên hệ với bạn. Hãy nhập chính xác',
                'form_address.min'      =>'Địa chỉ tối thiểu 10 ký tự'
            ]
        );
        $user = new User();
        $user->name     = $request->form_name;
        $user->gender   = $request->form_gender;
        $user->email    = $request->form_email;
        $user->password = Hash::make($request->form_password);
        $user->address  = $request->form_address;
        $user->phone    = $request->form_phone;
        $user->role     = $request->form_role;

        if($request->hasFile('form_image')){
            $allowed_type = array('image/jpeg', 'image/png');
            $allowed_size = '2097152';
            $dir = 'images/user/';

            $file = $request->file('form_image');

            $image_type = $file->getClientMimeType();
            $image_size = $file->getClientSize();
            if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
                $image_name = $file->getClientOriginalName();
                $image_name_random = str_random(4).'_'.$image_name;
                while(file_exists($dir.$image_name_random)){
                    $image_name_random = str_random(4).'_'.$image_name;
                }
                $file->move($dir,$image_name_random);
                $user->image = $image_name_random;
            }
            else{
                return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa 2mb');
            }
        }
        $user->save();
        return redirect()->route('all-users')->with('add_success','Thêm user mới thành công.');
    }
    public function get_edit($id){
        $current_user = User::find($id);
        $current_user->status = 1;
        $current_user->save();
        return view('back.pages.user.edit',compact('current_user'));
    }
    public function post_edit($id, Request $request){
        $this->validate($request,
            [
                'form_email'       =>'required|email|unique:users,email,'.$id,
                'form_name'        =>'required|min:2|max:50',
                'form_gender'      =>'required|in:Male,Female',
                'form_role'        =>'required|in:admin,author,member',
                'form_phone'       =>'required|unique:users,phone,'.$id.'|regex:/(0)[0-9]{9,10}/',
                'form_address'     =>'required|min:10'
            ],
            [
                'form_email.required'   =>'Email là bắt buộc',
                'form_email.email'      =>'Không đúng định dạng example@domain.com',
                'form_email.unique'     =>'Email này đã có người sử dụng',

                'form_name.required'=>'Họ tên là bắt buộc',
                'form_name.min'     =>'Họ tên tối thiểu 2 ký tự',
                'form_name.max'     =>'Họ tên tối đa 50 ký tự',

                'form_gender.required'  =>'Giới tính là bắt buộc',
                'form_gender.in'        =>'Sai giá trị giới tính',

                'form_role.required'  =>'Phân quyền là bắt buộc',
                'form_role.in'        =>'Sai giá trị phân quyền',

                'form_phone.required' =>'Số điện thoại sử dụng để liên hệ với bạn. Hãy nhập chính xác',
                'form_phone.regex'    =>'Số điện thoại bắt đầu bằng 0, gồm 10 hoặc 11 ký tự số',
                'form_phone.unique'   =>'Số điện thoại này đã có người sử dụng',

                'form_address.required' =>'Địa chỉ sử dụng để liên hệ với bạn. Hãy nhập chính xác',
                'form_address.min'      =>'Địa chỉ tối thiểu 10 ký tự'
            ]
        );

        $current_user = User::find($id);
        $current_user->name     = $request->form_name;
        $current_user->gender   = $request->form_gender;
        $current_user->email    = $request->form_email;
        $current_user->address  = $request->form_address;
        $current_user->phone    = $request->form_phone;
        $current_user->role     = $request->form_role;

        if($request->hasFile('form_image')){
            $allowed_type = array('image/jpeg', 'image/png');
            $allowed_size = '2097152';
            $dir = 'images/user/';

            $file = $request->file('form_image');

            $image_type = $file->getClientMimeType();
            $image_size = $file->getClientSize();
            if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
                $image_name = $file->getClientOriginalName();
                $image_name_random = str_random(4).'_'.$image_name;
                while(file_exists($dir.$image_name_random)){
                    $image_name_random = str_random(4).'_'.$image_name;
                }
                $file->move($dir,$image_name_random);
                unlink($dir.$current_user->image);
                $current_user->image = $image_name_random;
            }
            else{
                return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa 2mb');
            }
        }
        $current_user->save();
        return redirect()->route('user-edit',$id)->with('edit_success','Chỉnh sửa người dùng thành công');
    }
    public function get_delete($id){
        $current_user = User::find($id);
        $dir = 'images/user/';
        $current_user->delete();
        if($current_user->image != 'default-avatar.jpg'){
            unlink($dir.$current_user->image);
        }
        return redirect()->route('all-users')->with('delete_success','Xóa người dùng thành công.');
    }
}
