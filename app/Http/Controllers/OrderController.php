<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Order_Product;
use App\Product;

class OrderController extends Controller
{
    public function get_list(){
    	$orders = Order::all();
        // Order::where('status',1)->update(['status' => 0]);
    	return view('back.pages.order.list', compact('orders'));
    }
    public function get_processed(){
    	$orders = Order::where('active',1)->get();
    	return view('back.pages.order.processed', compact('orders'));
    }
    public function get_ship(){
        $orders = Order::where('active',2)->get();
        return view('back.pages.order.shipping', compact('orders'));
    }
    public function get_success(){
        $orders = Order::where('active',3)->get();
        $from = '';
        $to = '';
        return view('back.pages.order.success', compact('orders','from','to'));
    }
    public function get_no_process(){
    	$orders = Order::where('active',0)->get();
       
    	return view('back.pages.order.no_process', compact('orders'));
    }
    public function search(Request $rq)
    {
        $orders = Order::where('active',3)->whereBetween('updated_at', array($rq->from, $rq->to))->get();
        $from = $rq->from;
        $to = $rq->to;
        return view('back.pages.order.success', compact('orders','from','to'));
    }
    public function statistical()
    {
        $orders = Order::where('active',3)->get();
        return view('back.pages.order.statistical', compact('orders'));
    }
    public function get_details($id){
    	$current_order = Order::find($id);
        $current_order->status = 1;
        $current_order->save();
    	$order_details = Order_Product::where('order_id',$id)->get();
    	return view('back.pages.order.details', compact('current_order','order_details'));
    }
    public function get_delete($id){
        $current_order = Order::find($id);
        $order_product = Order_Product::where('order_id',$current_order->id)->get();
        foreach($order_product as $op){
            $op->delete();
        }
        $current_order->delete();
        return redirect()->back()->with('delete_success','Xóa đơn hàng thành công');
    }
    public function active($id)
    {
        $current_order = Order::find($id);
        if ( $current_order->active == 0) {
            $current_order->active = 1;
            $current_order->status = 0;
        }
        else if($current_order->active == 1)
        {
            $current_order->active = 2;
            $current_order->status = 0;
        }
        else if($current_order->active == 2)
        {
            $current_order->active = 3;
        }
        
        $order_details = Order_Product::where('order_id',$id)->get();
        foreach ($order_details as $key => $value) {

            $product = Product::find($value->product_id);
            $number = $product->number - $value->product_quantity;
            $product->number = $number;
            $product->save();
        }
        
        $current_order->save();
        if ( $current_order->active == 1) {
            return redirect()->route('order-processed')->with('delete_success','Xác nhận đơn hàng thành công');
        }
        else if($current_order->active == 2)
        {
            return redirect()->route('order-ship')->with('delete_success','Xác nhận đơn hàng thành công');
        }
        else if($current_order->active == 3)
        {
           return redirect()->route('order-success')->with('delete_success','Xác nhận đơn hàng thành công');
        }
       
    }
}
