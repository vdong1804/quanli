<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;

class SlideController extends Controller
{
    public function get_list(){
    	$active_slides 	= Slide::where('active',1)->orderBy('id', 'desc')->get();
    	$no_active_slides = Slide::where('active',0)->orderBy('id', 'desc')->get();
    	return view('back.pages.slide.list',compact('active_slides','no_active_slides'));
    }
    public function get_add(){
    	return view('back.pages.slide.add');
    }
    public function post_add(Request $request){
    	$this->validate($request,
            [
                'form_title'  =>'required|min:2|max:50|unique:slides,title',
                'form_active' =>'required|in:1,0',
                'form_image'  =>'required'
            ],
            [
                'form_title.required'=>'Tiêu đề slide là bắt buộc',
                'form_title.min'     =>'Tiêu đề slide tối thiểu 2 ký tự',
                'form_title.max'     =>'Tiêu đề slide tối đa 50 ký tự',
                'form_title.unique'  =>'Tiêu đề đã tồn tại',

                'form_active.required'  =>'Trạng thái active của slide là bắt buộc',
                'form_active.in'        =>'Sai giá trị trạng thái active',

                'form_image.required'  =>'Hình ảnh là bắt buộc'
            ]
        );

    	$allowed_type = array('image/jpeg', 'image/png');
        $allowed_size = '2097152';
        $dir = 'images/slide/';

        $file = $request->file('form_image');

        $image_type = $file->getClientMimeType();
        $image_size = $file->getClientSize();

        if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
        	$slide = new Slide();
        	$slide->title  = $request->form_title;
        	$slide->slug   = str_slug($request->form_title);
        	$slide->active = $request->form_active;

        	$image_name = $file->getClientOriginalName();
            $image_name_random = str_random(4).'_'.$image_name;
            while(file_exists($dir.$image_name_random)){
                $image_name_random = str_random(4).'_'.$image_name;
            }
            $file->move($dir,$image_name_random);
            $slide->image = $image_name_random;

            $slide->save();
            return redirect()->route('all-slides')->with('add_success','Thêm slide mới thành công.');
        }
        else{
            return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa 2mb');
        }
    }
    public function get_edit($id){
    	$current_slide = Slide::find($id);
    	return view('back.pages.slide.edit',compact('current_slide'));
    }
    public function post_edit($id, Request $request){
    	$this->validate($request,
            [
                'form_title'  =>'required|min:2|max:50|unique:slides,title,'.$id,
                'form_active' =>'required|in:1,0'
            ],
            [
                'form_title.required'=>'Tiêu đề slide là bắt buộc',
                'form_title.min'     =>'Tiêu đề slide tối thiểu 2 ký tự',
                'form_title.max'     =>'Tiêu đề slide tối đa 50 ký tự',
                'form_title.unique'  =>'Tiêu đề đã tồn tại',

                'form_active.required'  =>'Trạng thái active của slide là bắt buộc',
                'form_active.in'        =>'Sai giá trị trạng thái active'
            ]
        );
        $current_slide = Slide::find($id);
    	$current_slide->title  = $request->form_title;
    	$current_slide->slug   = str_slug($request->form_title);
    	$current_slide->active = $request->form_active;
    	if($request->hasFile('form_image')){
    		$allowed_type = array('image/jpeg', 'image/png');
	        $allowed_size = '2097152';
	        $dir = 'images/slide/';

	        $file = $request->file('form_image');

	        $image_type = $file->getClientMimeType();
	        $image_size = $file->getClientSize();

	        if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
	        	$image_name = $file->getClientOriginalName();
	            $image_name_random = str_random(4).'_'.$image_name;

	            while(file_exists($dir.$image_name_random)){
	                $image_name_random = str_random(4).'_'.$image_name;
	            }

	            $file->move($dir,$image_name_random);
	            unlink($dir.$current_slide->image);
	            $current_slide->image = $image_name_random;
	        }
	        else{
	            return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa 2mb');
	        }
    	}
    	$current_slide->save();
    	return redirect()->route('slide-edit',$id)->with('edit_success','Chỉnh sửa slide thành công.');
    }
    public function get_delete($id){
    	$current_slide = Slide::find($id);
        $dir = 'images/slide/';
        $current_slide->delete();
        unlink($dir.$current_slide->image);
        return redirect()->route('all-slides')->with('delete_success','Xóa Slide thành công.');
    }
}
