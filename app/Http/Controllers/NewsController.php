<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Category;
use Auth;
use App\Events\ApproveNewsEvent;

class NewsController extends Controller
{
    function __construct()
    {
        $categories = Category::all();
        view()->share('categories',$categories);
    }
    // admin
    public function HandlePublished()
    {
        $news = News::where('active', 2)->orderBy('created_at', 'desc')->get();
         return view('back.pages.news.admin.published', compact('news'));
    }
    public function HandlePending()
    {
        $news = News::where('active',1)->orderBy('created_at', 'desc')->get();
        return view('back.pages.news.admin.pending', compact('news'));
    }
    public function ApproveNews($id)
    {
        $currentNews = News::find($id);
        $currentNews->active = 2;
        $currentNews->save();
        event(new ApproveNewsEvent($currentNews->title, $currentNews->to_user->id));
        return redirect()->back()->with('approve_success','Phê duyệt bài viết thành công');
    }
    // author
    public function getList()
    {
        $news = News::where('user_id',Auth::user()->id)->orderBy('created_at', 'desc')->get();
        return view('back.pages.news.list', compact('news'));
    }
    public function getPublished()
    {
        $news = News::where('user_id',Auth::user()->id)->where('active',2)->orderBy('created_at', 'desc')->get();
        return view('back.pages.news.published', compact('news'));
    }
    public function getPending()
    {
        $news = News::where('user_id',Auth::user()->id)->where('active',1)->orderBy('created_at', 'desc')->get();
        return view('back.pages.news.pending', compact('news'));
    }
    public function getDraft()
    {
        $news = News::where('user_id',Auth::user()->id)->where('active',0)->orderBy('created_at', 'desc')->get();
        return view('back.pages.news.draft', compact('news'));
    }
    public function getPreview($id)
    {
        $currentNews = News::find($id);
        return view('front.pages.news.preview', compact('currentNews'));
    }
    public function getAdd()
    {
        return view('back.pages.news.add');
    }
    public function postAdd(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:news,title|min:7|max:70',
            'slug' => 'required|unique:news,slug|min:3|max:70',
            'content' => 'required',
            'image' => 'required',
            'active' => 'required|in:0,1'
        ],[
            'title.required'=>'Chưa nhập Tiêu Đề.',
            'title.unique'=>'Tiêu đề này đã tồn tại.',
            'title.min'=>'Tiêu đề ít nhất 7 ký tự.',
            'title.max'=>'Tiêu đề tối đa 70 ký tự.',

            'slug.required'=>'Chưa có Permalink.',
            'slug.unique'=>'Permalink này đã tồn tại.',
            'slug.min'=>'Permalink ít nhất 3 ký tự.',
            'slug.max'=>'Permalink tối đa 70 ký tự.',

            'content.required'=>'Chưa nhập nội dung bài viết.',
            'image.required'=>'Chưa chọn Feature image.',

            'active.required'=>'Chưa chọn trạng thái bài viết.',
            'active.in'=>'Sai giá trị active.'
        ]);
        $news = new News();
        $news->user_id = Auth::user()->id;
        $news->title = $request->title;
        $news->slug = $request->slug;
        $news->content = $request->content;

        $allowed_type = array('image/jpeg', 'image/png');
        $allowed_size = '2097152';
        $dir = 'images/news/';

        $image = $request->file('image');

        $image_type = $image->getClientMimeType();
        $image_size = $image->getClientSize();
        if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
            $image_name = $image->getClientOriginalName();
            $image_name_random = str_random(4).'_'.$image_name;
            while(file_exists($dir.$image_name_random)){
                $image_name_random = str_random(4).'_'.$image_name;
            }
            $image->move($dir,$image_name_random);
            $news->image = $image_name_random;
        }
        else {
            return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa 2mb');
        }

        $news->active = $request->active;
        if ($news->active == "0") {
            $notice = "Bài viết được lưu nháp thành công.";
        } elseif ($news->active == "1") {
            $notice = "Yêu cầu publish bài viết thành công, đang chờ admin phê duyệt.";
            //xử lý pusher ở đây
        }
        $news->save();
        return redirect()->route('edit-news', $news->id)->with('add_success',$notice);
    }
    public function getEdit($id)
    {
        $currentNews = News::find($id);
        return view('back.pages.news.edit',compact('currentNews'));
    }
    public function postEdit($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:news,title,'.$id.'|min:7|max:70',
            'slug' => 'required|unique:news,slug,'.$id.'|min:3|max:70',
            'content' => 'required',
            'active' => 'required|in:0,1'
        ],[
            'title.required'=>'Chưa nhập Tiêu Đề.',
            'title.unique'=>'Tiêu đề này đã tồn tại.',
            'title.min'=>'Tiêu đề ít nhất 7 ký tự.',
            'title.max'=>'Tiêu đề tối đa 70 ký tự.',

            'slug.required'=>'Chưa có Permalink.',
            'slug.unique'=>'Permalink này đã tồn tại.',
            'slug.min'=>'Permalink ít nhất 3 ký tự.',
            'slug.max'=>'Permalink tối đa 70 ký tự.',

            'content.required'=>'Chưa nhập nội dung bài viết.',

            'active.required'=>'Chưa chọn trạng thái bài viết.',
            'active.in'=>'Sai giá trị active.'
        ]);

        $currentNews = News::find($id);
        $currentNews->user_id = Auth::user()->id;
        $currentNews->title = $request->title;
        $currentNews->slug = $request->slug;
        $currentNews->content = $request->content;

        if ($request->hasFile('image')) {
            $allowed_type = array('image/jpeg', 'image/png');
            $allowed_size = '2097152';
            $dir = 'images/news/';

            $image = $request->file('image');

            $image_type = $image->getClientMimeType();
            $image_size = $image->getClientSize();
            if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
                $image_name = $image->getClientOriginalName();
                $image_name_random = str_random(4).'_'.$image_name;
                while(file_exists($dir.$image_name_random)){
                    $image_name_random = str_random(4).'_'.$image_name;
                }
                $image->move($dir,$image_name_random);
                unlink($dir.$currentNews->image);
                $currentNews->image = $image_name_random;
            }
            else {
                return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa 2mb');
            }
        }

        $currentNews->active = $request->active;
        if ($currentNews->active == "0") {
            $notice = "Bài viết được lưu nháp thành công.";
        } elseif ($currentNews->active == "1") {
            $notice = "Yêu cầu publish bài viết thành công, đang chờ admin phê duyệt.";
            //xử lý pusher ở đây
        }
        $currentNews->save();
        return redirect()->route('edit-news', $id)->with('edit_success',$notice);
    }
    public function getDelete($id)
    {
        $currentNews = News::find($id);
        $dir = 'images/news/';
        $currentNews->delete();
        unlink($dir.$currentNews->image);
        return redirect()->back()->with('delete_success','Xóa bài viết thành công.');
    }
    public function getAdminDelete($id)
    {
        $currentNews = News::find($id);
        $dir = 'images/news/';
        $currentNews->delete();
        unlink($dir.$currentNews->image);
        return redirect()->back()->with('delete_success','Xóa bài viết thành công.');
    }
}
