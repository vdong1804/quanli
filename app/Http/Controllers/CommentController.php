<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    public function get_list(){
    	$comments = Comment::orderBy('id', 'desc')->get();
    	return view('back.pages.comment.list', compact('comments'));
    }
    public function get_delete($id){
    	$current_comment = Comment::find($id);
    	$current_comment->delete();
    	return redirect()->route('all-comments')->with('delete_success', 'Xóa comment thành công.');
    }
}
