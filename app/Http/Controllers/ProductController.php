<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function get_list(){
    	$products = Product::all();
    	return view('back.pages.product.list',compact('products'));
    }
    public function get_details($id){
    	$current_product = Product::find($id);
    	return view('back.pages.product.details',compact('current_product'));
    }
    public function get_add(){
    	$parent_categories = Category::whereNull('parent_id')->get();
    	$child_categories  = Category::whereNotNull('parent_id')->get();
    	return view('back.pages.product.add',compact('parent_categories','child_categories'));
    }
    public function post_add(Request $request){
    	$this->validate($request,[
    			'form_category' 	=>'required|numeric',
    			'form_name'			=>'required|unique:products,name|min:2|max:50',
    			'form_price'		=>'required|numeric',
                'number'        =>'required|numeric',
    			'form_discount' 	=>'numeric',
    			'form_unit'			=>'required|min:2|max:10',
    			// 'form_description'	=>'',
    			'form_images'		=>'required'
    		],[
    			'form_category.required'=>'Chưa chọn loại sản phẩm.',
    			'form_category.numeric' =>'Loại sản phẩm yêu cầu ký tự số.',

    			'form_name.required'=>'Chưa nhập tên sản phẩm.',
    			'form_name.unique'	=>'Tên sản phẩm đã tồn tại.',
    			'form_name.min'		=>'Tên sản phẩm giới hạn 2-30 ký tự.',
    			'form_name.max'		=>'Tên sản phẩm giới hạn 2-30 ký tự.',

    			'form_price.required'=>'Chưa nhập giá gốc sản phẩm.',
    			'form_price.numeric' =>'Giá gốc sản phẩm yêu cầu ký tự số.',
                'number.numeric' =>'số lượng yêu cầu ký tự số.',

    			'form_discount.numeric'=>'Giá khuyến mãi sản phẩm yêu cầu ký tự số.',

    			'number.required'=>'Chưa nhập số lượng sản phẩm.',
                'form_unit.required'=>'Chưa nhập đơn vị sản phẩm.',
    			'form_unit.min'		=>'Đơn vị sản phẩm giới hạn 2-10 ký tự.',
    			'form_unit.max'		=>'Đơn vị sản phẩm giới hạn 2-10 ký tự.',

    			'form_images.required'=>'Chưa upload ảnh sản phẩm.'
    		]);

    	if($request->hasFile('form_images')){
    		$allowed_type = array('image/jpeg', 'image/png');
            $allowed_size = '2097152';
            $dir = 'images/product/';

	    	$images = $request->file('form_images');

	    	if(count($images) == 3){
	    		$images_name_array = array();
		    	foreach ($images as $image) {
		    		if(in_array($image->getClientMimeType(), $allowed_type) && ($image->getClientSize() <= $allowed_size))
		    		{
		    			$image_name_random = str_random(4).'_'.$image->getClientOriginalName();
		    			while(file_exists($dir.$image_name_random)){
		                    $image_name_random = str_random(4).'_'.$image->getClientOriginalName();
		                }
		    			array_push($images_name_array, $image_name_random);
		    			$image->move($dir,$image_name_random);
		    		}
		    		else{
		    			return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa mỗi ảnh 2mb.');
		    		}
		    	}
		    	$product = new Product();
		    	$product->category_id = $request->form_category;
		    	$product->name = $request->form_name;
		    	$product->slug = str_slug($request->form_name);
		    	$product->price = $request->form_price;
		    	$product->discount = $request->form_discount;
		    	$product->unit = $request->form_unit;
		    	$product->description = $request->form_description;
		    	$product->image = json_encode($images_name_array);
                $product->number = $request->number;
		    	$product->save();
		    	return redirect()->route('product-details',$product->id)->with('add_success','Thêm sản phẩm thành công');
		    }
		    else{
		    	return redirect()->back()->withInput()->withErrors('Yêu cầu chọn 3 ảnh cùng 1 lúc khi upload.');
		    }
    	}
    	else{
    		return redirect()->back()->withInput()->withErrors('Chưa upload ảnh sản phẩm');
    	}
    }
    public function get_edit($id){
    	$parent_categories = Category::whereNull('parent_id')->get();
    	$child_categories  = Category::whereNotNull('parent_id')->get();
    	$current_product   = Product::find($id);
    	return view('back.pages.product.edit',compact('parent_categories','child_categories','current_product'));
    }
    public function post_edit($id, Request $request){
    	$this->validate($request,[
				'form_category' 	=>'required|numeric',
				'form_name'			=>'required|unique:products,name,'.$id.'|min:2|max:50',
				'form_price'		=>'required|numeric',
                'number'        =>'required|numeric',
				'form_discount' 	=>'numeric',
				'form_unit'			=>'required|min:2|max:10'
			],[
				'form_category.required'=>'Chưa chọn loại sản phẩm.',
				'form_category.numeric' =>'Loại sản phẩm yêu cầu ký tự số.',

				'form_name.required'=>'Chưa nhập tên sản phẩm.',
				'form_name.unique'	=>'Tên sản phẩm đã tồn tại.',
				'form_name.min'		=>'Tên sản phẩm giới hạn 2-30 ký tự.',
				'form_name.max'		=>'Tên sản phẩm giới hạn 2-30 ký tự.',

				'form_price.required'=>'Chưa nhập giá gốc sản phẩm.',
				'form_price.numeric' =>'Giá gốc sản phẩm yêu cầu ký tự số.',
                'number.numeric' =>'số lượng yêu cầu ký tự số.',

				'form_discount.numeric'=>'Giá khuyến mãi sản phẩm yêu cầu ký tự số.',
                'number.required'=>'Chưa nhập số lượng sản phẩm.',
				'form_unit.required'=>'Chưa nhập đơn vị sản phẩm.',
				'form_unit.min'		=>'Đơn vị sản phẩm giới hạn 2-10 ký tự.',
				'form_unit.max'		=>'Đơn vị sản phẩm giới hạn 2-10 ký tự.'
			]);
    	$current_product = Product::find($id);
    	$current_product->category_id = $request->form_category;
    	$current_product->name 		  = $request->form_name;
    	$current_product->slug 		  = str_slug($request->form_name);
    	$current_product->price 	  = $request->form_price;
    	$current_product->discount 	  = $request->form_discount;
    	$current_product->unit 		  = $request->form_unit;
    	$current_product->description = $request->form_description;
        $current_product->number = $request->number;
    	$allowed_type = array('image/jpeg', 'image/png');
        $allowed_size = '2097152';
        $dir = 'images/product/';

        if($request->hasFile('form_images_0')){
        	$current_images = json_decode($current_product->image);
        	$image_0 	= $request->file('form_images_0');
        	$image_type = $image_0->getClientMimeType();
            $image_size = $image_0->getClientSize();
        	if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
        		$image_name_random = str_random(4).'_'.$image_0->getClientOriginalName();
    			while(file_exists($dir.$image_name_random)){
                    $image_name_random = str_random(4).'_'.$image_0->getClientOriginalName();
                }
                $image_0->move($dir,$image_name_random);
                //unlink($dir.$current_images[0]);
                $current_product->image = json_encode([$image_name_random,$current_images[1],$current_images[2]]);
        	}
        	else{
    			return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa mỗi ảnh 2mb.');
    		}
        }
        if($request->hasFile('form_images_1')){
        	$current_images = json_decode($current_product->image);
        	$image_1 	= $request->file('form_images_1');
        	$image_type = $image_1->getClientMimeType();
            $image_size = $image_1->getClientSize();
        	if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
        		$image_name_random = str_random(4).'_'.$image_1->getClientOriginalName();
    			while(file_exists($dir.$image_name_random)){
                    $image_name_random = str_random(4).'_'.$image_1->getClientOriginalName();
                }
                $image_1->move($dir,$image_name_random);
                //unlink($dir.$current_images[1]);
                $current_product->image = json_encode([$current_images[0],$image_name_random,$current_images[2]]);
        	}
        	else{
    			return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa mỗi ảnh 2mb.');
    		}
        }
        if($request->hasFile('form_images_2')){
        	$current_images = json_decode($current_product->image);
        	$image_2 	= $request->file('form_images_2');
        	$image_type = $image_2->getClientMimeType();
            $image_size = $image_2->getClientSize();
        	if (in_array($image_type, $allowed_type) && ($image_size <= $allowed_size)) {
        		$image_name_random = str_random(4).'_'.$image_2->getClientOriginalName();
    			while(file_exists($dir.$image_name_random)){
                    $image_name_random = str_random(4).'_'.$image_2->getClientOriginalName();
                }
                $image_2->move($dir,$image_name_random);
                //unlink($dir.$current_images[2]);
                $current_product->image = json_encode([$current_images[0],$current_images[1],$image_name_random]);
        	}
        	else{
    			return redirect()->back()->withInput()->withErrors('Chỉ cho phép ảnh jpg/png với dung lượng tối đa mỗi ảnh 2mb.');
    		}
        }
        $current_product->save();
        return redirect()->route('product-details',$current_product->id)->with('edit_success','Chỉnh sửa sản phẩm thành công');
    }
    public function get_delete($id){
    	$current_product = Product::find($id);

    	$dir = 'images/product/';
    	$img = json_decode($current_product->image);

    	for($i=0; $i<count($img); $i++){
    		unlink($dir.$img[$i]);
    	}
    	$current_product->delete();
    	return redirect()->route('all-products')->with('delete_success','Xóa sản phẩm thành công.');
    }
}
