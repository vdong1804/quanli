<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function get_list(){
    	$parent_categories = Category::whereNull('parent_id')->orderBy('id','desc')->get();
    	$child_categories  = Category::whereNotNull('parent_id')->orderBy('id','desc')->get();
    	return view('back.pages.category.list',compact('parent_categories','child_categories'));
    }
    public function post_add(Request $request){
    	$this->validate($request,[
    			'form_name'			=>'required|unique:categories,name|min:2|max:30'
    			// 'form_description'	=>''
    		],[
    			'form_name.required'=>'Chưa nhập tên danh mục.',
    			'form_name.unique'	=>'Tên danh mục đã tồn tại.',
    			'form_name.min'		=>'Tên danh mục giới hạn 2-30 ký tự.',
    			'form_name.max'		=>'Tên danh mục giới hạn 2-30 ký tự.'
    		]);
    	if(isset($request->form_category)){
    		$this->validate($request,[
    			'form_category' 	=>'required|numeric'
    		],[
    			'form_category.required'=>'Chưa chọn loại sản phẩm.',
    			'form_category.numeric' =>'Loại sản phẩm yêu cầu ký tự số.'
    		]);
    	}
    	$category = new Category();
    	$category->parent_id   = $request->form_category;
    	$category->name 	   = $request->form_name;
    	$category->slug 	   = str_slug($request->form_name);
    	$category->description = $request->form_description;
    	$category->save();
    	return redirect()->route('all-categories')->with('add_success', 'Thêm danh mục mới thành công.');
    }
    public function get_edit($id){
    	$parent_categories = Category::whereNull('parent_id')->orderBy('created_at','desc')->get();
    	$child_categories  = Category::whereNotNull('parent_id')->orderBy('created_at','desc')->get();
    	$current_category  = Category::find($id);
    	// dd($current_category->parent_id);
    	return view('back.pages.category.edit',compact('parent_categories','child_categories','current_category'));
    }
    public function post_edit($id, Request $request){
    	$this->validate($request,[
    			'form_name'			=>'required|unique:categories,name,'.$id.'|min:2|max:30'
    			// 'form_description'	=>''
    		],[
    			'form_name.required'=>'Chưa nhập tên danh mục.',
    			'form_name.unique'	=>'Tên danh mục đã tồn tại.',
    			'form_name.min'		=>'Tên danh mục giới hạn 2-30 ký tự.',
    			'form_name.max'		=>'Tên danh mục giới hạn 2-30 ký tự.'
    		]);

    	$category = Category::find($id);

    	if(isset($request->form_category)){
    		$this->validate($request,[
    			'form_category' 	=>'required|numeric'
    		],[
    			'form_category.required'=>'Chưa chọn loại sản phẩm.',
    			'form_category.numeric' =>'Loại sản phẩm yêu cầu ký tự số.'
    		]);

    		$category->parent_id   = $request->form_category;
    	}

    	$category->name 	   = $request->form_name;
    	$category->slug 	   = str_slug($request->form_name);
    	$category->description = $request->form_description;
    	$category->save();
    	return redirect()->route('category-edit',$category->id)->with('edit_success', 'Chỉnh sửa danh mục thành công.');
    }
    public function get_delete($id){
    	$category = Category::find($id);
    	if($category->parent_id == NULL){
    		if(count($category->get_children)>0){
    			return redirect()->back()->with('delete_failed', 'Yêu cầu xóa hết các danh mục con trước khi thực hiện xóa danh mục cha.');
    		}
    	}
    	$category->delete();
    	return redirect()->route('all-categories')->with('delete_success', 'Xóa danh mục thành công.');
    }
}
