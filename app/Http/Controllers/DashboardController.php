<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Order;
use App\News;
use App\Slide;
use App\Comment;
use App\User;
use Auth;

class DashboardController extends Controller
{
    public function get_dashboard(){
        $total_categories = count(Category::all());
    	$total_products   = count(Product::all());
    	$total_orders 	  = count(Order::all());
    	$total_news 	  = count(News::where('active', 2)->get());
    	$total_slides 	  = count(Slide::all());
    	$total_comments   = count(Comment::all());
    	$total_users 	  = count(User::all());
    	$total_revenue	  = 0;
        
    	foreach (Order::all() as $key) {
    		$total_revenue += $key->amount;
    	}
    	return view('back.pages.dashboard.dashboard',compact('total_categories','total_products','total_orders','total_news','total_slides','total_comments','total_users','total_revenue'));
    }
    public function get_author_dashboard(){
        $total_news = count(News::where('user_id', Auth::user()->id)->get());
    	return view('back.pages.dashboard.author_dashboard', compact('total_news'));
    }
}
