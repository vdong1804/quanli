<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    //1 Category có thể có nhiều sản phẩm
    public function to_product(){
        return $this->hasMany('App\Product', 'category_id', 'id');
    }

    //1 Category có thể có 1 thằng category cha hoặc không
    public function get_parent(){
    	return $this->belongsTo(Category::class, 'parent_id');
    }

    //1 Category có thể có nhiều thằng category con
    public function get_children(){
    	return $this->hasMany(Category::class, 'parent_id');
    	// return $this->hasMany($this, 'parent_id');
    }
}
