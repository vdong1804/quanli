<?php
// https://trungquandev.com/
// Mở composer.json
// Thêm vào trong "autoload" chuỗi sau
// "files": [
//         "app/tq17_function/paginate.php"
// ]

// Chạy cmd : composer dumpautoload

//Đây là cách thêm 1 function bên ngoài vào trong project Laravel
//Từ nay về sau có thể dùng bất cứ function trong này ở bất cứ đâu Laravel mà không phải use nữa
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

	function my_paginate($data = array(), $perPage, $request){
		$currentPage = LengthAwarePaginator::resolveCurrentPage();

		$collection = new Collection($data);

		$currentPageSearchResults = $collection->slice(($currentPage-1) * $perPage, $perPage)->all();

		$paginatedSearchResults= new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
		$paginatedSearchResults->setPath($request->url());
		$paginatedSearchResults->appends($request->except(['page']));

		return $paginatedSearchResults;
	}
?>