<?php

namespace App;

class Cart
{
	public $items = NULL;
	public $totalQty = 0;
	public $totalPrice = 0;

	public function __construct($oldCart){
		if($oldCart){
			$this->items 	  = $oldCart->items;
			$this->totalQty   = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
		}
	}
	//thêm sản phẩm vào giỏ hàng này
	public function add($item, $id, $qty = NULL){
		$price_or_discount = $item->price;
		if($item->discount != 0){
			$price_or_discount = $item->discount;
		}

		$cart = ['qty'=>0, 'price' => $price_or_discount, 'unit'=>$item->unit, 'item' => $item];
		if($this->items){
			if(array_key_exists($id, $this->items)){
				$cart = $this->items[$id];
			}
		}

		if($qty != NULL){
			$cart['qty']   += $qty;
			$cart['price']  = $price_or_discount * $cart['qty'];

			if($this->items){
				if(array_key_exists($id, $this->items)){
					$this->removeItem($id); //xóa item này đi rồi làm mới lại (bug o.O)
					$this->totalQty   += $cart['qty'];
					$this->totalPrice += $cart['price'];
					$this->items[$id]  = $cart;
				}
				else{
					$this->totalQty   += $cart['qty'];
					$this->totalPrice += $cart['price'];
					$this->items[$id]  = $cart;
				}
			}
			else{
				$this->totalQty   += $cart['qty'];
				$this->totalPrice += $cart['price'];
				$this->items[$id]  = $cart;
			}
		}
		else{
			$cart['qty']++;
			$cart['price'] = $price_or_discount * $cart['qty'];

			$this->totalQty++;
			$this->totalPrice += $price_or_discount;
			$this->items[$id]  = $cart;
		}
	}
	//giảm số lượng đi 1
	public function reduceByOne($id){
		$this->items[$id]['qty']--;
		if($this->items[$id]['item']['discount'] > 0){
			$this->items[$id]['price'] -= $this->items[$id]['item']['discount'];
			$this->totalPrice		   -= $this->items[$id]['item']['discount'];
		}
		else{
			$this->items[$id]['price'] -= $this->items[$id]['item']['price'];
			$this->totalPrice		   -= $this->items[$id]['item']['price'];
		}
		$this->totalQty--;

		if($this->items[$id]['qty']<=0){
			unset($this->items[$id]);
		}
	}
	//xóa nhiều
	public function removeItem($id){
		$this->totalQty   -= $this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['price'];
		unset($this->items[$id]);
	}
}
