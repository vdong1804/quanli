<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;
use App\Category;
use App\Cart;
use App\News;
use App\User;
use App\Order;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        $total_new_order = count(Order::where([['status',0],['active',0]])->get());
        View::share('total_new_order',$total_new_order);
        $total_new_user = count(User::where('status',0)->get());
        View::share('total_new_user',$total_new_user);
        Schema::defaultStringLength(191);

        view()->composer(['front.layout.header','front.pages.checkout'], function($view){
            if(Session('cart')){
                $old_cart = Session::get('cart');
                $cart = new Cart($old_cart);
                $view->with([
                    'cart'          => Session::get('cart'),
                    'products_cart'  => $cart->items,
                    'totalPrice'    => $cart->totalPrice,
                    'totalQty'      => $cart->totalQty
                ]);
            }
        });

        view()->composer(['back.layout.slide_menu'], function($view){
            $pendingNews = count(News::where('active',1)->get());
            if ($pendingNews>0) {
                $view->with([
                    'pendingNews' => $pendingNews
                ]);
            }
        });

        view()->composer(['front.layout.menu','front.layout.footer'], function($view){
            $bakeryCategories = Category::where('parent_id',1)->orderBy('created_at','desc')->get();
            $fruitCategories = Category::where('parent_id',2)->orderBy('created_at','desc')->get();
            $categoriesFooter = Category::whereNotNull('parent_id')->orderBy('created_at','desc')->limit(7)->get();
            $newsFooter = News::where('active', 2)->orderBy('created_at','desc')->limit(4)->get();
            $view->with([
                'bakeryCategories' => $bakeryCategories,
                'fruitCategories' => $fruitCategories,
                'categoriesFooter' => $categoriesFooter,
                'newsFooter' => $newsFooter
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
