<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    //1 product thuộc về 1 category
    public function to_category(){
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    //1 product có thể có nhiều comment
    public function to_comment(){
        return $this->hasMany('App\Comment', 'product_id', 'id');
    }

    //product và order, quan hệ nhiều nhiều
    public function to_order(){
    	return $this->belongsToMany('App\Order');
    	//câu trên tương tự câu dưới với tên của pivot table đã đặt theo chuẩn
    	// return $this->belongsToMany('App\Order','order_product','product_id', 'order_id');
    }
}
