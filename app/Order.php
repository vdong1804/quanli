<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'orders';
	protected $fillable = ['form_name','form_email','form_address','form_phone'];
    //order và product, quan hệ nhiều nhiều
    public function to_product(){
    	return $this->belongsToMany('App\Product');
    }

    //1 order thuộc về 1 user
    public function to_user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
