<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_Product extends Model
{
    protected $table = 'order_product';

    //1 order_product thuộc về 1 product
    public function to_product(){
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
}
