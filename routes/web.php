<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/forget-session', function(){
    Session::flush();
    return redirect()->back();
});
Route::get('/all-session', function(){
    $session = Session::all();
    // $session = Auth::check();
    // $session = Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
    dd($session);
});
// Nhóm xử lý front-end
    Route::get('/', [
        'as'  => 'home',
        'uses'=> 'FrontEndController@get_home'
    ]);
    Route::get('category/{slug}.html', [
        'as'  => 'category-product',
        'uses'=> 'FrontEndController@get_category_product'
    ]);
    Route::get('detail/{slug}.html', [
        'as'  => 'product-detail',
        'uses'=> 'FrontEndController@get_product_detail'
    ]);
    Route::get('show-comment/{productID}', [
        'as'  => 'show-comment',
        'uses'=> 'FrontEndController@get_show_comment'
    ]);
    Route::post('add-comment/{productID}',[
        'as'   => 'comment-add-new',
        'uses' => 'FrontEndController@post_add_comment'
    ]);

    Route::get('news', [
        'as'  => 'show-all-news-frontend',
        'uses'=> 'FrontEndController@showNews'
    ]);
    Route::get('news/{slug}.html', [
        'as'  => 'read-news-frontend',
        'uses'=> 'FrontEndController@readNews'
    ]);

    Route::get('about', [
        'as'   => 'about-us',
        'uses' => 'FrontEndController@get_about'
    ]);
    Route::get('contact', [
        'as'   => 'contact-us',
        'uses' => 'FrontEndController@get_contact'
    ]);

    Route::get('register', [
        'as'   => 'register',
        'uses' => 'FrontEndController@get_register'
    ]);
    Route::post('register', [
        'as'   => 'register',
        'uses' => 'FrontEndController@post_register'
    ]);

    Route::get('login', [
        'as'   => 'login',
        'uses' => 'FrontEndController@get_login'
    ]);
    Route::post('login', [
        'as'   => 'login',
        'uses' => 'FrontEndController@post_login'
    ]);

    Route::get('logout', [
        'as'   => 'logout',
        'uses' => 'FrontEndController@get_logout'
    ]);
    Route::get('profile.html',[
        'as' => 'profile',
        'uses' => 'FrontEndController@getProfile'
    ]);
    Route::get('order-detail/{orderProductId}',[
        'as' => 'profile-order-detail',
        'uses' => 'FrontEndController@getProfileOrderDetail'
    ]);

    Route::get('add-to-cart/{id}',[
        'as'   => 'add-to-cart',
        'uses' => 'FrontEndController@get_add_to_cart'
    ]);
    Route::get('delete-item-cart/{id}',[
        'as'   => 'delete-item-cart',
        'uses' => 'FrontEndController@get_delete_item_cart'
    ]);
    Route::get('reduce-item-cart/{id}',[
        'as'   => 'reduce-item-cart',
        'uses' => 'FrontEndController@get_reduce_item_cart'
    ]);
    Route::get('checkout',[
        'as'   => 'checkout',
        'uses' => 'FrontEndController@get_checkout'
    ]);
    Route::post('checkout',[
        'as'   => 'checkout',
        'uses' => 'FrontEndController@post_checkout'
    ]);
    Route::get('search', [
        'as'   => 'search',
        'uses' => 'FrontEndController@get_search'
    ]);

// Nhóm xử lý back-end
    Route::group(['prefix'=>'get-login-dashboard','middleware'=>'GetLoginDashboard'], function(){
        Route::get('login','UserController@get_admin_login')->name('get-login-dashboard');
        Route::post('login','UserController@post_admin_login')->name('post-login-dashboard');
        Route::get('logout','UserController@get_admin_logout')->name('get-logout-dashboard');
    });

    Route::group(['prefix'=>'dashboard','middleware'=>'PostLoginDashboard'], function(){
        // error page
        route::get('error',function(){ return view('back.pages.error'); })->name('error-page');

        // author
        Route::group(['prefix'=>'author','middleware'=>'AuthorDashboard'], function(){
            Route::get('dashboard', [
                'as'  => 'get-author-dashboard',
                'uses'=> 'DashboardController@get_author_dashboard'
            ]);
            Route::group(['prefix'=>'news'], function(){
                Route::get('list', 'NewsController@getList')->name('all-news-author');
                Route::get('published', 'NewsController@getPublished')->name('published-news-author');
                Route::get('pending', 'NewsController@getPending')->name('pending-news-author');
                Route::get('draft', 'NewsController@getDraft')->name('draft-news-author');
                Route::get('preview/true/{id}', 'NewsController@getPreview')->name('preview-news-author');
                Route::get('add', 'NewsController@getAdd')->name('add-news');
                Route::post('add', 'NewsController@postAdd')->name('add-news');
                Route::get('edit/{id}', 'NewsController@getEdit')->name('edit-news');
                Route::post('edit/{id}', 'NewsController@postEdit')->name('edit-news');
                Route::get('delete/{id}', 'NewsController@getDelete')->name('detele-news');
            });
        });

        // admin
        Route::group(['prefix'=>'admin','middleware'=>'AdminDashboard'], function(){
            Route::get('dashboard', [
                'as'  => 'get-dashboard',
                'uses'=> 'DashboardController@get_dashboard'
            ]);
            Route::group(['prefix'=>'order'], function(){
                Route::get('list','OrderController@get_list')->name('all-orders');
                Route::get('processed','OrderController@get_processed')->name('order-processed');
                Route::post('search','OrderController@search')->name('order.search');
                Route::get('ship','OrderController@get_ship')->name('order-ship');
                 Route::get('statistical','OrderController@statistical')->name('statistical');
                Route::get('success','OrderController@get_success')->name('order-success');
                Route::get('no-process','OrderController@get_no_process')->name('order-no-process');
                Route::get('details/{id}','OrderController@get_details')->name('order-details');
                Route::get('delete/{id}','OrderController@get_delete')->name('order-detele');
                Route::get('active/{id}','OrderController@active')->name('order.active');
            });
            Route::group(['prefix'=>'product'], function(){
                Route::get('list','ProductController@get_list')->name('all-products');
                Route::get('details/{id}','ProductController@get_details')->name('product-details');
                Route::get('add','ProductController@get_add')->name('product-add-new');
                Route::post('add','ProductController@post_add')->name('product-add-new');
                Route::get('edit/{id}','ProductController@get_edit')->name('product-edit');
                Route::post('edit/{id}','ProductController@post_edit')->name('product-edit');
                Route::get('delete/{id}','ProductController@get_delete')->name('product-delete');
            });
            Route::group(['prefix'=>'category'], function(){
                Route::get('list','CategoryController@get_list')->name('all-categories');
                // Route::get('add','CategoryController@get_add')->name('category-add-new');
                Route::post('add','CategoryController@post_add')->name('category-add-new');
                Route::get('edit/{id}','CategoryController@get_edit')->name('category-edit');
                Route::post('edit/{id}','CategoryController@post_edit')->name('category-edit');
                Route::get('delete/{id}','CategoryController@get_delete')->name('category-delete');
            });
            Route::group(['prefix'=>'user'], function(){
                Route::get('list','UserController@get_list')->name('all-users');
                // Route::get('details/{id}','UserController@get_details')->name('user-details');
                Route::get('add','UserController@get_add')->name('user-add-new');
                Route::post('add','UserController@post_add')->name('user-add-new');
                Route::get('edit/{id}','UserController@get_edit')->name('user-edit');
                Route::post('edit/{id}','UserController@post_edit')->name('user-edit');
                Route::get('delete/{id}','UserController@get_delete')->name('user-delete');
            });
            Route::group(['prefix'=>'slide'], function(){
                Route::get('list','SlideController@get_list')->name('all-slides');
                // Route::get('details/{id}','SlideController@get_details')->name('slide-details');
                Route::get('add','SlideController@get_add')->name('slide-add-new');
                Route::post('add','SlideController@post_add')->name('slide-add-new');
                Route::get('edit/{id}','SlideController@get_edit')->name('slide-edit');
                Route::post('edit/{id}','SlideController@post_edit')->name('slide-edit');
                Route::get('delete/{id}','SlideController@get_delete')->name('slide-delete');
            });
            Route::group(['prefix'=>'comment'], function(){
                Route::get('list','CommentController@get_list')->name('all-comments');
                Route::get('delete/{id}','CommentController@get_delete')->name('comment-delete');
            });
            Route::group(['prefix'=>'news'], function(){
                Route::get('published','NewsController@HandlePublished')->name('news-admin-published');
                Route::get('pending','NewsController@HandlePending')->name('news-admin-pending');
                Route::get('preview/true/{id}', 'NewsController@getPreview')->name('preview-news-admin');
                Route::get('approve/{id}', 'NewsController@ApproveNews')->name('approve-news');
                Route::get('delete/{id}', 'NewsController@getAdminDelete')->name('detele-news-admin');
            });
        });
    });
