<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SlidesTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        // $this->call(CategoriesTableSeeder::class);
        // $this->call(ProductsTableSeeder::class);
    }
}

/*
	News sẽ thêm dữ liệu sau, ban đầu không cần vì nó cũng chẳng liên kết tới bảng nào cả.
	--Trung Quân--
*/
//class SlidesTableSeeder
class SlidesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('slides')->insert([
            [
                'title'     => 'Trungquandev Slide 1',
                'slug'      => 'trungquandev-slide-1',
                'image'     => 'trungquandev-slide-1.jpg',
                'active'    => '1',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 2',
                'slug'      => 'trungquandev-slide-2',
                'image'     => 'trungquandev-slide-2.jpg',
                'active'    => '1',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 3',
                'slug'      => 'trungquandev-slide-3',
                'image'     => 'trungquandev-slide-3.jpg',
                'active'    => '1',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 4',
                'slug'      => 'trungquandev-slide-4',
                'image'     => 'trungquandev-slide-4.jpg',
                'active'    => '1',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 5',
                'slug'      => 'trungquandev-slide-5',
                'image'     => 'trungquandev-slide-5.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 6',
                'slug'      => 'trungquandev-slide-6',
                'image'     => 'trungquandev-slide-6.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 7',
                'slug'      => 'trungquandev-slide-7',
                'image'     => 'trungquandev-slide-7.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 8',
                'slug'      => 'trungquandev-slide-8',
                'image'     => 'trungquandev-slide-8.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 9',
                'slug'      => 'trungquandev-slide-9',
                'image'     => 'trungquandev-slide-9.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 10',
                'slug'      => 'trungquandev-slide-10',
                'image'     => 'trungquandev-slide-10.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 11',
                'slug'      => 'trungquandev-slide-11',
                'image'     => 'trungquandev-slide-11.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 12',
                'slug'      => 'trungquandev-slide-12',
                'image'     => 'trungquandev-slide-12.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 13',
                'slug'      => 'trungquandev-slide-13',
                'image'     => 'trungquandev-slide-13.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 14',
                'slug'      => 'trungquandev-slide-14',
                'image'     => 'trungquandev-slide-14.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 15',
                'slug'      => 'trungquandev-slide-15',
                'image'     => 'trungquandev-slide-15.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 16',
                'slug'      => 'trungquandev-slide-16',
                'image'     => 'trungquandev-slide-16.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ],
            [
                'title'     => 'Trungquandev Slide 17',
                'slug'      => 'trungquandev-slide-17',
                'image'     => 'trungquandev-slide-17.jpg',
                'active'    => '0',
                'created_at'=> new DateTime()
            ]
        ]);
    }
}
//end SlidesTableSeeder

//class UsersTableSeeder
class UsersTableSeeder extends Seeder
{
    public function run()
    {
    	DB::table('users')->insert([
    		[
    			'name'		=> 'trungquan_admin',
    			'gender'	=> 'Male',
    			'email'		=> 'trungquan_admin@trungquandev.com',
    			'password'	=> bcrypt('17071995'),
    			'address'	=> 'Thanh Hoá',
    			'phone'		=> '0123456789',
    			'image'		=> 'user1-avatar.jpg',
    			'role'		=> 'admin',
    			'created_at'=> new DateTime()
    		],
 			[
    			'name'		=> 'trungquan_author',
    			'gender'	=> 'Female',
    			'email'		=> 'trungquan_author@trungquandev.com',
    			'password'	=> bcrypt('17071995'),
    			'address'	=> 'Hà Nội',
    			'phone'		=> '0123456789',
    			'image'		=> 'default-avatar.jpg',
    			'role'		=> 'author',
    			'created_at'=> new DateTime()
    		],
    		[
    			'name'		=> 'trungquan_member',
    			'gender'	=> 'Male',
    			'email'		=> 'trungquan_member@trungquandev.com',
    			'password'	=> bcrypt('17071995'),
    			'address'	=> 'Thanh Hoá',
    			'phone'		=> '0123456789',
    			'image'		=> 'default-avatar.jpg',
    			'role'		=> 'member',
    			'created_at'=> new DateTime()
    		]
        ]);
    }
}
//end UsersTableSeeder

//class CategoriesTableSeeder
class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
    	DB::table('categories')->insert([
    		/*
    		[
    			'parent_id'		=> NULL,
    			'name'			=> 'Bánh',
    			'slug'			=> 'banh',
    			'description'	=> 'Tất cả các loại bánh trên đời ( O.o)',
    			'created_at' 	=> new DateTime()
    		],
    		[
    			'parent_id'		=> NULL,
    			'name'			=> 'Trái cây',
    			'slug'			=> 'trai-cay',
    			'description'	=> 'Tất cả các loại trái cây trên đời ( O.o)',
    			'created_at' 	=> new DateTime()
    		]
    		*/
            /*
    		[
    			'parent_id'		=> '1',
    			'name'			=> 'Bánh mặn',
    			'slug'			=> 'banh-man',
    			'description'	=> 'Nếu từng bị mê hoặc bởi các loại tarlet ngọt thì chắn chắn bạn không thể bỏ qua những loại tarlet mặn. Ngoài hình dáng bắt mắt, lớp vở bánh giòn giòn cùng với nhân mặn như thịt gà, nấm, thị heo ,… của bánh sẽ chinh phục bất cứ ai dùng thử.',
    			'created_at' 	=> new DateTime()
    		],
    		[
                'parent_id'     => '1',
                'name'          => 'Bánh ngọt',
                'slug'          => 'banh-ngot',
                'description'   => 'Bánh ngọt là một loại thức ăn thường dưới hình thức món bánh dạng bánh mì từ bột nhào, được nướng lên dùng để tráng miệng. Bánh ngọt có nhiều loại, có thể phân loại dựa theo nguyên liệu và kỹ thuật chế biến như bánh ngọt làm từ lúa mì, bơ, bánh ngọt dạng bọt biển. Bánh ngọt có thể phục vụ những mục đính đặc biệt như bánh cưới, bánh sinh nhật, bánh Giáng sinh, bánh Halloween..',
                'created_at'    => new DateTime()
            ],
            [
                'parent_id'     => '1',
                'name'          => 'Bánh trái cây',
                'slug'          => 'banh-trai-cay',
                'description'   => 'Bánh trái cây, hay còn gọi là bánh hoa quả, là một món ăn chơi, không riêng gì của Huế nhưng khi "lạc" vào mảnh đất Cố đô, món bánh này dường như cũng mang chút tinh tế, cầu kỳ và đặc biệt. Lấy cảm hứng từ những loại trái cây trong vườn, qua bàn tay khéo léo của người phụ nữ Huế, món bánh trái cây ra đời - ngọt thơm, dịu nhẹ làm đẹp lòng biết bao người thưởng thức.',
                'created_at'    => new DateTime()
            ],
            [
                'parent_id'     => '1',
                'name'          => 'Bánh kem',
                'slug'          => 'banh-kem',
                'description'   => 'Với người Việt Nam thì bánh ngọt nói chung đều hay được quy về bánh bông lan – một loại tráng miệng bông xốp, ăn không hoặc được phủ kem lên thành bánh kem. Tuy nhiên, cốt bánh kem thực ra có rất nhiều loại với hương vị, kết cấu và phương thức làm khác nhau chứ không chỉ đơn giản là “bánh bông lan” chung chung đâu nhé...!',
                'created_at'    => new DateTime()
            ],
            [
                'parent_id'     => '1',
                'name'          => 'Bánh crepe',
                'slug'          => 'banh-crepe',
                'description'   => 'Crepe là một món bánh nổi tiếng của Pháp, nhưng từ khi du nhập vào Việt Nam món bánh đẹp mắt, ngon miệng này đã làm cho biết bao bạn trẻ phải "xiêu lòng"',
                'created_at'    => new DateTime()
            ],
            [
                'parent_id'     => '1',
                'name'          => 'Bánh Pizza',
                'slug'          => 'banh-pizza',
                'description'   => 'Pizza đã không chỉ còn là một món ăn được ưa chuộng khắp thế giới mà còn được những nhà cầm quyền EU chứng nhận là một phần di sản văn hóa ẩm thực châu Âu. Và để được chứng nhận là một nhà sản xuất pizza không hề đơn giản. Người ta phải qua đủ mọi các bước xét duyệt của chính phủ Ý và liên minh châu Âu nữa… tất cả là để đảm bảo danh tiếng cho món ăn này.',
                'created_at'    => new DateTime()
            ],
            [
    			'parent_id'		=> '1',
    			'name'			=> 'Bánh su kem',
    			'slug'			=> 'banh-su-kem',
    			'description'	=> 'Bánh su kem là món bánh ngọt ở dạng kem được làm từ các nguyên liệu như bột mì, trứng, sữa, bơ.... đánh đều tạo thành một hỗn hợp và sau đó bằng thao tác ép và phun qua một cái túi để định hình thành những bánh nhỏ và cuối cùng được nướng chín. Bánh su kem có thể thêm thành phần Sô cô la để tăng vị hấp dẫn. Bánh có xuất xứ từ nước Pháp.',
    			'created_at' 	=> new DateTime()
    		]
            */
        ]);
    }
}
//end CategoriesTableSeeder

//class ProductsTableSeeder
class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('products')->insert([
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Sầu riêng',
                'slug'        => 'banh-crepe-sau-rieng',
                'price'       => '150000',
                'discount'    => '120000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    '1430967449-pancake-sau-rieng-6.jpg',
                                    '1430967449-pancake-sau-rieng-6.jpg',
                                    '1430967449-pancake-sau-rieng-6.jpg'
                                ]),
                'description' => 'Bánh crepe sầu riêng Quân làm.',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Chocolate',
                'slug'        => 'banh-crepe-chocolate',
                'price'       => '180000',
                'discount'    => '160000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'crepe-chocolate.jpg',
                                    'crepe-chocolate.jpg',
                                    'crepe-chocolate.jpg'
                                ]),
                'description' => 'Bánh crepe Chocolate Quân làm.',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Sầu riêng - Chuối',
                'slug'        => 'banh-crepe-sau-rieng-chuoi',
                'price'       => '150000',
                'discount'    => '10000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'crepe-chuoi.jpg',
                                    'crepe-chuoi.jpg',
                                    'crepe-chuoi.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Đào',
                'slug'        => 'banh-crepe-dao',
                'price'       => '160000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'crepe-dao.jpg',
                                    'crepe-dao.jpg',
                                    'crepe-dao.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Dâu',
                'slug'        => 'banh-crepe-dau',
                'price'       => '160000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'crepedau.jpg',
                                    'crepedau.jpg',
                                    'crepedau.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Pháp',
                'slug'        => 'banh-crepe-phap',
                'price'       => '200000',
                'discount'    => '180000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'crepe-phap.jpg',
                                    'crepe-phap.jpg',
                                    'crepe-phap.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Táo',
                'slug'        => 'banh-crepe-tao',
                'price'       => '160000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'crepe-tao.jpg',
                                    'crepe-tao.jpg',
                                    'crepe-tao.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Trà Xanh',
                'slug'        => 'banh-crepe-tra-xanh',
                'price'       => '160000',
                'discount'    => '150000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'crepe-traxanh.jpg',
                                    'crepe-traxanh.jpg',
                                    'crepe-traxanh.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '7',
                'name'        => 'Bánh Crepe Sầu riêng và Dứa',
                'slug'        => 'banh-crepe-sau-rieng-va-dua',
                'price'       => '160000',
                'discount'    => '150000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'saurieng-dua.jpg',
                                    'saurieng-dua.jpg',
                                    'saurieng-dua.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh Gato Trái cây Việt Quất',
                'slug'        => 'banh-gato-trai-cay-viet-quat',
                'price'       => '250000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    '544bc48782741.png',
                                    '544bc48782741.png',
                                    '544bc48782741.png'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh sinh nhật rau câu trái cây',
                'slug'        => 'banh-sinh-nhat-rau-cau-trai-cay',
                'price'       => '200000',
                'discount'    => '180000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    '210215-banh-sinh-nhat-rau-cau-body- (6).jpg',
                                    '210215-banh-sinh-nhat-rau-cau-body- (6).jpg',
                                    '210215-banh-sinh-nhat-rau-cau-body- (6).jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Chocolate Dâu',
                'slug'        => 'banh-kem-chocolate-dau',
                'price'       => '300000',
                'discount'    => '280000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'banh kem sinh nhat.jpg',
                                    'banh kem sinh nhat.jpg',
                                    'banh kem sinh nhat.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Dâu III',
                'slug'        => 'banh-kem-dau-iii',
                'price'       => '300000',
                'discount'    => '280000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'Banh-kem (6).jpg',
                                    'Banh-kem (6).jpg',
                                    'Banh-kem (6).jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Dâu I',
                'slug'        => 'banh-kem-dau-i',
                'price'       => '350000',
                'discount'    => '320000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'banhkem-dau.jpg',
                                    'banhkem-dau.jpg',
                                    'banhkem-dau.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '5',
                'name'        => 'Bánh trái cây II',
                'slug'        => 'banh-trai-cay-ii',
                'price'       => '150000',
                'discount'    => '120000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'banhtraicay.jpg',
                                    'banhtraicay.jpg',
                                    'banhtraicay.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '5',
                'name'        => 'Apple Cake',
                'slug'        => 'apple-cake',
                'price'       => '250000',
                'discount'    => '240000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'Fruit-Cake_7979.jpg',
                                    'Fruit-Cake_7979.jpg',
                                    'Fruit-Cake_7979.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '4',
                'name'        => 'Bánh ngọt nhân cream táo',
                'slug'        => 'banh-ngot-nhan-cream-tao',
                'price'       => '180000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    '20131108144733.jpg',
                                    '20131108144733.jpg',
                                    '20131108144733.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '4',
                'name'        => 'Bánh Chocolate Trái cây',
                'slug'        => 'banh-chocolate-trai-cay',
                'price'       => '150000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'Fruit-Cake_7976.jpg',
                                    'Fruit-Cake_7976.jpg',
                                    'Fruit-Cake_7976.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '4',
                'name'        => 'Bánh Chocolate Trái cây II',
                'slug'        => 'banh-chocolate-trai-cay-ii',
                'price'       => '150000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'Fruit-Cake_7981.jpg',
                                    'Fruit-Cake_7981.jpg',
                                    'Fruit-Cake_7981.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '4',
                'name'        => 'Peach Cake',
                'slug'        => 'peach-cake',
                'price'       => '160000',
                'discount'    => '150000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'Peach-Cake_3294.jpg',
                                    'Peach-Cake_3294.jpg',
                                    'Peach-Cake_3294.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '3',
                'name'        => 'Bánh bông lan trứng muối I',
                'slug'        => 'banh-bong-lan-trung-muoi-i',
                'price'       => '160000',
                'discount'    => '150000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'banhbonglantrung.jpg',
                                    'banhbonglantrung.jpg',
                                    'banhbonglantrung.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '3',
                'name'        => 'Bánh bông lan trứng muối II',
                'slug'        => 'banh-bong-lan-trung-muoi-ii',
                'price'       => '180000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'banhbonglantrungmuoi.jpg',
                                    'banhbonglantrungmuoi.jpg',
                                    'banhbonglantrungmuoi.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '3',
                'name'        => 'Bánh French',
                'slug'        => 'banh-french',
                'price'       => '180000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'banh-man-thu-vi-nhat-1.jpg',
                                    'banh-man-thu-vi-nhat-1.jpg',
                                    'banh-man-thu-vi-nhat-1.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '3',
                'name'        => 'Bánh mì Australia',
                'slug'        => 'banh-mi-australia',
                'price'       => '80000',
                'discount'    => '70000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'dung-khoai-tay-lam-banh-gato-man-cuc-ngon.jpg',
                                    'dung-khoai-tay-lam-banh-gato-man-cuc-ngon.jpg',
                                    'dung-khoai-tay-lam-banh-gato-man-cuc-ngon.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '3',
                'name'        => 'Bánh mặn thập cẩm',
                'slug'        => 'banh-man-thap-cam',
                'price'       => '50000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'Fruit-Cake.png',
                                    'Fruit-Cake.png',
                                    'Fruit-Cake.png'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '3',
                'name'        => 'Bánh Muffins trứng',
                'slug'        => 'banh-muffins-trung',
                'price'       => '100000',
                'discount'    => '80000',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'maxresdefault.jpg',
                                    'maxresdefault.jpg',
                                    'maxresdefault.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '3',
                'name'        => 'Bánh mì Loại I',
                'slug'        => 'banh-mi-loai-i',
                'price'       => '100000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'sli12.jpg',
                                    'sli12.jpg',
                                    'sli12.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Chocolate Dâu I',
                'slug'        => 'banh-kem-chocolate-dau-i',
                'price'       => '380000',
                'discount'    => '350000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'sli12.jpg',
                                    'sli12.jpg',
                                    'sli12.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Trái cây I',
                'slug'        => 'banh-kem-trai-cay-i',
                'price'       => '380000',
                'discount'    => '350000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'Fruit-Cake.jpg',
                                    'Fruit-Cake.jpg',
                                    'Fruit-Cake.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Trái cây II',
                'slug'        => 'banh-kem-trai-cay-ii',
                'price'       => '380000',
                'discount'    => '350000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'Fruit-Cake_7971.jpg',
                                    'Fruit-Cake_7971.jpg',
                                    'Fruit-Cake_7971.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Doraemon',
                'slug'        => 'banh-kem-doraemon',
                'price'       => '280000',
                'discount'    => '250000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'p1392962167_banh74.jpg',
                                    'p1392962167_banh74.jpg',
                                    'p1392962167_banh74.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Caramen Pudding',
                'slug'        => 'banh-kem-caramen-pudding',
                'price'       => '280000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'Caramen-pudding636099031482099583.jpg',
                                    'Caramen-pudding636099031482099583.jpg',
                                    'Caramen-pudding636099031482099583.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Chocolate Fruit',
                'slug'        => 'banh-kem-chocolate-fruit',
                'price'       => '320000',
                'discount'    => '300000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'chocolate-fruit636098975917921990.jpg',
                                    'chocolate-fruit636098975917921990.jpg',
                                    'chocolate-fruit636098975917921990.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Coffee Chocolate GH6',
                'slug'        => 'banh-kem-coffee-chocolate-gh6',
                'price'       => '320000',
                'discount'    => '300000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'COFFE-CHOCOLATE636098977566220885.jpg',
                                    'COFFE-CHOCOLATE636098977566220885.jpg',
                                    'COFFE-CHOCOLATE636098977566220885.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Mango Mouse',
                'slug'        => 'banh-kem-mango-mouse',
                'price'       => '320000',
                'discount'    => '300000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'mango-mousse-cake.jpg',
                                    'mango-mousse-cake.jpg',
                                    'mango-mousse-cake.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Matcha Mouse',
                'slug'        => 'banh-kem-matcha-mouse',
                'price'       => '350000',
                'discount'    => '330000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'MATCHA-MOUSSE.jpg',
                                    'MATCHA-MOUSSE.jpg',
                                    'MATCHA-MOUSSE.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Flower Fruit',
                'slug'        => 'banh-kem-flower-fruit',
                'price'       => '350000',
                'discount'    => '330000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'flower-fruits636102461981788938.jpg',
                                    'flower-fruits636102461981788938.jpg',
                                    'flower-fruits636102461981788938.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Strawberry Delight',
                'slug'        => 'banh-kem-strawberry-delight',
                'price'       => '350000',
                'discount'    => '330000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'strawberry-delight636102445035635173.jpg',
                                    'strawberry-delight636102445035635173.jpg',
                                    'strawberry-delight636102445035635173.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '6',
                'name'        => 'Bánh kem Raspberry Delight',
                'slug'        => 'banh-kem-raspberry-delight',
                'price'       => '350000',
                'discount'    => '330000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'raspberry.jpg',
                                    'raspberry.jpg',
                                    'raspberry.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '8',
                'name'        => 'Beefy Pizza',
                'slug'        => 'beefy-pizza',
                'price'       => '150000',
                'discount'    => '130000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    '40819_food_pizza.jpg',
                                    '40819_food_pizza.jpg',
                                    '40819_food_pizza.jpg'
                                ]),
                'description' => 'Thịt bò xay, ngô, sốt BBQ, phô mai mozzarella',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '8',
                'name'        => 'Hawaii Pizza',
                'slug'        => 'hawaii-pizza',
                'price'       => '120000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'hawaiian paradise_large-900x900.jpg',
                                    'hawaiian paradise_large-900x900.jpg',
                                    'hawaiian paradise_large-900x900.jpg'
                                ]),
                'description' => 'Sốt cà chua, ham , dứa, pho mai mozzarella',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '8',
                'name'        => 'Smoke Chicken Pizza',
                'slug'        => 'smoke-chicken-pizza',
                'price'       => '120000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'chicken black pepper_large-900x900.jpg',
                                    'chicken black pepper_large-900x900.jpg',
                                    'chicken black pepper_large-900x900.jpg'
                                ]),
                'description' => 'Gà hun khói,nấm, sốt cà chua, pho mai mozzarella.',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '8',
                'name'        => 'Sausage Pizza',
                'slug'        => 'sausage-pizza',
                'price'       => '120000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'pizza-miami.jpg',
                                    'pizza-miami.jpg',
                                    'pizza-miami.jpg'
                                ]),
                'description' => 'Xúc xích klobasa, Nấm, Ngô, sốtcà chua, pho mai Mo...',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '8',
                'name'        => 'Ocean Pizza',
                'slug'        => 'ocean-pizza',
                'price'       => '120000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'seafood curry_large-900x900.jpg',
                                    'seafood curry_large-900x900.jpg',
                                    'seafood curry_large-900x900.jpg'
                                ]),
                'description' => 'Tôm , mực, xào cay,ớt xanh, hành tây, cà chua, pho...',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '8',
                'name'        => 'All Meaty Pizza',
                'slug'        => 'all-meaty-pizza',
                'price'       => '140000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'all1).jpg',
                                    'all1).jpg',
                                    'all1).jpg'
                                ]),
                'description' => 'Ham, bacon, chorizo, pho mai mozzarella.',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '8',
                'name'        => 'Tuna Pizza',
                'slug'        => 'tuna-pizza',
                'price'       => '140000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    '54eaf93713081_-_07-germany-tuna.jpg',
                                    '54eaf93713081_-_07-germany-tuna.jpg',
                                    '54eaf93713081_-_07-germany-tuna.jpg'
                                ]),
                'description' => 'Cá Ngừ, sốt Mayonnaise,sốt càchua, hành tây, pho mai Mozzarella',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem nhân dừa',
                'slug'        => 'banh-su-kem-nhan-dua',
                'price'       => '120000',
                'discount'    => '100000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'maxresdefault.jpg',
                                    'maxresdefault.jpg',
                                    'maxresdefault.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem sữa tươi',
                'slug'        => 'banh-su-kem-sua-tuoi',
                'price'       => '120000',
                'discount'    => '100000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'sukem.jpg',
                                    'sukem.jpg',
                                    'sukem.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem sữa tươi chiên giòn',
                'slug'        => 'banh-su-kem-sua-tuoi-chien-gion',
                'price'       => '150000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    '1434429117-banh-su-kem-chien-20.jpg',
                                    '1434429117-banh-su-kem-chien-20.jpg',
                                    '1434429117-banh-su-kem-chien-20.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem dâu sữa tươi',
                'slug'        => 'banh-su-kem-dau-sua-tuoi',
                'price'       => '150000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'sukemdau.jpg',
                                    'sukemdau.jpg',
                                    'sukemdau.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem bơ sữa tươi',
                'slug'        => 'banh-su-kem-bo-sua-tuoi',
                'price'       => '150000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'He-Thong-Banh-Su-Singapore-Chewy-Junior.jpg',
                                    'He-Thong-Banh-Su-Singapore-Chewy-Junior.jpg',
                                    'He-Thong-Banh-Su-Singapore-Chewy-Junior.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem nhân trái cây sữa tươi',
                'slug'        => 'banh-su-kem-nhan-trai-cay-sua-tuoi',
                'price'       => '150000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'foody-banh-su-que-635930347896369908.jpg',
                                    'foody-banh-su-que-635930347896369908.jpg',
                                    'foody-banh-su-que-635930347896369908.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem cà phê',
                'slug'        => 'banh-su-kem-ca-phe',
                'price'       => '150000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'banh-su-kem-ca-phe-1.jpg',
                                    'banh-su-kem-ca-phe-1.jpg',
                                    'banh-su-kem-ca-phe-1.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem phô mai',
                'slug'        => 'banh-su-kem-pho-mai',
                'price'       => '170000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    '50020041-combo-20-banh-su-que-pho-mai-9.jpg',
                                    '50020041-combo-20-banh-su-que-pho-mai-9.jpg',
                                    '50020041-combo-20-banh-su-que-pho-mai-9.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '9',
                'name'        => 'Bánh su kem sữa tươi chocolate',
                'slug'        => 'banh-su-kem-sua-tuoi-chocolate',
                'price'       => '190000',
                'discount'    => '0',
                'unit'        => 'hộp',
                'image'       => json_encode([
                                    'combo-20-banh-su-que-kem-sua-tuoi-phu-socola.jpg',
                                    'combo-20-banh-su-que-kem-sua-tuoi-phu-socola.jpg',
                                    'combo-20-banh-su-que-kem-sua-tuoi-phu-socola.jpg'
                                ]),
                'description' => '',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '4',
                'name'        => 'Bánh Macaron Pháp',
                'slug'        => 'banh-macaron-phap',
                'price'       => '200000',
                'discount'    => '180000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'Macaron9.jpg',
                                    'Macaron9.jpg',
                                    'Macaron9.jpg'
                                ]),
                'description' => 'Thưởng thức macaron, người ta có thể tìm thấy từ những hương vị truyền thống như mâm xôi, chocolate, cho đến những hương vị mới như nấm và trà xanh. Macaron với vị giòn tan của vỏ bánh, béo ngậy ngọt ngào của phần nhân, với vẻ ngoài đáng yêu và nhiều màu sắc đẹp mắt, đây là món bạn không nên bỏ qua khi khám phá nền ẩm thực Pháp.',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '4',
                'name'        => 'Bánh Tiramisu - Italia',
                'slug'        => 'banh-tiramisu-italia',
                'price'       => '200000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    '234.jpg',
                                    '234.jpg',
                                    '234.jpg'
                                ]),
                'description' => 'Chỉ cần cắn một miếng, bạn sẽ cảm nhận được tất cả các hương vị đó hòa quyện cùng một chính vì thế người ta còn ví món bánh này là Thiên đường trong miệng của bạn',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '5',
                'name'        => 'Bánh Táo - Mỹ',
                'slug'        => 'banh-tao-my',
                'price'       => '270000',
                'discount'    => '0',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    '1234.jpg',
                                    '1234.jpg',
                                    '1234.jpg'
                                ]),
                'description' => 'Bánh táo Mỹ với phần vỏ bánh mỏng, giòn mềm, ẩn chứa phần nhân táo thơm ngọt, điểm chút vị chua dịu của trái cây quả sẽ là một lựa chọn hoàn hảo cho những tín đồ bánh ngọt trên toàn thế giới.',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '4',
                'name'        => 'Bánh Cupcake - Anh Quốc',
                'slug'        => 'banh-cupcake-anh-quoc',
                'price'       => '270000',
                'discount'    => '250000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    'cupcake.jpg',
                                    'cupcake.jpg',
                                    'cupcake.jpg'
                                ]),
                'description' => 'Những chiếc cupcake có cấu tạo gồm phần vỏ bánh xốp mịn và phần kem trang trí bên trên rất bắt mắt với nhiều hình dạng và màu sắc khác nhau. Cupcake còn được cho là chiếc bánh mang lại niềm vui và tiếng cười như chính hình dáng đáng yêu của chúng.',
                'created_at'  => new DateTime()
            ],
            [
                'category_id' => '4',
                'name'        => 'Bánh Sachertorte',
                'slug'        => 'banh-Sachertorte',
                'price'       => '250000',
                'discount'    => '220000',
                'unit'        => 'cái',
                'image'       => json_encode([
                                    '111.jpg',
                                    '111.jpg',
                                    '111.jpg'
                                ]),
                'description' => 'Sachertorte là một loại bánh xốp được tạo ra bởi loại&nbsp;chocholate&nbsp;tuyệt hảo nhất của nước Áo. Sachertorte có vị ngọt nhẹ, gồm nhiều lớp bánh được làm từ ruột bánh mì và bánh sữa chocholate, xen lẫn giữa các lớp bánh là mứt mơ. Món bánh chocholate này nổi tiếng tới mức thành phố Vienna của Áo đã ấn định&nbsp;tổ chức một ngày Sachertorte quốc gia, vào 5/12 hằng năm',
                'created_at'  => new DateTime()
            ]
        ]);
    }
}
//end ProductsTableSeeder
