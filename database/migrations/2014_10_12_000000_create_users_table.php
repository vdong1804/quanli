<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->enum('gender',['Male','Female'])->default('Male');
            $table->string('email',191)->unique();
            $table->string('password',255);
            $table->string('address',100);
            $table->string('phone',191)->unique();
            $table->string('image',100)->default('default-avatar.jpg');
            $table->enum('role',['admin','author','member'])->default('member');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('users');
    }
}
