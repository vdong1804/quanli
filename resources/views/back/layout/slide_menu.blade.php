<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <!-- <a href="https://trungquandev.com/" target="__blank" class="site_title"> -->
        <i class="fa fa-paw"></i> <span>Green Cat !</span>
      </a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="images/user/{{Auth::user()->image}}" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>{{Auth::user()->name}}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
          @if(Auth::user()->role == 'author')
            <li>
              <a href="{{route('get-author-dashboard')}}">
                <i class="fa fa-dashboard"></i> Bảng điều khiển <span class="fa fa-chevron-right"></span>
              </a>
            </li>
            <li><a><i class="fa fa-edit"></i> Tin tức <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{route('all-news-author')}}">Bài viết của tôi</a></li>
                <li><a href="{{route('add-news')}}">Thêm mới</a></li>
                <li><a href="{{route('published-news-author')}}">Được phát hành</a></li>
                <li><a href="{{route('pending-news-author')}}">Đang chờ xử lý</a></li>
                <li><a href="{{route('draft-news-author')}}">Bản nháp</a></li>
              </ul>
            </li>
          @else
            <li>
              <a href="{{route('get-dashboard')}}">
                <i class="fa fa-dashboard"></i> Bảng điều khiển <span class="fa fa-chevron-right"></span>
              </a>
            </li>
            <li><a><i class="fa fa-shopping-cart"></i> Đơn hàng @if($total_new_order>0)({{ $total_new_order }})@endif<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{route('all-orders')}}">Tất cả đơn hàng</a></li>
                <li><a href="{{route('order-ship')}}">Đang giao</a></li>
                <li><a href="{{route('order-success')}}">Đã hoàn thành</a></li>
                <li><a href="{{route('order-processed')}}">Đã xử lí</a></li>
                <li><a href="{{route('order-no-process')}}">Chưa xử lí@if($total_new_order>0)({{ $total_new_order }})@endif</a></li>
                <li><a href="{{route('statistical')}}">Thống kê</a></li>
              </ul>
            </li>
            <li><a><i class="fa fa-cube"></i> Products <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{route('all-products')}}">Tất cả sản phẩm</a></li>
                <li><a href="{{route('product-add-new')}}">Thêm mới</a></li>
                <li><a href="{{route('all-categories')}}">Danh mục sản phẩm</a></li>
              </ul>
            </li>
            <li><a><i class="fa fa-edit"></i> Tin tức
                  @if(isset($pendingNews))
                    <span class="count-pending-news">
                      ({{$pendingNews}})
                    </span>
                  @endif
                   <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{route('news-admin-published')}}">Được phát hành</a></li>
                <li><a href="{{route('news-admin-pending')}}">
                  Đang chờ xử lý
                  @if(isset($pendingNews))
                    <span class="count-pending-news">
                      ({{$pendingNews}})
                    </span>
                  @endif
                </a></li>
              </ul>
            </li>
            <li><a><i class="fa fa-image"></i> Slides <span class="fa fa-chevron-right"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{route('all-slides')}}">Hình ảnh</a></li>
                <li><a href="{{route('slide-add-new')}}">Thêm mới</a></li>
              </ul>
            </li>
            <li>
              <a href="{{route('all-comments')}}"><i class="fa fa-comments-o"></i> Bình luận <span class="fa fa-chevron-right"></span></a>
            </li>
            <li><a><i class="fa fa-user"></i> Người dùng @if($total_new_user>0)({{ $total_new_user }})@endif<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{route('all-users')}}">Tất cả người dùng</a></li>
                <li><a href="{{route('user-add-new')}}">Thêm mới</a></li>
                {{-- <li><a href="tables_dynamic.html">Your Profile</a></li> --}}
              </ul>
            </li>
          @endif
        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('get-logout-dashboard')}}">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>
