<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Quản lý siêu thị Tân Hường</title>
  <link rel="shortcut icon" href="{{asset('images/favicon/tanhuong.jpg')}}" />
  <base href="{{ asset('') }}">

  <!-- Bootstrap -->
  <link href="assets/bootstrap-min-only/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="assets/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="assets/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
  <link href="assets/iCheck/skins/flat/green.css" rel="stylesheet">
  <link href="assets/switchery/dist/switchery.min.css" rel="stylesheet">
  {{-- slick slider --}}
  <link href="build/css/slick.css" rel="stylesheet">
  <link href="build/css/jquery-ui.css" rel="stylesheet">
  <link href="build/css/slick-theme.css" rel="stylesheet">

  @yield('css')

  <!-- Custom Theme Style from vendor gelena master -->
  <link href="build/css/custom.min.css" rel="stylesheet">
  {{-- my custom backend css - trungquan --}}
  <link href="css/trungquan-backend.css" rel="stylesheet">
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      {{-- slide menu --}}
      @include('back.layout.slide_menu')

      <!-- top navigation header top menu -->
      @include('back.layout.header_menu')
      @if(Auth::check())
      @php
      $currentUserId = Auth::user()->id;
      @endphp
      @endif
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        @yield('content')
      </div>
      <!-- /page content -->

      <!-- footer content -->
      @include('back.layout.footer')
      <!-- /footer content -->
    </div>
  </div>

  <!-- jQuery -->
  <script src="assets/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="assets/bootstrap-min-only/bootstrap.min.js"></script>
  <!-- Datatables -->
  <script src="assets/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="assets/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="assets/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="assets/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="assets/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="assets/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="assets/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="assets/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="assets/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="assets/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  <script src="assets/iCheck/icheck.min.js"></script>
  <script src="assets/switchery/dist/switchery.min.js"></script>
  <script src="assets/bootstrapck4-skin/ckeditor.js" ></script>
  <script src="assets/pusher-js/dist/web/pusher.min.js"></script>
  <script src="build/js/slick.js"></script>
  
  @yield('script')

  <script type="text/javascript">

    $(document).ready(function(){
          // approve news
          var pusher = new Pusher('82881fdb575ce09055ff', {
            cluster: 'ap1',
            encrypted: true
          });
          var channel = pusher.subscribe('channel-approve-news');
          channel.bind('App\\Events\\ApproveNewsEvent', addNoticeNews);
        });

    var notice_array = new Array();
    function addNoticeNews(data) {
      var title = data.title;
      var userId = data.userId;

      var currentUserId = <?php echo json_encode($currentUserId); ?>;

      if (currentUserId == userId) {
        var notice = "<li><a href='javascript:void(0)'><span class='message'>Bài viết <strong>"+title+"</strong> của bạn đã được duyệt.</span></a></li>";
        notice_array.push(notice);
        var count_notice = "<strong>"+notice_array.length+"</strong>";
        $('#menu1').prepend(notice);
        $('#count-notice').html(count_notice);
      }
    }
          //clear notice
          $("#news-notice").on("click", function(){
            $('#count-notice').html("");
          });
        </script>
                  <script type="text/javascript">
            $( function() {
            var date = new Date();
            date1 =  new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1);
            date = date.format('yyyy-mm-dd');
        
            $( ".datepicker2" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat:'yy-mm-dd',
                // minDate : date,
            });
            date1 = date1.format('yyyy-mm-dd');
           
                       $( ".datepicker1" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat:'yy-mm-dd',
                // minDate : date,
                onSelect: function(dateText) {
                    $(".datepicker2").datepicker('destroy');
                    $( ".datepicker2" ).datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat:'yy-mm-dd',
                        minDate : dateText,
                    });
                    $(this).change();
                }
            });
            } );
    </script>
        <script src="build/js/jquery-ui.js"></script>
        <script src="build/js/date.format.js"></script>
        <!-- Custom Theme Scripts from vendor gelena master  -->
        <script src="build/js/custom.min.js"></script>
        {{-- my custom backend js - trungquan --}}
        <script src="js/trungquan-backend.js"></script>
      </body>
      </html>
