<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Quản lý siêu thị Tân Hường</title>
    <base href="{{ asset('') }}">
    <!-- Bootstrap -->
    <link href="assets/bootstrap-min-only/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">

    {{-- my custom backend css --}}
    <link href="css/trungquan-backend.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
            @if(Session::has('logout_success'))
                <div class="alert alert-success">
                    {{Session::get('logout_success')}}
                </div>
            @endif
            <form name="form_login" role="form" action="{{route('post-login-dashboard')}}" method="POST">
              <h1>Login Dashboard</h1>
              <input type="hidden" name="_token" value="{{csrf_token()}}" />
              <div>
                <input type="email" class="form-control" name="form_email" placeholder="Enter e-mail" required />
              </div>
              <div>
                <input type="password" class="form-control" name="form_password" placeholder="Enter password" required />
              </div>
              <div>
                <button type="submit" class="btn btn-default submit">Đăng nhập</button>
                <button type="reset" class="btn btn-default submit">Reset</button>
                {{-- <a class="reset_pass" href="#">Forgot password?</a> --}}
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-paw"></i> Green Cat</h1>
                  <p>©2018 - All Rights Reserved. <br>Thanks to Gentelella Alela!</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
