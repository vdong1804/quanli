@extends('back.layout.index')
@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="">
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                @if($total_new_user > 0)
                <h3><a href="{{ route('all-users') }}"> Có {{ $total_new_user }} thành viên mới</a></h3>
                @endif
                @if($total_new_order > 0)
                <h3><a href="{{ route('order-no-process') }}"> Có {{ $total_new_order }} đơn hàng mới</a></h3>
                 @endif
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-list"></i>
                  </div>
                  <div class="count">{{$total_categories}}</div>
                  <h3>Danh mục</h3>
                  <p>Tổng số danh mục có sẵn</p>
                </div>
              </div>

              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-cube"></i>
                  </div>
                  <div class="count">{{$total_products}}</div>
                  <h3>Sản phẩm</h3>
                  <p>Tổng số sản phẩm hiện có</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                    @if($total_new_order > 0)
                      <span class="label label-warning" style="font-size: 15px">{{ $total_new_order }}</span>
                    @endif
                  </div>
                  <div class="count">{{$total_orders}}</div>
                  <h3>Đơn hàng</h3>
                  <p>Tổng số đơn hàng </p>
                </div>
              </div>

              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"></i>
                  </div>
                  <div class="count"><span style="font-size:32px;">{{number_format($total_revenue)}} vnđ</span></div>
                  <h3>Doanh thu</h3>
                  <p>Tổng số doanh thu </p>
                </div>
              </div>

              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-edit"></i>
                  </div>
                  <div class="count">{{$total_news}}</div>
                  <h3>Tin tức</h3>
                  <p>Tổng số tin tức đã được xuất bản. </p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-image"></i>
                  </div>
                  <div class="count">{{$total_slides}}</div>
                  <h3>Slides</h3>
                  <p>Tổng số slides </p>
                </div>
              </div>

              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-comments-o"></i>
                  </div>
                  <div class="count">{{$total_comments}}</div>
                  <h3>Bình luận</h3>
                  <p>Tổng số bình luận</p>
                </div>
              </div>

              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user"></i>
                    @if($total_new_user > 0)
                      <span class="label label-warning" style="font-size: 15px">{{ $total_new_user }}</span>
                    @endif
                  </div>
                  <div class="count">{{$total_users}}</div>
                  <h3>Người dùng</h3>
                  <p>Tông số người dùng </p>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
