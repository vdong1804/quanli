@extends('back.layout.index')
@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="">
          <div class="x_content">
            <div class="row">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-edit"></i>
                  </div>
                  <div class="count">{{$total_news}}</div>

                  <h3>Tin tức</h3>
                  <p>Bài viết cảu bạn</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
