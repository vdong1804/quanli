@extends('back.layout.index')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(Session::has('delete_success'))
                <div class="alert alert-success">
                    {{session('delete_success')}}
                </div>
            @endif
            <div class="x_panel">
              <div class="x_title">
                <h2>Bài viết<small>...</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap list-news" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Hoạt động</th>
                      <th>Tiêu đề</th>
                      <th>Hình Ảnh</th>
                      <th>Ngày</th>
                      <th>Hành động</th>
                      <th>Sửa</th>
                      <th>Xóa</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $stt = 1; @endphp
                    @foreach($news as $item)
                        <tr style="text-align: center;">
                          <td>{{$stt}}</td>
                          @if($item->active == 0)
                            <td style="color: #F0AD4E; font-weight: bold;">Ngáo</td>
                          @elseif($item->active == 1)
                            <td style="color: #46B8DA; font-weight: bold;">Đang chờ xử lý</td>
                          @elseif($item->active == 2)
                            <td style="color: #169F85; font-weight: bold;">Xuất bản</td>
                          @endif
                          <td>{{$item->title}}</td>
                          <td><img width="50" height="50" src="images/news/{{$item->image}}" alt="{{$item->image}}" /></td>
                          <td>{{$item->created_at->format('H:i, d M Y')}}</td>
                          @if($item->active == 2)
                            <td><a style="color: #169F85; font-weight: bold;" href="{{route('read-news-frontend', $item->slug)}}">Lượt xem</a></td>
                          @else
                            <td><a style="color: #F0AD4E; font-weight: bold;" href="{{route('preview-news-author', $item->id)}}">Xem trước</a></td>
                          @endif
                          <td><a href="{{route('edit-news', $item->id)}}"><i class="fa fa-edit"></i></a></td>
                          <td><a href="{{route('detele-news', $item->id)}}"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        @php $stt++; @endphp
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
@endsection
