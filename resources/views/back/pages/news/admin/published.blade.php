@extends('back.layout.index')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(Session::has('delete_success'))
                <div class="alert alert-success">
                    {{session('delete_success')}}
                </div>
            @endif
            <div class="x_panel">
              <div class="x_title">
                <h2>Bài viết đã xuất bản<small>...</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap list-news" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Tiêu đề</th>
                      <th>Hình Ảnh</th>
                      <th>Ngày</th>
                      <th>Lượt xem</th>
                      <th>Xóa</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $stt = 1; @endphp
                    @foreach($news as $item)
                        <tr style="text-align: center;">
                          <td>{{$stt}}</td>
                          <td>{{$item->title}}</td>
                          <td><img width="50" height="50" src="images/news/{{$item->image}}" alt="{{$item->image}}" /></td>
                          <td>{{$item->created_at->format('H:i, d M Y')}}</td>
                          <td><a href="{{route('read-news-frontend', $item->slug)}}"><i class="fa fa-eye"></i></a></td>
                          <td><a href="{{route('detele-news-admin', $item->id)}}"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        @php $stt++; @endphp
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
@endsection
