@extends('back.layout.index')
@section('content')
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Sửa bài viết: {{$currentNews->title}}</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
            @if(Session::has('add_success'))
                <div class="alert alert-success">
                    {{session('add_success')}}
                </div>
            @endif
            @if(Session::has('edit_success'))
                <div class="alert alert-success">
                    {{session('edit_success')}}
                </div>
            @endif
            <form id="form-add-news" action="{{route('edit-news',$currentNews->id)}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12" for="title">Tiêu đề <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" name="title" id="title" required="required" class="form-control col-md-7 col-xs-12" @if(old('title')) value="{{old('title')}}" @else value="{{$currentNews->title}}" @endif placeholder='Enter news title...'>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12" for="slug">Permalink:</label>
                <div class="col-md-5 col-sm-5 col-xs-5 editSlug">
                  <span>{{asset('/')}}</span>
                  <input type="text" name="slug" id="slug" required="required" class="form-control col-md-5 col-xs-12" @if(old('slug')) value="{{old('slug')}}" @else value="{{$currentNews->slug}}" @endif readonly>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6 editSlugCheckbox">
                  <input type="checkbox" class="js-switch" id="edit-slug" /> Sửa
                </div>
              </div>
              <br>
              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12" for="content">Nội dung <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea id="content" class="form-control ckeditor" name="content">@if(old('content')){{old('content')}}@else{{$currentNews->content}}@endif</textarea>
                </div>
              </div>
              <br>
              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12">Hình Ảnh <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <table>
                    <thead>
                      <tr>
                        <th><img src="images/news/{{$currentNews->image}}" height="100" alt=""></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style="text-align: center;">
                        <td>
                          <br>
                          <label class="custom-file-upload">
                      <input id="file-input" type="file" class="form-control hidden" name="image">
                      <i class="fa fa-cloud-upload" style="font-size: 17px;"></i> Chỉnh sửa hình ảnh nổi bật
                  </label>
                        </td>
                      </tr>
                      <tr style="text-align: center;">
                        <td><br><div id="preview"></div></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12">Trạng thái <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12 editActiveRadio">
                  <p>
                      Lưu bản nháp: <input type="radio" class="flat" name="active" id="active" value="0" @if($currentNews->active=="0"){{"checked"}}@endif /> &nbsp;&nbsp;&nbsp;
                      Xuất bản: <input type="radio" class="flat" name="active" id="active" value="1" @if($currentNews->active=="1"){{"checked"}}@endif required />
                    </p>
                </div>
              </div>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="{{route('all-news-author')}}">
                   <button class="btn btn-primary" type="button">Hủy</button>
                  </a>
                  <button type="submit" class="btn btn-success">Gửi</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
          $("#edit-slug").change(function(){
              if($(this).is(":checked")){
                  $("#slug").removeAttr('readonly');
              }
              else{
                  $("#slug").attr('readonly','true');
              }
          });
          $("#title").on("keyup", function(){
            var title = $("#title").val();
            $("#slug").val(ChangeToSlug(title));
          });

          function previewImages() {
            var $preview = $("#preview").empty();
            if (this.files) $.each(this.files, readAndPreview);
            function readAndPreview(i, file) {
              if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
                return alert(file.name +" không được hỗ trợ");
              } // else...
              var reader = new FileReader();

              $(reader).on("load", function() {
                $preview.append($("<img/>", {src:this.result, height:100}));
              });

              reader.readAsDataURL(file);
            }
          }
          $('#file-input').on("change", previewImages);
      });
    </script>
@endsection
