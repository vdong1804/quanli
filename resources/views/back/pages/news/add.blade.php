@extends('back.layout.index')
@section('content')
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Bài viết: Thêm mới</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
            <form id="form-add-news" action="{{route('add-news')}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12" for="title">Tiêu đề <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" name="title" id="title" required="required" class="form-control col-md-7 col-xs-12" value="{{old('title')}}" placeholder='Enter news title...'>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12" for="slug">Permalink:</label>
                <div class="col-md-5 col-sm-5 col-xs-3 editSlug">
                  <span>{{asset('/')}}</span>
                  <input type="text" name="slug" id="slug" required="required" class="form-control col-md-3 col-xs-12" value="{{old('slug')}}" readonly>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6 editSlugCheckbox">
                  <input type="checkbox" class="js-switch" id="edit-slug" /> Sửa
                </div>
              </div>
              <br>
              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12" for="content">Nội dung <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea id="content" class="form-control ckeditor" name="content">{{ old('content') }}</textarea>
                </div>
              </div>
              <br>
              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12">Hình Ảnh <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="custom-file-upload">
                        <input id="file-input" type="file" class="form-control hidden" name="image">
                        <i class="fa fa-cloud-upload"></i> Đặt hình ảnh nổi bật
                    </label>
                    <br>
                    <label>Xem trước:</label>
                    <div id="preview"></div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12">Trạng thái <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12 editActiveRadio">
                  <p>
                      Lưu bản nháp: <input type="radio" class="flat" name="active" id="active" value="0" /> &nbsp;&nbsp;&nbsp;
                      Công bố: <input type="radio" class="flat" name="active" id="active" value="1" checked required />
                    </p>
                </div>
              </div>

              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="{{route('all-news-author')}}">
                   <button class="btn btn-primary" type="button">Hủy</button>
                  </a>
                  <button type="submit" class="btn btn-success">Gửi</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
          $("#edit-slug").change(function(){
              if($(this).is(":checked")){
                  $("#slug").removeAttr('readonly');
              }
              else{
                  $("#slug").attr('readonly','true');
              }
          });
          $("#title").on("keyup", function(){
            var title = $("#title").val();
            $("#slug").val(ChangeToSlug(title));
          });

          function previewImages() {
            var $preview = $("#preview").empty();
            if (this.files) $.each(this.files, readAndPreview);
            function readAndPreview(i, file) {
              if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
                return alert(file.name +" không được hỗ trợ");
              } // else...
              var reader = new FileReader();

              $(reader).on("load", function() {
                $preview.append($("<img/>", {src:this.result, height:100}));
              });

              reader.readAsDataURL(file);
            }
          }
          $('#file-input').on("change", previewImages);
      });
    </script>
@endsection
