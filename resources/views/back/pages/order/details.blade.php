@extends('back.layout.index')
@section('content')
	<div class="">

	    <div class="clearfix"></div>

	    <div class="row">
	      <div class="col-md-12">
	        <div class="x_panel">
	          <div class="x_title">
	            <h2>Chi tiết đơn hàng <small>...</small></h2>
	            <ul class="nav navbar-right panel_toolbox">
	              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	              </li>
	              <li><a class="close-link"><i class="fa fa-close"></i></a>
	              </li>
	            </ul>
	            <div class="clearfix"></div>
	          </div>
	          <div class="x_content">

	            <section class="content invoice">
	              <!-- title row -->
	              <div class="row">
	                <div class="col-xs-12 invoice-header">
	                  <h1>
                          <i class="fa fa-globe"></i>
                          <small class="pull-right">Ngày: {{$current_order->created_at->format('d/m/Y')}}</small>
                      </h1>
	                </div>
	                <!-- /.col -->
	              </div>
	              <!-- info row -->
	              <div class="row invoice-info">
	                <div class="col-sm-4 invoice-col">
	                  Từ
	                  <address>
                          <strong>{{$current_order->form_name}}</strong>
                          <br>Dịa chỉ: {{$current_order->form_address}}
                          <br>Điện thoại: {{$current_order->form_phone}}
                          <br>Email: {{$current_order->form_email}}
                      </address>
	                </div>
	                <!-- /.col -->
	                <div class="col-sm-4 invoice-col">
	                  <b>Hóa đơn #{{$current_order->id}}</b>
	                  <br>
	                  <br>
	                  <b>Đơn hàng số:</b> <strong>{{$current_order->id}}</strong>
	                  <br>
	                  <b>Thanh toán do:</b> ...{{-- 2/22/2014 --}}
	                  <br>
	                  <b>Tài khoản:</b> ...{{-- 968-34567 --}}
	                </div>
	                <!-- /.col -->
	              </div>
	              <!-- /.row -->

	              <!-- Table row -->
	              <div class="row">
	                <div class="col-xs-12 table">
	                  <table class="table table-striped">
	                    <thead>
	                      <tr>
	                        <th>Số lượng</th>
	                        <th>Sản phẩmm</th>
	                        <th>Đơn vị</th>
	                        <th>Tổng tiền</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                     @foreach($order_details as $detail)
	                      <tr>
	                        <td>{{$detail->product_quantity}}</td>
	                        <td>{{$detail->to_product->name}}</td>
	                        <td>{{$detail->product_unit}}</td>
	                        <td>{{number_format($detail->product_price)}} đồng</td>
	                      </tr>
	                     @endforeach
	                    </tbody>
	                  </table>
	                </div>
	                <!-- /.col -->
	              </div>
	              <!-- /.row -->

	              <div class="row">
	                <!-- accepted payments column -->
	                <div class="col-xs-6">
	                  <p class="lead">Thanh toán do:</p>
	                  @if($current_order->payment == "ATM")
	                  	<img src="images/payment/visa.png" alt="Visa">
	                  @else
	                  	<i class="fa fa-car" style="font-size: 17px;"></i> <strong>SHIP COD</strong>
	                  @endif
	                  {{-- <img src="images/payment/mastercard.png" alt="Mastercard">
	                  <img src="images/payment/american-express.png" alt="American Express">
	                  <img src="images/payment/paypal.png" alt="Paypal"> --}}
	                </div>
	                <!-- /.col -->
	                <div class="col-xs-6">
	                  <p class="lead">Số tiền</p>
	                  <div class="table-responsive">
	                    <table class="table">
	                      <tbody>
	                        <tr>
	                          <th style="width:50%">Subtotal:</th>
	                          <td>{{number_format($current_order->amount)}} đồng</td>
	                        </tr>
	                        <tr>
	                          <th>Shipping:</th>
	                          <td>{{number_format('0')}} đồng</td>
	                        </tr>
	                        <tr>
	                          <th>Total:</th>
	                          <td>{{number_format($current_order->amount)}} đồng</td>
	                        </tr>
	                      </tbody>
	                    </table>
	                  </div>
	                </div>
	                <!-- /.col -->
	              </div>
	              <!-- /.row -->

	              <!-- this row will not appear when printing -->
	              <div class="row no-print">
	                <div class="col-xs-12">
	                  <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> In</button>
	                  <div class="pull-right">
	                  	@if($current_order->active == 0)
	                  		<a href="{{ route('order.active',$current_order->id) }}" class="btn btn-default">Xác Nhận</a>
	                  	@elseif($current_order->active == 1)
	                  		<a href="{{ route('order.active',$current_order->id) }}" class="btn btn-default">Ship hàng</a>
	                  	@elseif($current_order->active == 2)
							<a href="{{ route('order.active',$current_order->id) }}" class="btn btn-default">Hoàn thành</a>
	                  	@endif
	                  	<button class="btn btn-success"><i class="fa fa-credit-card"></i></button>
	                  </div>
	                </div>
	              </div>
	            </section>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
@endsection
