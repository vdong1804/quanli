@extends('back.layout.index')
@section('content')
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
		      <div class="x_title">
		        <h2>Đơn hàng đã xử lí<small>...</small></h2>
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
		      </div>
		      <div class="x_content">
		      	@if(Session::has('delete_success'))
	                <div class="alert alert-success">
	                    {{session('delete_success')}}
	                </div>
	            @endif
	            
		        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		          <thead>
		            <tr>
		              <th>Active</th>
		              <th>Khách hàng</th>
		              <th>Số tiền</th>
		              <th>Payment</th>
		              <th>Ngày</th>
		              <th>Chi tiết</th>
		              <th>Xóa</th>
		              <th>Tin nhắn</th>
		            </tr>
		          </thead>
		          <tbody>
		          	@foreach($orders as $order)
		          	@if($order->status == 0)
			            <tr style="text-align: center; color: #0daefe">
			              @if($order->active == 0)
			               <td>No</td>
			              @else
			               <td>Yes</td>
			              @endif
			              <td>{{$order->form_name}}</td>
			              <td>{{number_format($order->amount)}} đồng</td>
			              <td>{{$order->payment}}</td>
			              <td>{{$order->created_at->format('H:i, d M Y')}}</td>
			              <td><a href="{{route('order-details',$order->id)}}"><i class="fa fa-sliders"></i></a></td>
			              <td><a href="{{route('order-detele',$order->id)}}"><i class="fa fa-trash"></i></a></td>
			              <td>{{$order->message}}</td>
			            </tr>
			          @else
			          	<tr style="text-align: center;">
			              @if($order->active == 0)
			               <td>No</td>
			              @else
			               <td>Yes</td>
			              @endif
			              <td>{{$order->form_name}}</td>
			              <td>{{number_format($order->amount)}} đồng</td>
			              <td>{{$order->payment}}</td>
			              <td>{{$order->created_at->format('H:i, d M Y')}}</td>
			              <td><a href="{{route('order-details',$order->id)}}"><i class="fa fa-sliders"></i></a></td>
			              <td><a href="{{route('order-detele',$order->id)}}"><i class="fa fa-trash"></i></a></td>
			              <td>{{$order->message}}</td>
			            </tr>
			         @endif
			        @endforeach
		          </tbody>
		        </table>
		      </div>
		    </div>
		</div>
	</div>
@endsection
