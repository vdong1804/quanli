@extends('back.layout.index')
@section('content')
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
		      <div class="x_title">
		        <h2>Tất cả sản phẩm<small>...</small></h2>
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
		      </div>
		      <div class="x_content">
		      	@if(Session::has('delete_success'))
	                <div class="alert alert-success">
	                    {{session('delete_success')}}
	                </div>
	            @endif
		        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		          <thead>
		            <tr>
		              <th>Tên</th>
		              <th>Giá</th>
		              <th>Giảm giá</th>
		              <th>Đơn vị</th>
		              <th>Hình Ảnh</th>
		              <th>Chi tiết</th>
		              <th>Sửa</th>
		              <th>Xóa</th>
		              <th>Mổ tả</th>
		            </tr>
		          </thead>
		          <tbody>
		          	@foreach($products as $product)
		          		@php
		          			$img = json_decode($product->image);
		          		@endphp
			            <tr style="text-align: center;">
			              <td>{{$product->name}}</td>
			              <td>{{number_format($product->price)}} đồng</td>
			              <td>{{number_format($product->discount)}} đồng</td>
			              <td>{{ $product->number }} {{$product->unit}}</td>
			              <td><img width="60" height="40" src="images/product/{{$img[0]}}" alt="{{$product->slug}}" /></td>
			              <td><a href="{{route('product-details',$product->id)}}"><i class="fa fa-sliders"></i></a></td>
			              <td><a href="{{route('product-edit',$product->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('product-delete',$product->id)}}"><i class="fa fa-trash"></i></a></td>
			              <td>{{$product->description}}</td>
			            </tr>
			        @endforeach
		          </tbody>
		        </table>
		      </div>
		    </div>
		</div>
	</div>
@endsection