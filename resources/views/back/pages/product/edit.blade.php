@extends('back.layout.index')
@section('content')
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Sửa sản phẩm: {{$current_product->name}} </h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	        <br />
	        @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
	        <form id="form-edit" action="{{route('product-edit',$current_product->id)}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
	          <input type="hidden" name="_token" value="{{csrf_token()}}">
	          <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Categories</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="select2_group form-control" name="form_category">
                  	@foreach($parent_categories as $parent_category)
	                    <optgroup label="{{$parent_category->name}}">
	                     @foreach($child_categories as $child_category)
	                      	@if($child_category->parent_id == $parent_category->id)
	                       		<option
	                       			@if($current_product->category_id == $child_category->id)
	                       				{{'selected'}}
	                       			@endif
	                       			value="{{$child_category->id}}">
	                       		{{$child_category->name}}</option>
	                      	@endif
	                     @endforeach
	                    </optgroup>
                    @endforeach
                  </select>
                </div>
              </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <input type="text" name="form_name" id="first-name" required="required" class="form-control col-md-7 col-xs-12" @if(old('form_name'))
							              				value="{{old('form_name')}}"
							              		   @else
	              										value="{{$current_product->name}}"
	              								   @endif
	              		placeholder='Enter product name...'>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Giá <span class="required">*</span>
	            </label>
	            <div class="col-md-3 col-sm-3 col-xs-12">
	              <input type="number" id="last-name" name="form_price" required="required" class="form-control col-md-7 col-xs-12" @if(old('form_price'))
							              				value="{{old('form_price')}}"
							              		   @else
							              				value="{{$current_product->price}}"
							              		   @endif
							              		   >
	            </div>
	          </div>
	          <div class="form-group">
	            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Giảm giá <span class="required">*</span></label>
	            <div class="col-md-3 col-sm-3 col-xs-12">
	              <input type="number" id="last-name" name="form_discount" class="form-control col-md-7 col-xs-12" @if(old('form_discount'))
	              				value="{{old('form_discount')}}"
	              			 @else
	              				value="{{$current_product->discount}}"
	              			 @endif
	              			>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">Số lượng<span class="required">*</span>
	            </label>
	            <div class="col-md-2 col-sm-2 col-xs-12">
	              <input type="text" id="last-name" name="number" required="required" class="form-control col-md-7 col-xs-12" @if(old('form_unit'))
							              				value="{{old('number')}}"
							              			 @else
							              				value="{{$current_product->number}}"
							              			 @endif
	               		>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">Đơn vị <span class="required">*</span>
	            </label>
	            <div class="col-md-2 col-sm-2 col-xs-12">
	              <input type="text" id="last-name" name="form_unit" required="required" class="form-control col-md-7 col-xs-12" @if(old('form_unit'))
							              				value="{{old('form_unit')}}"
							              			 @else
							              				value="{{$current_product->unit}}"
							              			 @endif
	               		placeholder="Eg: hộp, cái...">
	            </div>
	          </div>

	          <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Mô tả</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea class="form-control" name="form_description" rows="3" placeholder='Enter product description...'>@if(old('form_description')){{old('form_description')}}@else{{$current_product->description}}@endif</textarea>
                </div>
              </div>
              <div class="form-group">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12">Hình ảnh <span class="required">*</span></label>
              	<div class="col-md-7 col-sm-7 col-xs-12">
              		@php
              			$img = json_decode($current_product->image);
              		@endphp
              		<table>
              			<thead>
              				<tr>
              					<th><img src="images/product/{{$img[0]}}" height="100" alt=""></th>
              					<th><img src="images/product/{{$img[1]}}" height="100" alt=""></th>
              					<th><img src="images/product/{{$img[2]}}" height="100" alt=""></th>
              				</tr>
              			</thead>
              			<tbody>
              				<tr style="text-align: center;">
              					<td>
              						<br>
              						<label class="custom-file-upload">
									    <input id="file-input-0" type="file" class="form-control hidden" name="form_images_0">
									    <i class="fa fa-cloud-upload" style="font-size: 17px;"></i>
									</label>
              					</td>
              					<td>
              						<br>
              						<label class="custom-file-upload">
									    <input id="file-input-1" type="file" class="form-control hidden" name="form_images_1">
									    <i class="fa fa-cloud-upload" style="font-size: 17px;"></i>
									</label>
              					</td>
              					<td>
              						<br>
              						<label class="custom-file-upload">
									    <input id="file-input-2" type="file" class="form-control hidden" name="form_images_2">
									    <i class="fa fa-cloud-upload" style="font-size: 17px;"></i>
									</label>
              					</td>
              				</tr>
              				<tr style="text-align: center;">
              					<td><br><div id="preview-0"></div></td>
              					<td><br><div id="preview-1"></div></td>
              					<td><br><div id="preview-2"></div></td>
              				</tr>
              			</tbody>
              		</table>
              		<br>
              	</div>
              </div>
	          <div class="ln_solid"></div>
	          <div class="form-group">
	            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	              <a href="{{route('all-products')}}">
	               <button class="btn btn-primary" type="button">Hủy</button>
	              </a>
				  <button class="btn btn-primary" type="reset">Cài lại</button>
	              <button type="submit" class="btn btn-success">Gửi</button>
	            </div>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
	// preview upload, sử dụng jquery, còn bên add thì dùng js thuần
		$(document).ready(function(){
			function previewImages_0() {
			  var $preview = $("#preview-0").empty();
			  if (this.files) $.each(this.files, readAndPreview);
			  function readAndPreview(i, file) {
			    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
			      return alert(file.name +" không được hỗ trợ");
			    } // else...
			    var reader = new FileReader();

			    $(reader).on("load", function() {
			      $preview.append($("<img/>", {src:this.result, height:100}));
			    });

			    reader.readAsDataURL(file);
			  }
			}
			function previewImages_1() {
			  var $preview = $("#preview-1").empty();
			  if (this.files) $.each(this.files, readAndPreview);
			  function readAndPreview(i, file) {
			    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
			      return alert(file.name +" không được hỗ trợ");
			    } // else...
			    var reader = new FileReader();

			    $(reader).on("load", function() {
			      $preview.append($("<img/>", {src:this.result, height:100}));
			    });

			    reader.readAsDataURL(file);
			  }
			}
			function previewImages_2() {
			  var $preview = $("#preview-2").empty();
			  if (this.files) $.each(this.files, readAndPreview);
			  function readAndPreview(i, file) {
			    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
			      return alert(file.name +" không được hỗ trợ");
			    } // else...
			    var reader = new FileReader();

			    $(reader).on("load", function() {
			      $preview.append($("<img/>", {src:this.result, height:100}));
			    });

			    reader.readAsDataURL(file);
			  }
			}

			$('#file-input-0').on("change", previewImages_0);
			$('#file-input-1').on("change", previewImages_1);
			$('#file-input-2').on("change", previewImages_2);
		});
	</script>
@endsection