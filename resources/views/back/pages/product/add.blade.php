@extends('back.layout.index')
@section('content')
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Sản phẩm: Thêm mới</h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	        <br />
	        @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
	        <form id="form-add" action="{{route('product-add-new')}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
	          <input type="hidden" name="_token" value="{{csrf_token()}}">
	          <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Categories</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="select2_group form-control" name="form_category">
                  	@foreach($parent_categories as $parent_category)
	                    <optgroup label="{{$parent_category->name}}">
	                     @foreach($child_categories as $child_category)
	                      @if($child_category->parent_id == $parent_category->id)
	                       	<option value="{{$child_category->id}}" {{ ( collect(old('form_category'))->contains($child_category->id) ) ? 'selected':'' }}>{{$child_category->name}}</option>
	                      @endif
	                     @endforeach
	                    </optgroup>
                    @endforeach
                  </select>
                </div>
              </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên  <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <input type="text" name="form_name" id="first-name" required="required" class="form-control col-md-7 col-xs-12" value="{{old('form_name')}}" placeholder='Enter product name...'>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Giá <span class="required">*</span>
	            </label>
	            <div class="col-md-3 col-sm-3 col-xs-12">
	              <input type="number" id="last-name" name="form_price" required="required" class="form-control col-md-7 col-xs-12" @if(old('form_price'))
							              				value="{{old('form_price')}}"
							              		   @else
							              				value="0"
							              		   @endif
							              		   >
	            </div>
	          </div>
	          <div class="form-group">
	            <label for="last-name" class="control-label col-md-3 col-sm-3 col-xs-12">Giảm giá <span class="required">*</span></label>
	            <div class="col-md-3 col-sm-3 col-xs-12">
	              <input type="number" id="last-name" name="form_discount" class="form-control col-md-7 col-xs-12" @if(old('form_discount'))
	              				value="{{old('form_discount')}}"
	              			 @else
	              				value="0"
	              			 @endif
	              			>
	            </div>
	          </div>
	         <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">Số lượng <span class="required">*</span>
	            </label>
	            <div class="col-md-2 col-sm-2 col-xs-12">
	              <input type="text" id="last-name" name="number" required="required" class="form-control col-md-7 col-xs-12" value="{{old('number')}}">
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">Đơn vị <span class="required">*</span>
	            </label>
	            <div class="col-md-2 col-sm-2 col-xs-12">
	              <input type="text" id="last-name" name="form_unit" required="required" class="form-control col-md-7 col-xs-12" value="{{old('form_unit')}}" placeholder="Eg: hộp, cái...">
	            </div>
	          </div>
	          <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Mô tả</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea class="form-control" name="form_description" rows="3" placeholder='Enter product description...'>{{old('form_description')}}</textarea>
                </div>
              </div>
              <div class="form-group">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12">Hình ảnh <span class="required">*</span></label>
              	<div class="col-md-6 col-sm-6 col-xs-12">
              		<label class="custom-file-upload">
					    <input id="file-input" type="file" class="form-control hidden" name="form_images[]" multiple>
					    <i class="fa fa-cloud-upload"></i> Yêu cầu chọn 3 hình ảnh cùng một lúc nếu bạn muốn thêm sản phẩm.
					</label>
              		<br>
              		<label>Preview:</label>
              		<div id="preview"></div>
              	</div>
              </div>
	          <div class="ln_solid"></div>
	          <div class="form-group">
	            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	              <a href="{{route('all-products')}}">
	               <button class="btn btn-primary" type="button">Hủy</button>
	              </a>
				  <button class="btn btn-primary" type="reset">Cài lại</button>
	              <button type="submit" class="btn btn-success">Gửi</button>
	            </div>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
	// preview upload dùng js thuần, bên edit dùng jquery
		function previewImages() {
		  var preview = document.querySelector('#preview');

		  if (this.files) {
		    [].forEach.call(this.files, readAndPreview);
		  }

		  function readAndPreview(file) {

		    // Make sure `file.name` matches our extensions criteria
		    if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
		      return alert(file.name + " không được hỗ trợ");
		    } // else...

		    var reader = new FileReader();

		    reader.addEventListener("load", function() {
		      var image = new Image();
		      image.height = 100;
		      image.title  = file.name;
		      image.src    = this.result;
		      preview.appendChild(image);
		    }, false);
		    reader.readAsDataURL(file);
		  }

		}
		document.querySelector('#file-input').addEventListener("change", previewImages, false);
	</script>
@endsection
