@extends('back.layout.index')
@section('content')
	<div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Chi tiết sản phẩm </h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            @if(Session::has('add_success'))
                <div class="alert alert-success">
                    {{session('add_success')}}
                </div>
            @endif
            @if(Session::has('edit_success'))
                <div class="alert alert-success">
                    {{session('edit_success')}}
                </div>
            @endif
          	@php
      				$img = json_decode($current_product->image);
      			@endphp
            <div class="col-md-7 col-sm-7 col-xs-12">
              <div class="product-image">
                <div class="product-detail-image-for">
					@php
						for($i=0; $i<count($img); $i++){
					@endphp
						<img src="images/product/{{$img[$i]}}" alt="">
					@php
						}
					@endphp
				</div>
              </div>
              <div class="product_gallery">
                <div class="product-detail-image-nav">
					@php
						for($i=0; $i<count($img); $i++){
					@endphp
						<a><img src="images/product/{{$img[$i]}}" alt=""></a>
					@php
						}
					@endphp
				</div>
              </div>
            </div>

            <div class="col-md-5 col-sm-5 col-xs-12" style="border:0px solid #e5e5e5;">
              <h3 class="prod_title">
              	{{$current_product->name}}
                &nbsp;
                <a href="{{route('product-edit',$current_product->id)}}"><i class="fa fa-edit"></i></a>
                &nbsp;
                <a href="{{route('product-delete',$current_product->id)}}"><i class="fa fa-trash"></i></a>
              </h3>
              <p>{{$current_product->description}}</p>
              <br />
              <div class="">
                <h2>Giá: {{number_format($current_product->price)}} đồng </h2>
                <h2>Giảm giá: {{number_format($current_product->discount)}} đồng </h2>
                <h2>Đơn vị: {{ $current_product->number }} {{ucfirst($current_product->unit)}}</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection