@extends('back.layout.index')
@section('content')
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Sửa người dùng: {{$current_user->name}}</h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	        <br />
	        @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
            @if(Session::has('edit_success'))
                <div class="alert alert-success">
                    {{session('edit_success')}}
                </div>
            @endif
	        <form id="form-edit" action="{{route('user-edit',$current_user->id)}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left input_mask">
	          <input type="hidden" name="_token" value="{{csrf_token()}}">
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-name">Tên <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="text" name="form_name" id="id-name" required="required" class="form-control col-md-7 col-xs-12 has-feedback-left"
	              @if(old('form_name'))
			        value="{{old('form_name')}}"
          		  @else
					value="{{$current_user->name}}"
				  @endif placeholder='Enter user name...'>
	                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">
	            	{{-- Gender <span class="required">*</span> --}}
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <p>
                    Male: <input type="radio" class="flat" name="form_gender" id="genderM" value="Male"
                    @if($current_user->gender=="Male"){{"checked"}}@endif /> &nbsp;&nbsp;&nbsp;
                    Female: <input type="radio" class="flat" name="form_gender" id="genderF" value="Female" @if($current_user->gender=="Female"){{"checked"}}@endif />
                  </p>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">
	            	{{-- Role <span class="required">*</span> --}}
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <p>
                    Admin: <input type="radio" class="flat" name="form_role" id="role-Ad" value="admin"
                    @if($current_user->role=="admin"){{"checked"}}@endif /> &nbsp;&nbsp;&nbsp;
                    Author: <input type="radio" class="flat" name="form_role" id="role-Au" value="author" @if($current_user->role=="author"){{"checked"}}@endif  /> &nbsp;&nbsp;&nbsp;
                    Member: <input type="radio" class="flat" name="form_role" id="role-Me" value="member" @if($current_user->role=="member"){{"checked"}}@endif />
                  </p>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-email">Email <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="email" name="form_email" id="id-email" required="required" class="form-control col-md-7 col-xs-12 has-feedback-left"
	              @if(old('form_email'))
      				value="{{old('form_email')}}"
      		      @else
						value="{{$current_user->email}}"
				  @endif placeholder='Enter email...'>
	                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-address">Địa chỉ <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="text" name="form_address" id="id-address" required="required" class="form-control col-md-7 col-xs-12 has-feedback-left"
	              @if(old('form_address'))
      				value="{{old('form_address')}}"
      		      @else
						value="{{$current_user->address}}"
				  @endif placeholder='Enter address...'>
	                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-phone">Điện thoại <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="number" name="form_phone" id="id-phone" required="required" class="form-control col-md-6 col-xs-12 has-feedback-left"
	              @if(old('form_phone'))
      				value="{{old('form_phone')}}"
      		      @else
						value="{{$current_user->phone}}"
				  @endif placeholder='Enter phone number...'>
	                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>

              <div class="form-group">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12">Hình địa diện </label>
              	<div class="col-md-6 col-sm-6 col-xs-12">
              		<table>
              			<thead>
              				<tr>
              					<th><img src="images/user/{{$current_user->image}}" height="100" alt=""></th>
              				</tr>
              			</thead>
              			<tbody>
              				<tr style="text-align: center;">
              					<td>
              						<br>
              						<label class="custom-file-upload">
									    <input id="file-input" type="file" class="form-control hidden" name="form_image">
									    <i class="fa fa-cloud-upload" style="font-size: 17px;"></i>
									</label>
              					</td>
              				</tr>
              				<tr style="text-align: center;">
              					<td><br><div id="preview"></div></td>
              				</tr>
              			</tbody>
              		</table>
              	</div>
              </div>
	          <div class="ln_solid"></div>
	          <div class="form-group">
	            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	              <a href="{{route('all-users')}}">
	               <button class="btn btn-primary" type="button">Hủy</button>
	              </a>
	              <button type="submit" class="btn btn-success">Gửi</button>
	            </div>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
	// preview upload dùng js thuần, bên edit dùng jquery
		$(document).ready(function(){
			function previewImages() {
			  var $preview = $("#preview").empty();
			  if (this.files) $.each(this.files, readAndPreview);
			  function readAndPreview(i, file) {
			    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
			      return alert(file.name +" không được hỗ trợ");
			    } // else...
			    var reader = new FileReader();

			    $(reader).on("load", function() {
			      $preview.append($("<img/>", {src:this.result, height:100}));
			    });

			    reader.readAsDataURL(file);
			  }
			}
			$('#file-input').on("change", previewImages);
		});
	</script>
@endsection