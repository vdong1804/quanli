@extends('back.layout.index')
@section('content')
	{{-- admin --}}
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			@if(count($errors)>0)
		        <div class="alert alert-danger">
		            @foreach($errors->all() as $err)
		                {{ $err }}<br>
		            @endforeach
		        </div>
		    @endif
		    @if(Session::has('add_success'))
		        <div class="alert alert-success">
		            {{session('add_success')}}
		        </div>
		    @endif
		    @if(Session::has('delete_success'))
		        <div class="alert alert-success">
		            {{session('delete_success')}}
		        </div>
		    @endif
		    @if(Session::has('delete_failed'))
                <div class="alert alert-success">
                    {{session('delete_failed')}}
                </div>
            @endif
			<div class="x_panel">
		      <div class="x_title">
		        <h2>Users: Admin <small>...</small></h2>
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
		      </div>
		      <div class="x_content">
		        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
		          <thead>
		            <tr>
		              <th>Tên</th>
		              <th>Giới tính</th>
		              <th>Email</th>
		              <th>Địa chỉ</th>
		              <th>Điện thoại</th>
		              <th>Hình Ảnh</th>
		              <th>Sửa</th>
		              <th>Xóa</th>
		            </tr>
		          </thead>
		          <tbody>
		          	@foreach($admin_users as $admin)
		          	@if($admin->status == 0)
			            <tr style="text-align: center;color: #0daefe">
			              <td>{{$admin->name}}</td>
			              <td>{{$admin->gender}}</td>
			              <td>{{$admin->email}}</td>
			              <td>{{$admin->address}}</td>
			              <td>{{$admin->phone}}</td>
			              <td><img width="50" height="50" src="images/user/{{$admin->image}}" alt="{{$admin->image}}" /></td>
			              <td><a href="{{route('user-edit',$admin->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('user-delete',$admin->id)}}"><i class="fa fa-trash"></i></a></td>
			            </tr>
			        @else
			        	<tr style="text-align: center;">
			              <td>{{$admin->name}}</td>
			              <td>{{$admin->gender}}</td>
			              <td>{{$admin->email}}</td>
			              <td>{{$admin->address}}</td>
			              <td>{{$admin->phone}}</td>
			              <td><img width="50" height="50" src="images/user/{{$admin->image}}" alt="{{$admin->image}}" /></td>
			              <td><a href="{{route('user-edit',$admin->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('user-delete',$admin->id)}}"><i class="fa fa-trash"></i></a></td>
			            </tr>
			        @endif
			        @endforeach
		          </tbody>
		        </table>
		      </div>
		    </div>
		</div>
	</div>
	{{-- author --}}
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
		      <div class="x_title">
		        <h2>Users: Author <small>...</small></h2>
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
		      </div>
		      <div class="x_content">
		        <table id="datatable-keytable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		          <thead>
		            <tr>
		              <th>Tên</th>
		              <th>Giới tính</th>
		              <th>Email</th>
		              <th>Địa chỉ</th>
		              <th>Điện thoại</th>
		              <th>Hình Ảnh</th>
		              <th>Sửa</th>
		              <th>Xóa</th>
		            </tr>
		          </thead>
		          <tbody>
		          	@foreach($author_users as $author)
			            @if($author->status == 0)
			            <tr style="text-align: center;color: #0daefe">
			              <td>{{$admin->name}}</td>
			              <td>{{$admin->gender}}</td>
			              <td>{{$admin->email}}</td>
			              <td>{{$admin->address}}</td>
			              <td>{{$admin->phone}}</td>
			              <td><img width="50" height="50" src="images/user/{{$admin->image}}" alt="{{$admin->image}}" /></td>
			              <td><a href="{{route('user-edit',$admin->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('user-delete',$admin->id)}}"><i class="fa fa-trash"></i></a></td>
			            </tr>
			        @else
			        	<tr style="text-align: center;">
			              <td>{{$admin->name}}</td>
			              <td>{{$admin->gender}}</td>
			              <td>{{$admin->email}}</td>
			              <td>{{$admin->address}}</td>
			              <td>{{$admin->phone}}</td>
			              <td><img width="50" height="50" src="images/user/{{$admin->image}}" alt="{{$admin->image}}" /></td>
			              <td><a href="{{route('user-edit',$admin->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('user-delete',$admin->id)}}"><i class="fa fa-trash"></i></a></td>
			            </tr>
			        @endif
			        @endforeach
		          </tbody>
		        </table>
		      </div>
		    </div>
		</div>
	</div>
	{{-- member --}}
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
		      <div class="x_title">
		        <h2>Người dùng: Member <small>...</small></h2>
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
		      </div>
		      <div class="x_content">
		        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		          <thead>
		            <tr>
		             <th>Tên</th>
		              <th>Giới tính</th>
		              <th>Email</th>
		              <th>Địa chỉ</th>
		              <th>Điện thoại</th>
		              <th>Hình Ảnh</th>
		              <th>Sửa</th>
		              <th>Xóa</th>
		            </tr>
		          </thead>
		          <tbody>
		          	@foreach($member_users as $member)
			            @if($member->status == 0)
			            <tr style="text-align: center;color: #0daefe">
			              <td>{{$admin->name}}</td>
			              <td>{{$admin->gender}}</td>
			              <td>{{$admin->email}}</td>
			              <td>{{$admin->address}}</td>
			              <td>{{$admin->phone}}</td>
			              <td><img width="50" height="50" src="images/user/{{$admin->image}}" alt="{{$admin->image}}" /></td>
			              <td><a href="{{route('user-edit',$admin->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('user-delete',$admin->id)}}"><i class="fa fa-trash"></i></a></td>
			            </tr>
			        @else
			        	<tr style="text-align: center;">
			              <td>{{$admin->name}}</td>
			              <td>{{$admin->gender}}</td>
			              <td>{{$admin->email}}</td>
			              <td>{{$admin->address}}</td>
			              <td>{{$admin->phone}}</td>
			              <td><img width="50" height="50" src="images/user/{{$admin->image}}" alt="{{$admin->image}}" /></td>
			              <td><a href="{{route('user-edit',$admin->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('user-delete',$admin->id)}}"><i class="fa fa-trash"></i></a></td>
			            </tr>
			        @endif
			        @endforeach
		          </tbody>
		        </table>
		      </div>
		    </div>
		</div>
	</div>
@endsection