@extends('back.layout.index')
@section('content')
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Người dùng: Thêm mới</h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	        <br />
	        @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
	        <form id="form-add" action="{{route('user-add-new')}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left input_mask">
	          <input type="hidden" name="_token" value="{{csrf_token()}}">
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-name">Tên <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="text" name="form_name" id="id-name" required="required" class="form-control col-md-7 col-xs-12 has-feedback-left" value="{{old('form_name')}}" placeholder='Enter user name...'>
	                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">
	            	{{-- Gender <span class="required">*</span> --}}
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <p>
                    Male: <input type="radio" class="flat" name="form_gender" id="genderM" value="Male" checked required /> &nbsp;&nbsp;&nbsp;
                    Female: <input type="radio" class="flat" name="form_gender" id="genderF" value="Female" />
                  </p>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">
	            	{{-- Role <span class="required">*</span> --}}
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <p>
                    Admin: <input type="radio" class="flat" name="form_role" id="role-Ad" value="admin" /> &nbsp;&nbsp;&nbsp;
                    Author: <input type="radio" class="flat" name="form_role" id="role-Au" value="author" /> &nbsp;&nbsp;&nbsp;
                    Member: <input type="radio" class="flat" name="form_role" id="role-Me" value="member" checked required />
                  </p>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-email">Email <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="email" name="form_email" id="id-email" required="required" class="form-control col-md-7 col-xs-12 has-feedback-left" value="{{old('form_email')}}" placeholder='Enter email...'>
	                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-password">Mật khẩu <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="password" name="form_password" id="id-password" required="required" class="form-control col-md-7 col-xs-12 has-feedback-left" value="" placeholder='Enter password...'>
	                <span class="fa fa-ellipsis-h form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-re-password">Xác nhận mật khẩu <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="password" name="form_re_password" id="id-re-password" required="required" class="form-control col-md-7 col-xs-12 has-feedback-left" value="" placeholder='Enter re-password...'>
	                <span class="fa fa-ellipsis-h form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-address">Địa chỉ <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="text" name="form_address" id="id-address" required="required" class="form-control col-md-7 col-xs-12 has-feedback-left" value="{{old('form_address')}}" placeholder='Enter address...'>
	                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-phone">Điện thoại <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="number" name="form_phone" id="id-phone" required="required" class="form-control col-md-6 col-xs-12 has-feedback-left" value="{{old('form_phone')}}" placeholder='Enter phone number...'>
	                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
	            </div>
	          </div>

              <div class="form-group">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12">Hình đại diện </label>
              	<div class="col-md-6 col-sm-6 col-xs-12">
              		<label class="custom-file-upload">
					    <input id="file-input" type="file" class="form-control hidden" name="form_image">
					    <i class="fa fa-cloud-upload"></i>&nbsp; Nếu không tải lên, hình ảnh mặc định sẽ được sử dụng.
					</label>
              		<br>
              		<label>Xem trước:</label>
              		<div id="preview"></div>
              	</div>
              </div>
	          <div class="ln_solid"></div>
	          <div class="form-group">
	            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	              <a href="{{route('all-users')}}">
	               <button class="btn btn-primary" type="button">Hủy</button>
	              </a>
				  <button class="btn btn-primary" type="reset">Cài lại</button>
	              <button type="submit" class="btn btn-success">Gửi</button>
	            </div>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
	// preview upload dùng js thuần, bên edit dùng jquery
		$(document).ready(function(){
			function previewImages() {
			  var $preview = $("#preview").empty();
			  if (this.files) $.each(this.files, readAndPreview);
			  function readAndPreview(i, file) {
			    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
			      return alert(file.name +" không được hỗ trợ");
			    } // else...
			    var reader = new FileReader();

			    $(reader).on("load", function() {
			      $preview.append($("<img/>", {src:this.result, height:100}));
			    });

			    reader.readAsDataURL(file);
			  }
			}
			$('#file-input').on("change", previewImages);
		});
	</script>
@endsection