@extends('back.layout.index')
@section('content')
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			@if(count($errors)>0)
			    <div class="alert alert-danger">
			        @foreach($errors->all() as $err)
			            {{ $err }}<br>
			        @endforeach
			    </div>
			@endif
		</div>
	</div>
@endsection