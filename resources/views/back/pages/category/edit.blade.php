@extends('back.layout.index')
@section('content')
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			@if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
            @if(Session::has('edit_success'))
                <div class="alert alert-success">
                    {{session('edit_success')}}
                </div>
            @endif
            @if(Session::has('delete_failed'))
                <div class="alert alert-success">
                    {{session('delete_failed')}}
                </div>
            @endif
			<div class="x_panel">
		      <div class="x_title">
		        <h2>Sửa danh mục: {{$current_category->name}}</h2>
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
		      </div>
		      <div class="x_content">
		        <br />
	            <form id="form-edit-category" action="{{route('category-edit',$current_category->id)}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
		          <input type="hidden" name="_token" value="{{csrf_token()}}">
		          @if($current_category->parent_id != null)
			          <div class="form-group">
		                <label class="control-label col-md-3 col-sm-3 col-xs-12">Danh mục gốc</label>
		                <div class="col-md-6 col-sm-6 col-xs-12">
		                  <select class="select2_group form-control" name="form_category">
		                  	@foreach($parent_categories as $parent_category)
			                   <option value="{{$parent_category->id}}"
			                   			@if($current_category->parent_id == $parent_category->id)
		                       				{{'selected'}}
		                       			@endif
		                       			>{{$parent_category->name}}</option>
		                    @endforeach
		                  </select>
		                </div>
		              </div>
	              @endif
		          <div class="form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input type="text" name="form_name" id="id-name" required="required" class="form-control col-md-7 col-xs-12" @if(old('form_name'))
								              				value="{{old('form_name')}}"
								              		   @else
		              										value="{{$current_category->name}}"
		              								   @endif
	              						placeholder='Enter category name...'>
		            </div>
		          </div>
		          <div class="form-group">
	                <label class="control-label col-md-3 col-sm-3 col-xs-12">Mô tả</label>
	                <div class="col-md-6 col-sm-6 col-xs-12">
	                  <textarea class="form-control" name="form_description" rows="3" placeholder='Enter category description...'>@if(old('form_description')){{old('form_description')}}@else{{$current_category->description}}@endif</textarea>
	                </div>
	              </div>
		          <div class="ln_solid"></div>
		          <div class="form-group">
		            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <button class="btn btn-primary" type="reset">Cài lại</button>
		              <button type="submit" class="btn btn-success">Gửi</button>
		            </div>
		          </div>
		        </form>
	          </div>
	        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
		      <div class="x_title">
		        <h2>Danh mục gốc<small>...</small></h2>
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
		      </div>
		      <div class="x_content">
		        <table id="datatable-keytable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		          <thead>
		            <tr>
		              <th>Tên</th>
		              <th width="50%">Mô tả</th>
		              <th>Sửa</th>
		              <th>Xóa</th>
		            </tr>
		          </thead>
		          <tbody>
		          	@foreach($parent_categories as $parent_categorie)
			            <tr style="text-align: center;">
			              <td>{{$parent_categorie->name}}</td>
			              <td>{{$parent_categorie->description}}</td>
			              <td><a href="{{route('category-edit',$parent_categorie->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('category-delete',$parent_categorie->id)}}"><i class="fa fa-trash"></i></a></td>
			            </tr>
			        @endforeach
		          </tbody>
		        </table>
		      </div>
		    </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
		      <div class="x_title">
		        <h2>Danh mục con<small>...</small></h2>
		        <ul class="nav navbar-right panel_toolbox">
		          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		          </li>
		          <li><a class="close-link"><i class="fa fa-close"></i></a>
		          </li>
		        </ul>
		        <div class="clearfix"></div>
		      </div>
		      <div class="x_content">
		        <table id="datatable-responsive" class="table table-striped table-bordered" cellspacing="0" width="100%">
		          <thead>
		            <tr>
		              <th>Tên</th>
		              <th width="60%">Mô tả</th>
		              <th>Sửa</th>
		              <th>Xóa</th>
		            </tr>
		          </thead>
		          <tbody>
		          	@foreach($child_categories as $child_category)
			            <tr style="text-align: center;">
			              <td>{{$child_category->name}}</td>
			              <td>{{$child_category->description}}</td>
			              <td><a href="{{route('category-edit',$child_category->id)}}"><i class="fa fa-edit"></i></a></td>
			              <td><a href="{{route('category-delete',$child_category->id)}}"><i class="fa fa-trash"></i></a></td>
			            </tr>
			        @endforeach
		          </tbody>
		        </table>
		      </div>
		    </div>
		</div>
	</div>
@endsection