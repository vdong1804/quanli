@extends('back.layout.index')
@section('content')
	{{-- actived --}}
	<div class="row">
      <div class="col-md-12">
      	@if(count($errors)>0)
	        <div class="alert alert-danger">
	            @foreach($errors->all() as $err)
	                {{ $err }}<br>
	            @endforeach
	        </div>
	    @endif
	    @if(Session::has('add_success'))
	        <div class="alert alert-success">
	            {{session('add_success')}}
	        </div>
	    @endif
	    @if(Session::has('delete_success'))
	        <div class="alert alert-success">
	            {{session('delete_success')}}
	        </div>
	    @endif
	    @if(Session::has('delete_failed'))
            <div class="alert alert-success">
                {{session('delete_failed')}}
            </div>
        @endif
        <div class="x_panel">
          <div class="x_title">
            <h2>Slides Gallery <small> Actived </small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">

              {{-- <p>Media gallery design emelents</p> --}}
              @foreach($active_slides as $active)
	              <div class="col-md-55" style="width: 25%">
	                <div class="thumbnail">
	                  <div class="image view view-first">
	                    <img style="width: 100%; display: block;" src="images/slide/{{$active->image}}" alt="{{$active->slug}}" />
	                    <div class="mask">
	                      <p>Actived</p>
	                      <div class="tools tools-bottom">
	                        {{-- <a href="#"><i class="fa fa-link"></i></a> --}}
	                        <a href="{{route('slide-edit',$active->id)}}"><i class="fa fa-pencil"></i></a>
	                        <a href="{{route('slide-delete',$active->id)}}"><i class="fa fa-times"></i></a>
	                      </div>
	                    </div>
	                  </div>
	                  <div class="caption">
	                    <p>{{$active->title}}</p>
	                  </div>
	                </div>
	              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
	{{-- no active --}}
	<div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Slides Gallery <small> No Active </small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">

              {{-- <p>Media gallery design emelents</p> --}}
              @foreach($no_active_slides as $no_active)
	              <div class="col-md-55">
	                <div class="thumbnail">
	                  <div class="image view view-first">
	                    <img style="width: 100%; display: block;" src="images/slide/{{$no_active->image}}" alt="{{$no_active->slug}}" />
	                    <div class="mask">
	                      <p>No active</p>
	                      <div class="tools tools-bottom">
	                        {{-- <a href="#"><i class="fa fa-link"></i></a> --}}
	                        <a href="{{route('slide-edit',$no_active->id)}}"><i class="fa fa-pencil"></i></a>
	                        <a href="{{route('slide-delete',$no_active->id)}}"><i class="fa fa-times"></i></a>
	                      </div>
	                    </div>
	                  </div>
	                  <div class="caption">
	                    <p>{{$no_active->title}}</p>
	                  </div>
	                </div>
	              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection