@extends('back.layout.index')
@section('content')
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Slide Sửa: {{$current_slide->title}}</h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	        <br />
	        @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
            @if(Session::has('edit_success'))
                <div class="alert alert-success">
                    {{session('edit_success')}}
                </div>
            @endif
	        <form id="form-edit" action="{{route('slide-edit',$current_slide->id)}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left input_mask">
	          <input type="hidden" name="_token" value="{{csrf_token()}}">
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-title">Tiêu đề <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="text" name="form_title" id="id-title" required="required" class="form-control col-md-7 col-xs-12" @if(old('form_title'))
	              										value="{{old('form_title')}}"
	              									@else
	              										value="{{$current_slide->title}}"
	              									@endif placeholder='Enter title...'>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">
	            	{{-- Gender <span class="required">*</span> --}}
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <p>
                    Active: <input type="radio" class="flat" name="form_active" id="activeY" value="1" @if($current_slide->active=="1"){{"checked"}}@endif /> &nbsp;&nbsp;&nbsp;
                    No Active: <input type="radio" class="flat" name="form_active" id="activeN" value="0" @if($current_slide->active=="0"){{"checked"}}@endif />
                  </p>
	            </div>
	          </div>

              <div class="form-group">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12">Hình Ảnh <span class="required">*</span></label>
              	<div class="col-md-6 col-sm-6 col-xs-12">
              		<table>
              			<thead>
              				<tr>
              					<th><img src="images/slide/{{$current_slide->image}}" height="100" alt=""></th>
              				</tr>
              			</thead>
              			<tbody>
              				<tr style="text-align: center;">
              					<td>
              						<br>
              						<label class="custom-file-upload">
									    <input id="file-input" type="file" class="form-control hidden" name="form_image">
									    <i class="fa fa-cloud-upload" style="font-size: 17px;"></i>
									</label>
              					</td>
              				</tr>
              				<tr style="text-align: center;">
              					<td><br><div id="preview"></div></td>
              				</tr>
              			</tbody>
              		</table>
              	</div>
              </div>
	          <div class="ln_solid"></div>
	          <div class="form-group">
	            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	              <a href="{{route('all-slides')}}">
	               <button class="btn btn-primary" type="button">Hủy</button>
	              </a>
	              <button type="submit" class="btn btn-success">Gửi</button>
	              <a href="{{route('slide-delete',$current_slide->id)}}">
	               <button class="btn btn-danger" type="button">Xóa</button>
	              </a>
	            </div>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
	// preview upload dùng js thuần, bên edit dùng jquery
		$(document).ready(function(){
			function previewImages() {
			  var $preview = $("#preview").empty();
			  if (this.files) $.each(this.files, readAndPreview);
			  function readAndPreview(i, file) {
			    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
			      return alert(file.name +" không được hỗ trợ");
			    } // else...
			    var reader = new FileReader();

			    $(reader).on("load", function() {
			      $preview.append($("<img/>", {src:this.result, height:100}));
			    });

			    reader.readAsDataURL(file);
			  }
			}
			$('#file-input').on("change", previewImages);
		});
	</script>
@endsection