@extends('back.layout.index')
@section('content')
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Slide: Thêm mới</h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	        <br />
	        @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        {{ $err }}<br>
                    @endforeach
                </div>
            @endif
	        <form id="form-add" action="{{route('slide-add-new')}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left input_mask">
	          <input type="hidden" name="_token" value="{{csrf_token()}}">
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id-title">Tiêu đề <span class="required">*</span>
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
	              <input type="text" name="form_title" id="id-title" required="required" class="form-control col-md-7 col-xs-12" value="{{old('form_title')}}" placeholder='Enter title...'>
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="control-label col-md-3 col-sm-3 col-xs-12">
	            	{{-- Gender <span class="required">*</span> --}}
	            </label>
	            <div class="col-md-6 col-sm-6 col-xs-12">
	              <p>
                    Active: <input type="radio" class="flat" name="form_active" id="activeY" value="1" /> &nbsp;&nbsp;&nbsp;
                    No Active: <input type="radio" class="flat" name="form_active" id="activeN" value="0" checked required />
                  </p>
	            </div>
	          </div>

              <div class="form-group">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12">Hình Ảnh <span class="required">*</span></label>
              	<div class="col-md-6 col-sm-6 col-xs-12">
              		<label class="custom-file-upload">
					    <input id="file-input" type="file" class="form-control hidden" name="form_image">
					    <i class="fa fa-cloud-upload"></i>&nbsp; Required
					</label>
              		<br>
              		<label>Xem trước:</label>
              		<div id="preview"></div>
              	</div>
              </div>
	          <div class="ln_solid"></div>
	          <div class="form-group">
	            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	              <a href="{{route('all-slides')}}">
	               <button class="btn btn-primary" type="button">Hủy</button>
	              </a>
				  <button class="btn btn-primary" type="reset">Cài lại</button>
	              <button type="submit" class="btn btn-success">Gửi</button>
	            </div>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
		$(document).ready(function(){
			function previewImages() {
			  var $preview = $("#preview").empty();
			  if (this.files) $.each(this.files, readAndPreview);
			  function readAndPreview(i, file) {
			    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
			      return alert(file.name +" không được hỗ trợ");
			    } // else...
			    var reader = new FileReader();

			    $(reader).on("load", function() {
			      $preview.append($("<img/>", {src:this.result, height:100}));
			    });

			    reader.readAsDataURL(file);
			  }
			}
			$('#file-input').on("change", previewImages);
		});
	</script>
@endsection