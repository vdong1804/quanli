@extends('front.layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Đơn hàng của bạn:</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="{{route('home')}}">Home</a> / <span><a href="{{route('profile')}}">Orders</a></span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br>
    <div class="table-responsive">
        <!-- Shop Products Table -->
        <table class="shop_table beta-shopping-cart-table" cellspacing="0">
            <thead>
                <tr>
                    <th>Đơn hàng</th>
                    <th>Tổng tiền</th>
                    <th>Hình thức thanh toán</th>
                    <th>Chi tiết</th>
                    <th>Thời gian</th>
                </tr>
            </thead>
            <tbody>
                @php
                 $stt = 1;
                @endphp
                @foreach($orders as $order)
                    <tr class="cart_item">
                        <td>
                            {{$stt}}
                        </td>

                        <td>
                            {{number_format($order->amount)}} đồng
                        </td>

                        <td>
                            {{$order->payment}}
                        </td>

                        <td>
                            <a href="{{route('profile-order-detail',$order->id)}}"><i class="fa fa-sliders"></i></a>
                        </td>
                        <td>
                            {{$order->created_at->format('H:i, d/m/Y')}}
                        </td>
                    </tr>
                    @php $stt++; @endphp
                @endforeach
            </tbody>
        </table>
        <!-- End of Shop Table Products -->
        <br>
    </div>
@endsection
