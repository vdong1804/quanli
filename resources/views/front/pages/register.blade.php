@extends('front.layout.index')
@section('content')
	<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Đăng ký</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb">
					<a href="{{route('home')}}">Home</a> / <span><a href="{{route('register')}}">Đăng ký</a></span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="container">
		<div id="content">
			@if(Session::has('flag') || Auth::check())
				<div class="alert alert-{{ Session::get('flag') }}">
					{{ Session::get('notif') }}
				</div>
				<div class="col-sm-6">
					Chào bạn <strong>{{Auth::user()->name}}</strong> đã quay trở lại, chúc bạn một ngày tốt lành. <br>
					Hãy xem hôm nay shop có gì mới dành cho bạn nhé.<br>
					--Siêu thị Tân Hường--
				</div>
				<br><br>
			@else
				<form action="{{route('register')}}" method="post" enctype="multipart/form-data" class="beta-form-checkout">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="row">
						<div class="col-sm-3"></div>
						@if(count($errors)>0)
							<div class="alert alert-danger">
								@foreach($errors->all() as $err)
									{{$err}}<br>
								@endforeach
							</div>
						@endif

						<div class="col-sm-6">
							<h4>Đăng ký tài khoản mua hàng</h4>
							<div class="space20">&nbsp;</div>
							<div class="form-block">
								<label for="email">Địa chỉ Email*</label>
								<input type="email" id="id_email" name="form_email" value="{{ old('form_email') }}" class="form-control" placeholder="example@tanhuong.com" required>
							</div>

							<div class="form-block">
								<label for="fullname">Họ tên*</label>
								<input type="text" id="id_fullname" name="form_fullname" class="form-control" value="{{ old('form_fullname') }}" required>
							</div>

							<div class="form-block">
	                            <label for="gender">Giới tính*</label>
                            	<span>Nam</span>
                            		<input type="radio" class="radio_gender" name="form_gender" value="Male" checked="checked">
                            	<span>&nbsp;&nbsp;&nbsp;</span>
                            	<span>Nữ</span>
                            		<input type="radio" class="radio_gender" name="form_gender" value="Female">
	                        </div>

							<div class="form-block">
								<label for="address">Địa chỉ*</label>
								<input type="text" id="id_address" name="form_address" class="form-control" value="{{ old('form_address') }}" required>
							</div>

							<div class="form-block">
								<label for="phone">Điện thoại*</label>
								<input type="text" id="id_phone" name="form_phone" class="form-control" value="{{ old('form_phone') }}" required>
							</div>
							<div class="form-block">
								<label for="password">Mật khẩu*</label>
								<input type="password" id="id_password" name="form_password" class="form-control" placeholder="Enter password" required>
							</div>
							<div class="form-block">
								<label for="re_password">Nhập lại mật khẩu*</label>
								<input type="password" id="id_re_password" name="form_re_password" class="form-control" placeholder="Enter re-password" required>
							</div>

							<div class="form-block">
	                            <label for="avatar">Hình đại diện</label>
	                            <input type="file" id="file-input" class="form-control" name="form_image"/>
	                            <label>Preview:</label>
              					<div id="preview"></div>
	                        </div>

							<div class="form-block">
								<label for="button">&nbsp;</label>
								<button type="submit" class="btn btn-submit">Đăng ký</button>
								<span>&nbsp;&nbsp;&nbsp;</span>
								<button type="reset" class="btn btn-reset">Nhập lại</button>
							</div>
						</div>
						<div class="col-sm-3"></div>
					</div>
				</form>
			@endif
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection
@section('script')
	<script type="text/javascript">
		$(document).ready(function(){
			function previewImages() {
			  var $preview = $("#preview").empty();
			  if (this.files) $.each(this.files, readAndPreview);
			  function readAndPreview(i, file) {
			    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
			      return alert(file.name +" không được hỗ trợ");
			    } // else...
			    var reader = new FileReader();

			    $(reader).on("load", function() {
			      $preview.append($("<img/>", {src:this.result, height:100}));
			    });

			    reader.readAsDataURL(file);
			  }
			}
			$('#file-input').on("change", previewImages);
		});
	</script>
@endsection