@extends('front.layout.index')
@section('content')
@php
	function change_color($str, $keyword){
        return str_ireplace($keyword,'<span style="color:#e74c3c; font-weight: bold;">'.$keyword.'</span>', $str);
    }
@endphp
	<div class="container">
		<div id="content" class="space-top-none">
			<div class="main-content">
				<div class="space40">&nbsp;</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="beta-products-list">
							<h4>Tìm kiếm: <span style='color:#e74c3c;'>{{$keyword}}</span></h4>
							<div class="beta-products-details">
								<p class="pull-left">Đang hiển thị: {{ count($search_products) }} sản phẩm</p>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								@foreach($search_products as $product)
									<div class="col-sm-3">
										<div class="single-item">
											@if($product->discount != 0)
												<div class="ribbon-wrapper">
													<div class="ribbon sale">Sale</div>
												</div>
											@endif
											@php
												$img = json_decode($product->image);
											@endphp
											<div class="single-item-header">
												<a href="{{route('product-detail',$product->slug)}}">
													<img src="images/product/{{$img['0']}}" alt="" height="250px">
												</a>
											</div>
											<div class="single-item-body">
												<p class="single-item-title">
													{!! change_color($product->name, ucfirst($keyword)) !!}
												</p>
												<p class="single-item-price">
													@if($product->discount == 0)
														<span class="flash-sale">{{number_format($product->price)}} đồng</span>
													@else
														<span class="flash-del">{{number_format($product->price)}} đồng</span>
														<span class="flash-sale">{{number_format($product->discount)}} đồng</span>
													@endif
												</p>
											</div>
											<div class="single-item-caption">
												<a class="add-to-cart pull-left" href="{{route('add-to-cart',$product->id)}}"><i class="fa fa-shopping-cart"></i></a>
												<a class="beta-btn primary" href="{{route('product-detail',$product->slug)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div> <!-- .beta-products-list -->
						<div style="text-align: left">
                        	{{ $search_products->links() }}
                        </div>
					</div>
				</div> <!-- end section with sidebar and main content -->
			</div> <!-- .main-content -->
		</div> <!-- #content -->
@endsection
