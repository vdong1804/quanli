@extends('front.layout.index')
@section('content')
	<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title"></h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb">
					<a href="{{route('home')}}">Trang chủ</a> / <span><a href="{{route('checkout')}}">Đặt hàng</a></span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="container">
		<div id="content">
			@if(count($errors)>0)
				<div class="alert alert-danger">
					@foreach($errors->all() as $err)
						{{$err}}<br>
					@endforeach
				</div>
			@endif
			@if(Session::has('checkout_success'))
				<div class="alert alert-success">
					{{ Session::get('checkout_success') }}
				</div>
			@endif

			<div class="row">
				@if(!Auth::check())
										<form action="{{route('checkout')}}" method="post" class="beta-form-checkout">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="col-sm-6">
							<h4>Đặt hàng</h4>
							<div class="space20">&nbsp;</div>

							<div class="form-block">
								<label for="name">Họ tên*</label>
								<input type="text" name="form_name" >
							</div>

							<div class="form-block">
								<label for="email">Email*</label>
								<input type="email" name="form_email" >
							</div>

							<div class="form-block">
								<label for="adress">Địa chỉ*</label>
								<input type="text" name="form_address" >
							</div>
							<div class="form-block">
								<label for="phone">Điện thoại*</label>
								<input type="text" name="form_phone" >
							</div>
							<div class="form-block">
								<label for="notes">Ghi chú</label>
								<textarea name="form_notes" maxlength="100"></textarea>
							</div>

							<div class="your-order">
								<div class="your-order-head"><h5>Hình thức thanh toán</h5></div>
								<div class="your-order-body">
									<ul class="payment_methods methods">
										<li class="payment_method_bacs">
											<input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="COD" data-order_button_text="">
											<label for="payment_method_bacs">Thanh toán khi nhận hàng </label>
											<div class="payment_box payment_method_bacs" style="display: none;">
												Si sẽ gửi hàng đến địa chỉ của bạn, bạn xem hàng rồi thanh toán tiền cho nhân viên giao hàng
											</div>
										</li>

										<li class="payment_method_cheque">
											<input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="ATM" checked="checked" data-order_button_text="">
											<label for="payment_method_cheque">Chuyển khoản </label>
											<div class="payment_box payment_method_cheque" style="display: block;">
												<input type="text" name="card_no" value="" placeholder="Visa Card...">
												<input type="text" name="exp_month" value="" placeholder="Expire month...">
												<input type="text" name="exp_year" value="" placeholder="Expire year...">
												<input type="text" name="CVC" value="" placeholder="CVC...">
											</div>
										</li>
									</ul>
								</div>
								<div class="text-center">
									<button type="submit" class="beta-btn primary checkout-btn">
										Đặt hàng <i class="fa fa-chevron-right"></i>
									</button>
								</div>
							</div>
						</div>
					</form>
				@else
					<form action="{{route('checkout')}}" method="post" class="beta-form-checkout">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="col-sm-6">
							<h4>Đặt hàng</h4>
							<div class="space20">&nbsp;</div>

							<div class="form-block">
								<label for="name">Họ tên*</label>
								<input type="text" name="form_name" value="{{Auth::user()->name}}" >
							</div>

							<div class="form-block">
								<label for="email">Email*</label>
								<input type="email" name="form_email" value="{{Auth::user()->email}}" >
							</div>

							<div class="form-block">
								<label for="adress">Địa chỉ*</label>
								<input type="text" name="form_address" value="{{Auth::user()->address}}" >
							</div>
							<div class="form-block">
								<label for="phone">Điện thoại*</label>
								<input type="text" name="form_phone" value="{{Auth::user()->phone}}">
							</div>
							<div class="form-block">
								<label for="notes">Ghi chú</label>
								<textarea name="form_notes" maxlength="100"></textarea>
							</div>

							<div class="your-order">
								<div class="your-order-head"><h5>Hình thức thanh toán</h5></div>
								<div class="your-order-body">
									<ul class="payment_methods methods">
										<li class="payment_method_bacs">
											<input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="COD" data-order_button_text="">
											<label for="payment_method_bacs">Thanh toán khi nhận hàng </label>
											<div class="payment_box payment_method_bacs" style="display: none;">
												Si sẽ gửi hàng đến địa chỉ của bạn, bạn xem hàng rồi thanh toán tiền cho nhân viên giao hàng
											</div>
										</li>

										<li class="payment_method_cheque">
											<input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="ATM" checked="checked" data-order_button_text="">
											<label for="payment_method_cheque">Chuyển khoản </label>
											<div class="payment_box payment_method_cheque" style="display: block;">
												<input type="text" name="card_no" value="" placeholder="Visa Card...">
												<input type="text" name="exp_month" value="" placeholder="Expire month...">
												<input type="text" name="exp_year" value="" placeholder="Expire year...">
												<input type="text" name="CVC" value="" placeholder="CVC...">
											</div>
										</li>
									</ul>
								</div>
								<div class="text-center">
									<button type="submit" class="beta-btn primary checkout-btn">
										Đặt hàng <i class="fa fa-chevron-right"></i>
									</button>
								</div>
							</div>
						</div>
					</form>
				@endif
				<div class="col-sm-6">
					<div class="your-order">
						<div class="your-order-head"><h5>Đơn hàng của bạn</h5></div>
						<div class="your-order-body" style="padding: 0px 10px">
							<div class="your-order-item">
								<div>
								<!--  one item	 -->
								@if(Session::has('cart'))
						 		 @foreach($products_cart as $product)
									<div class="media">
										@php
											$img = json_decode($product['item']['image']);
										@endphp
										<img style="width: 25%; height: 100px;" src="images/product/{{$img['0']}}" alt="" class="pull-left">
										<div class="media-body">
											<p class="font-large">{{ $product['item']['name'] }}</p>
											<span class="cart-item-options">
												Số lượng:
												<span class="cart-value">
													{{$product['qty']}} {{$product['unit']}}
												</span>
											</span>
											<br>
											<span class="cart-item-amount">
												Thành tiền:
												<span class="cart-value">
													{{number_format($product['price'])}} đồng
												</span>
											</span>
										</div>
									</div>
								 @endforeach
								@endif
								<!-- end one item -->
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="your-order-item">
								<div class="pull-left cart-value">
									<p class="your-order-f18">Tổng tiền:</p>
								</div>
								<div class="pull-right">
									<h5 class="cart-value">
										@if(Session::has('cart'))
											{{number_format(Session('cart')->totalPrice)}}
										@else
											0
										@endif
										đồng
									</h5>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div> <!-- .your-order -->
				</div>
			</div>
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection
