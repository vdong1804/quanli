@extends('front.layout.index')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title"><a href="{{route('category-product',$current_category->slug)}}">{{$current_category->name}}</a></h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{route('home')}}">Home</a> / <a href="{{route('category-product',$current_category->slug)}}">{{$current_category->name}}</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content" class="space-top-none">
			<div class="main-content">
				<div class="space10">&nbsp;</div>
				<div class="row">
					<div class="col-sm-3">
						<ul class="aside-menu">
							@foreach($similarCategory as $category)
								<li>
									<a href="{{route('category-product',$category->slug)}}">{{$category->name}}</a>
								</li>
							@endforeach
						</ul>
					</div>
					<div class="col-sm-9">
						<div class="beta-products-list">
							<h4><a href="{{route('category-product',$current_category->slug)}}">{{$current_category->name}}</a> / Sản Phẩm Mới</h4>
							<div class="beta-products-details">
								<p class="pull-left">Đang hiển thị: {{count($category_products)}} sản phẩm</p>
								<div class="clearfix"></div>
							</div>

							<div class="row">
								@foreach($category_products as $product)
								<div class="col-sm-4">
									<div class="single-item">
										@if($product->discount != 0)
											<div class="ribbon-wrapper">
												<div class="ribbon sale">Sale</div>
											</div>
										@endif
										@php
											$img = json_decode($product->image);
									    @endphp
										<div class="single-item-header">
											<a href="{{route('product-detail',$product->slug)}}">
												<img src="images/product/{{$img['0']}}" alt="" height="250px">
											</a>
										</div>
										<div class="single-item-body">
											<p class="single-item-title">{{$product->name}}</p>
											<p class="single-item-price">
												@if($product->discount != 0)
													<span class="flash-del">{{number_format($product->price)}} đồng</span>
													<span class="flash-sale">{{number_format($product->discount)}} đồng</span>
												@else
													<span class="flash-sale">{{number_format($product->price)}} đồng</span>
												@endif
											</p>
										</div>
										<div class="single-item-caption">
											<a class="add-to-cart pull-left" href="{{route('add-to-cart',$product->id)}}"><i class="fa fa-shopping-cart"></i></a>
											<a class="beta-btn primary" href="{{route('product-detail',$product->slug)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							<div class="row">{{$category_products->links()}}</div>
						</div> <!-- .beta-products-list -->

						<div class="space40">&nbsp;</div>

						<div class="beta-products-list">
							<h4>Sản Phẩm Khác</h4>
							<div class="beta-products-details">
								<p class="pull-left">Đang hiển thị: {{count($other_products)}} sản phẩm</p>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<div class="other-products-data">
									@foreach($other_products as $product)
									<div class="col-sm-4">
										<div class="single-item">
											@if($product->discount != 0)
												<div class="ribbon-wrapper">
													<div class="ribbon sale">Sale</div>
												</div>
											@endif
											@php
												$img = json_decode($product->image);
										    @endphp
											<div class="single-item-header">
												<a href="{{route('product-detail',$product->slug)}}">
													<img src="images/product/{{$img['0']}}" alt="" height="250px">
												</a>
											</div>
											<div class="single-item-body">
												<p class="single-item-title">{{$product->name}}</p>
												<p class="single-item-price">
													@if($product->discount != 0)
														<span class="flash-del">{{number_format($product->price)}} đồng</span>
														<span class="flash-sale">{{number_format($product->discount)}} đồng</span>
													@else
														<span class="flash-sale">{{number_format($product->price)}} đồng</span>
													@endif
												</p>
											</div>
											<div class="single-item-caption">
												<a class="add-to-cart pull-left" href="{{route('add-to-cart',$product->id)}}"><i class="fa fa-shopping-cart"></i></a>
												<a class="beta-btn primary" href="{{route('product-detail',$product->slug) }}">Chi tiết <i class="fa fa-chevron-right"></i></a>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							</div>
							<div class="space40">&nbsp;</div>
						</div> <!-- .beta-products-list -->
					</div>
				</div> <!-- end section with sidebar and main content -->


			</div> <!-- .main-content -->
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection
