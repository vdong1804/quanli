@extends('front.layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Chi tiết đơn hàng: <strong>#{{$currentOrder->id}}</strong></h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="{{route('home')}}">Home</a> / <span><a href="{{route('profile-order-detail', $currentOrder->id)}}">Order Details</a></span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br>
    <div class="table-responsive">
        <!-- Shop Products Table -->
        <table class="shop_table beta-shopping-cart-table" cellspacing="0">
            <thead>
                <tr>
                    <th class="product-name">Sản phẩm</th>
                    <th class="product-price">Đơn giá</th>
                    <th class="product-quantity">Số lượng</th>
                    <th class="product-unit">Đơn vị</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orderDetails as $detail)
                    @php
                        $img = json_decode($detail->to_product->image);
                    @endphp
                    <tr class="cart_item">
                        <td class="product-name">
                            <div class="media">
                                <img class="pull-left" height="10" src="images/product/{{$img['0']}}" alt="">
                                <div class="media-body">
                                    <p class="font-large table-title">{{$detail->to_product->name}}</p>
                                </div>
                            </div>
                        </td>

                        <td class="product-price">
                            <span class="amount">{{number_format($detail->product_price)}} đồng</span>
                        </td>

                        <td class="product-quantity">
                            <span class="quantity">{{$detail->product_quantity}}</span>
                        </td>

                        <td class="product-unit">
                            <span class="unit">{{$detail->product_unit}}</span>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6" class="actions">
                        <div class="total">
                            <label for="total">Tổng tiền: </label>
                            <span style="color: #F39C12; font-weight: bold;">{{number_format($currentOrder->amount)}} đồng</span>
                            <br><br>
                            <a href="{{route('profile')}}"><i class="fa fa-arrow-circle-left"></i>Quay lại</a>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
        <!-- End of Shop Table Products -->
        <br>
    </div>
@endsection
