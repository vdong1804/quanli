@extends('front.layout.index')
@section('content')
	<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">{{$current_product->name}}</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{route('home')}}">Trang Chủ</a> / <a href="{{route('product-detail',$current_product->slug)}}">{{$current_product->name}}</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="container">
		<div id="content">
			{{-- products detail --}}
			<div class="row">
				<div class="col-sm-9">
					@if(count($errors)>0)
						<div class="alert alert-danger">
							@foreach($errors->all() as $err)
								{{$err}}<br>
							@endforeach
						</div>
					@endif
					<div class="row">
						@php
							$img = json_decode($current_product->image);
						@endphp
						<div class="col-sm-5">
							<div class="product-detail-image-for">
								@php
									for($i=0; $i<count($img); $i++){
								@endphp
										<img class="myImg" src="images/product/{{$img[$i]}}" alt="">
								@php
									}
								@endphp
							</div>
							<!-- The Modal -->
							<div id="myModal" class="modal">
							  <span class="close">&times;</span>
							  <img class="modal-content" id="img01">
							  <div id="caption"></div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="single-item-body">
								<p class="single-item-title"><h3>{{$current_product->name}}</h3></p>
								<p class="single-item-price">
									@if($current_product->discount != 0)
										<span class="flash-del">{{number_format($current_product->price)}} đồng</span>
										<span class="flash-sale">{{number_format($current_product->discount)}} đồng</span>
									@else
										<span class="flash-sale">{{number_format($current_product->price)}} đồng</span>
									@endif
								</p>
							</div>

							<div class="clearfix"></div>
							{{-- <div class="space20">&nbsp;</div> --}}

							{{-- <div class="single-item-desc">
								<p>{{$current_product->description}}</p>
							</div> --}}
							<div class="space20">&nbsp;</div>

							<p>Số lượng:</p>
							<div class="single-item-options">
								<form action="{{route('add-to-cart',$current_product->id)}}" method="get" accept-charset="utf-8">
									<select class="wc-select" name="form_qty">
										<option disabled="true">Số lượng</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
									{{-- <a class="add-to-cart" href="{{route('add-to-cart',$current_product->id)}}"><i class="fa fa-shopping-cart"></i></a> --}}
									<button class="btn btn-default product-detail-btn" type="submit">
										<i class="fa fa-shopping-cart" style="font-size: 22px;"></i>
									</button>
								</form>
								<div class="clearfix"></div>
							</div>
							<div class="space20">&nbsp;</div>
							<div class="product-detail-image-nav">
								@php
									for($i=0; $i<count($img); $i++){
								@endphp
										<img src="images/product/{{$img[$i]}}" alt="">
								@php
									}
								@endphp
							</div>
						</div>
					</div>
					<div class="space40">&nbsp;</div>
					<div class="woocommerce-tabs">
						<ul class="tabs">
							<li><a href="#tab-description">Mô tả</a></li>
							<li><a href="#tab-reviews">Đánh giá <span class="total-comments"></span></a></li>
						</ul>

						<div class="panel" id="tab-description">
							<p>{{$current_product->description}}</p>
						</div>
						<div class="panel" id="tab-reviews">
							<div class="comments-area">
								@if(Auth::check())
									@if(Session::has('add_comment_success'))
										<div class="alert alert-success">
						                    {{session('add_comment_success')}}
						                </div>
									@endif
								    <div class="show-comment">
										{{-- FrontEndController@get_show_comment --}}
								    </div>
								    <hr>
									<div class="add-comment">
										<form name="fromAddComment" action="" method="post">
											<input type="hidden" name="_token" value="{{csrf_token()}}">
											<textarea id="form_content" name="form_content" required placeholder="Nhập nội dung bình luận..."></textarea>
											<div class="alert alert-danger show-error"></div>
											<div class="alert alert-success show-success"></div>
											<label for="button">&nbsp;</label>
											<button type="submit" class="btn btn-success btn-add-comment">Bình luận</button>
										</form>
									</div>
								@else
								    <div class="show-comment">
										{{-- FrontEndController@get_show_comment --}}
								    </div>
								    <hr>
									<p><a href="{{route('login')}}">Đăng nhập</a> để có thể bình luận cho sản phẩm này.</p>
								@endif
							</div>
						</div>
					</div>
					<div class="space50">&nbsp;</div>

			{{-- Similar products --}}
					<div class="beta-products-list">
						<h4>Sản phẩm tương tự</h4>
						<div class="beta-products-details">
							<p class="pull-left">Đang hiển thị: {{count($similar_products)}} sản phẩm</p>
							<div class="clearfix"></div>
						</div>

						<div class="row">
							<div class="similar-products-data">
								@foreach($similar_products as $similar_product)
								<div class="col-sm-4">
									<div class="single-item">
										@if($similar_product->discount != 0)
											<div class="ribbon-wrapper">
												<div class="ribbon sale">Sale</div>
											</div>
										@endif
										@php
											$img = json_decode($similar_product->image);
										@endphp
										<div class="single-item-header">
											<a href="{{route('product-detail',$similar_product->slug)}}">
												<img src="images/product/{{$img['0']}}" alt="">
											</a>
										</div>
										<div class="single-item-body">
											<p class="single-item-title">{{$similar_product->name}}</p>
											<p class="single-item-price">
												@if($similar_product->discount != 0)
													<span class="flash-del">{{number_format($similar_product->price)}} đồng</span>
													<span class="flash-sale">{{number_format($similar_product->discount)}} đồng</span>
												@else
													<span class="flash-sale">{{number_format($similar_product->price)}} đồng</span>
												@endif
											</p>
										</div>
										<div class="single-item-caption">
											<a class="add-to-cart pull-left" href="{{route('add-to-cart',$similar_product->id)}}"><i class="fa fa-shopping-cart"></i></a>
											<a class="beta-btn primary" href="{{route('product-detail',$similar_product->slug)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div> <!-- .beta-products-list -->
				</div>
			{{-- Best Seller --}}
				<div class="col-sm-3 aside">
					<div class="widget">
						<h3 class="widget-title">Bán chạy nhất</h3>
						<div class="widget-body">
							<div class="beta-sales beta-lists">
								@foreach($best_seller as $sale)
									@php
										$img = json_decode($sale->image);
									@endphp
									<div class="media beta-sales-item">
										<a class="pull-left" href="{{route('product-detail',$sale->slug)}}">
											<img src="images/product/{{$img['0']}}" alt="">
										</a>
										<div class="media-body">
											<a href="{{route('product-detail',$sale->slug)}}">
												{{$sale->name}}
											</a>
											<br>
											@if($sale->discount != 0)
												<span class="beta-sales-price">{{number_format($sale->discount)}} đồng</span>
											@else
												<span class="beta-sales-price">{{number_format($sale->price)}} đồng</span>
											@endif
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div> <!-- best sellers widget -->
					<div class="widget">
						<h3 class="widget-title">Sản phẩm mới</h3>
						<div class="widget-body">
							<div class="beta-sales beta-lists">
								@foreach($new_products as $new_product)
									@php
										$img = json_decode($new_product->image);
									@endphp
									<div class="media beta-sales-item">
										<a class="pull-left" href="{{route('product-detail',$new_product->slug)}}">
											<img src="images/product/{{$img['0']}}" alt="">
										</a>
										<div class="media-body">
											<a href="{{route('product-detail',$new_product->slug)}}">
												{{$new_product->name}}
											</a>
											<br>
											@if($new_product->discount != 0)
												<span class="beta-sales-price">{{number_format($new_product->discount)}} đồng</span>
											@else
												<span class="beta-sales-price">{{number_format($new_product->price)}} đồng</span>
											@endif
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div> <!-- best sellers widget -->
				</div>
			</div>
		</div> <!-- #content -->
	</div> <!-- .container -->
	@php
		$productID = $current_product->id;
	@endphp
@endsection
@section('script')
	<script type="text/javascript">
		$(document).ready(function(){
			// display data comment
			var productID = <?php echo json_encode($productID); ?>;
			displaydata(productID);

			// add new comment
			$('.btn-add-comment').bind('click', function(e){
				e.preventDefault();
				var _token =$("form[name='fromAddComment']").find("input[name='_token']").val();
				var content = $('#form_content').val();
				$.ajax({
					url: 'add-comment/'+productID,
					type: 'POST',
					async: false,
					data: {
						'_token': _token,
						'content':content
					},
					success:function(result){
						// displaydata(productID); //bỏ dòng này vì pusher
						$('#form_content').val('');
						$('.show-error').fadeOut();
						$('.show-success').fadeIn();
						$('.show-success').html(result);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						var responseText = jQuery.parseJSON(jqXHR.responseText);
						$('.show-success').fadeOut();
						$('.show-error').fadeIn();
						$('.show-error').html(responseText["content"]);
				    },
				});
			});

			// comment realtime
			var pusher = new Pusher('82881fdb575ce09055ff', {
	          cluster: 'ap1',
	          encrypted: true
	      	});
	      	var channel = pusher.subscribe('channel-comment-realtime');
	      	channel.bind('App\\Events\\CommentPusherEvent', addCommentPusher);
		});

		function displaydata(productID)
		{
			$.ajax({
				url: 'show-comment/'+productID,
				type: 'GET',
				dataType:'json',
				async: false,
				success: function(results)
				{
					// console.log(results);
					$('.show-comment').html(results['comments']);
					$('.total-comments').html(results['totalComments']);
				}
			});
		}
      	function addCommentPusher(data) {
	        var comment 	  = data.comment;
	        var totalComments = data.totalComments;

	        $('.show-comment').append(comment);
	        $('.total-comments').html(totalComments);
	    }

	    // xử lý zoom ảnh detail
		  // Get the modal
		  var modal = document.getElementById('myModal');
		  // Get the image and insert it inside the modal - use its "alt" text as a caption
		  var img = document.getElementsByClassName('myImg');
		  var modalImg = document.getElementById("img01");
		  var captionText = document.getElementById("caption");
		  for (var i = 0; i < img.length; i++) {
		    img[i].onclick = function(){
		      modal.style.display = "block";
		      modalImg.src = this.src;
		      captionText.innerHTML = this.alt;
		    }
		  }
		  // Get the <span> element that closes the modal
		  var span = document.getElementsByClassName("close")[0];

		  // When the user clicks on <span> (x), close the modal
		  span.onclick = function() {
		      modal.style.display = "none";
		  }
		// Hết xử lý zoom ảnh detail
	</script>
@endsection
