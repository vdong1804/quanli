@extends('front.layout.index')
@section('content')
	{{-- slide --}}
	@include('front.layout.slide')
	{{-- end sile --}}
	<div class="container">
		<div id="content" class="space-top-none">
			<div class="main-content">
				<div class="space40">&nbsp;</div>
				<div class="row">
					<div class="col-sm-12">
						{{-- new products --}}
							@include('front.pages.child_home_pages.new_products')
						{{-- end new products --}}

							<div class="space40">&nbsp;</div>

						{{-- discount product --}}
							@include('front.pages.child_home_pages.discount_products')
						{{-- end discount product --}}
					</div>
				</div> <!-- end section with sidebar and main content -->
			</div> <!-- .main-content -->
		</div> <!-- #content -->
	</div><!-- #container -->
@endsection