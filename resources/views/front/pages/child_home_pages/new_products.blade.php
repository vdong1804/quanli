<div class="beta-products-list discount-product">
	<h4>Sản Phẩm Mới</h4>
	<div class="beta-products-details">
		<p class="pull-left">Đang hiển thị: {{count($new_products)}} sản phẩm</p>
		<div class="clearfix"></div>
	</div>

	<div class="row">
		<div class="new-products-data">
	    	@foreach($new_products as $new_product)
	        	<div class="col-sm-3">
					<div class="single-item">
						@if($new_product->discount != 0)
							<div class="ribbon-wrapper" style="z-index: 999;"><div class="ribbon sale">Sale</div></div>
						@endif
						<div class="single-item-header">
			        		@php
								$img = json_decode($new_product->image);
						    @endphp
				            <a href="{{route('product-detail',$new_product->slug)}}">
				            	<img src="images/product/{{$img['0']}}" alt="" />
				            </a>
				        </div>
				        <div class="single-item-body">
							<p class="single-item-title">{{$new_product->name}}</p>
							<p class="single-item-price">
								@if($new_product->discount == 0)
									<span class="flash-sale">{{number_format($new_product->price)}} đồng</span>
								@else
									<span class="flash-del">{{number_format($new_product->price)}} đồng</span>
									<span class="flash-sale">{{number_format($new_product->discount)}} đồng</span>
								@endif
							</p>
						</div>
						<div class="single-item-caption">
							<a class="add-to-cart pull-left" href="{{route('add-to-cart',$new_product->id)}}"><i class="fa fa-shopping-cart"></i></a>
							<a class="beta-btn primary" href="{{route('product-detail',$new_product->slug)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
							<div class="clearfix"></div>
						</div>
			        </div>
			    </div>
	        @endforeach
	    </div>
    </div>
</div> <!-- .beta-products-list -->