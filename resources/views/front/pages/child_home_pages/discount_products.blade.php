<div class="beta-products-list discount-product">
	<h4>Sản Phẩm Khuyến Mãi</h4>
	<div class="beta-products-details">
		<p class="pull-left">Đang hiển thị: {{count($discount_products)}} sản phẩm</p>
		<div class="clearfix"></div>
	</div>

	<div class="row">
		<div class="discount-data">
	    	@foreach($discount_products as $discount_product)
	        	<div class="col-sm-3">
					<div class="single-item">
						@if($discount_product->discount != 0)
							<div class="ribbon-wrapper" style="z-index: 999;"><div class="ribbon sale">Sale</div></div>
						@endif
						<div class="single-item-header">
			        		@php
								$img = json_decode($discount_product->image);
						    @endphp
						    <a href="{{route('product-detail',$discount_product->slug)}}">
				            	<img src="images/product/{{ $img['0'] }}"/>
				            </a>
				        </div>
				        <div class="single-item-body">
							<p class="single-item-title">{{$discount_product->name}}</p>
							<p class="single-item-price">
								@if($discount_product->discount == 0)
									<span class="flash-sale">{{number_format($discount_product->price)}} đồng</span>
								@else
									<span class="flash-del">{{number_format($discount_product->price)}} đồng</span>
									<span class="flash-sale">{{number_format($discount_product->discount)}} đồng</span>
								@endif
							</p>
						</div>
						<div class="single-item-caption">
							<a class="add-to-cart pull-left" href="{{route('add-to-cart',$discount_product->id)}}"><i class="fa fa-shopping-cart"></i></a>
							<a class="beta-btn primary" href="{{route('product-detail',$discount_product->slug)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
							<div class="clearfix"></div>
						</div>
			        </div>
			    </div>
	        @endforeach
	    </div>
    </div>
</div> <!-- .beta-products-list -->