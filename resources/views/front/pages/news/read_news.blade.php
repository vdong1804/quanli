@extends('front.layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="{{route('home')}}">Home</a> / <span><a href="{{route('show-all-news-frontend')}}">News</a></span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <div id="content-read-news">
            <div class="show-news">
                <div class="read-news-title">
                    <h6 class="inner-title read-news-title">{{$currentNews->title}}</h6>
                    <hr>
                </div>
                <div class="featured-image">
                    <img width="500" height="500" src="images/news/{{$currentNews->image}}" alt="{{$currentNews->image}}" />
                    <hr>
                </div>
                <div class="date-author">
                    <i class="fa fa-calendar"></i> <span>{{$currentNews->created_at->format('H:i, d/m/Y')}}</span>
                    <i class="fa fa-user"></i> <span>{{$currentNews->to_user->name}}</span>
                    <hr>
                </div>
                <div class="read-news-content">
                    {!! $currentNews->content !!}
                </div>
            </div>
        </div>
    </div>
    <hr>
    <br>
@endsection
