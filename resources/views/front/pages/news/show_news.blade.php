@extends('front.layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Tin tức mới:</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="{{route('home')}}">Home</a> / <span><a href="{{route('show-all-news-frontend')}}">News</a></span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br>
    <div class="show-news">
        @foreach($news as $item)
            <div class="news-item">
                <div class="news-item-left">
                    <img width="200" height="350" src="images/news/{{$item->image}}" alt="{{$item->image}}" />
                </div>
                <div class="news-item-right">
                    <div class="news-title">
                        <a href="{{route('read-news-frontend', $item->slug)}}">{{$item->title}}</a>
                    </div>
                    <div class="news-content">
                        {!!
                            str_limit($item->content, $limit = 450, $end = ' [...]')
                        !!}
                    </div>
                    <div class="news-button">
                        <a href="{{route('read-news-frontend', $item->slug)}}">
                            <button class="btn btn-info" type="button">Đọc tiếp...</button>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="news-paginate">
            {{$news->links()}}
        </div>
    </div>
    <br>
@endsection
