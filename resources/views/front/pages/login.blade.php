@extends('front.layout.index')
@section('content')
	<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Đăng nhập</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb">
					<a href="{{route('home')}}">Home</a> / <span><a href="{{route('login')}}">Đăng nhập</a></span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="container">
		<div id="content">
			<form action="{{route('login')}}" method="post" class="beta-form-checkout">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<div class="row">
					<div class="col-sm-3"></div>
					@if(count($errors)>0)
						<div class="alert alert-danger">
							@foreach($errors->all() as $err)
								{{$err}}<br>
							@endforeach
						</div>
					@endif
					@if(Session::has('login_success'))
						<div class="alert alert-success">
							{{ Session::get('login_success') }}
						</div>
					@endif
					@if(Auth::check())
						<div class="col-sm-6">
							Chào bạn <strong>{{Auth::user()->name}}</strong> đã quay trở lại, chúc bạn một ngày tốt lành. <br>
							Hãy xem hôm nay siêu thị có gì mới dành cho bạn nhé.<br>
							--Siêu thị Tân Hường--
						</div>
					@else
						@if(Session::has('register_success'))
							<div class="alert alert-success">
								{{Session::get('register_success')}}
							</div>
						@endif
						@if(Session::has('logout_success'))
							<div class="alert alert-success">
								{{Session::get('logout_success')}}
							</div>
						@endif
						<div class="col-sm-6">
							<h4>Đăng nhập</h4>
							<div class="space20">&nbsp;</div>
							<div class="form-block">
								<label for="email">Địa chỉ e-mail:</label>
								<input type="email" id="id_email" name="form_email" value="{{old('form_email')}}" class="form-control" placeholder="Enter email" required>
							</div>
							<div class="form-block">
								<label for="password">Mật Khẩu</label>
								<input type="password" id="id_password" name="form_password" class="form-control" placeholder="Enter password" required>
							</div>
							{{-- <div class="form-block">
								<label for="remember">&nbsp;</label>
							    <input type="checkbox" class="checkbox_remember" name="form_checkbox"> Ghi nhớ
							</div> --}}
							<div class="form-block">
								<label for="button">&nbsp;</label>
								<button type="submit" class="btn btn-submit">Đăng nhập</button>
								<span>&nbsp;&nbsp;&nbsp;</span>
								<button type="reset" class="btn btn-reset">Nhập lại</button>
							</div>
						</div>
					@endif
					<div class="col-sm-3"></div>
				</div>
			</form>
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection