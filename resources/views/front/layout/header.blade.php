<div id="header">
        <div class="header-top">
            <div class="container">
                <div class="pull-left auto-width-left">
                    <ul class="top-menu menu-beta l-inline">
                        
                    </ul>
                </div>
                <div class="pull-right auto-width-right">
                    <ul class="top-details menu-beta l-inline">
                        @if(Auth::check())
                            <li><a href="{{route('profile')}}"><i class="fa fa-user"></i>{{Auth::user()->name}}</a></li>
                            @if(Auth::user()->role == "admin")
                                <li><a href="{{route('get-dashboard')}}"><i class="fa fa-dashboard"></i>Tới trang quản trị</a></li>
                            @endif
                            @if(Auth::user()->role == "author")
                                <li><a href="{{route('get-author-dashboard')}}"><i class="fa fa-dashboard"></i>Tới trang quản trị</a></li>
                            @endif
                            <li><a href="{{route('logout')}}"><i class="fa fa-sign-out"></i>Đăng xuất</a></li>
                        @else
                            <li><a href="{{route('register')}}"><i class="fa fa-user"></i>Đăng ký</a></li>
                            <li><a href="{{route('login')}}"><i class="fa fa-sign-in"></i>Đăng nhập</a></li>
                        @endif
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div> <!-- .container -->
        </div> <!-- .header-top -->
        <div class="header-body">
            <div class="container beta-relative">
                <div class="pull-left">
                    <a href="{{route('home')}}" id="logo"><img src="images/logo/tanhuong.jpg" style="width:300px; height:100px;" width="350px" height="75px" alt=""></a>
                </div>
                <div class="pull-right beta-components space-left ov">
                    <div class="space10">&nbsp;</div>
                    <div class="beta-comp">
                        <form role="search" method="get" id="searchform" action="{{route('search')}}">
                            <input type="text" name="form_search" value="{{old('form_search')}}" placeholder="Nhập nội dung tìm kiếm..."/>
                            <button class="fa fa-search" type="submit" id="searchsubmit"></button>
                        </form>
                        @if(Session::has('search_error'))
                            <div class="alert alert-danger">
                                {{ Session::get('search_error') }}
                            </div>
                        @endif
                    </div>

                    <div class="beta-comp">
                        <div class="cart">
                            <div class="beta-select">
                                <i class="fa fa-shopping-cart"></i>
                                    Giỏ hàng
                                        @if(Session::has('cart'))
                                            <span class="cart-value">
                                                ({{ Session('cart')->totalQty }})
                                            </span>
                                        @else
                                            (Trống)
                                        @endif
                                <i class="fa fa-chevron-down"></i>
                            </div>
                            <div class="beta-dropdown cart-body">
                            @if(Session::has('cart'))
                             @foreach($products_cart as $product)
                                <div class="cart-item">
                                    <a class="cart-item-delete" href="{{route('delete-item-cart', $product['item']['id'])}}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                    <a href="{{route('add-to-cart',$product['item']['id'])}}" class="cart-item-plus">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <a href="{{route('reduce-item-cart', $product['item']['id'])}}" class="cart-item-minimize">
                                        <i class="fa fa-window-minimize"></i>
                                    </a>
                                    <div class="media">
                                        @php
                                            $img = json_decode($product['item']['image']);
                                        @endphp
                                        <a class="pull-left" href="{{route('product-detail',$product['item']['slug'])}}">
                                            <img src="images/product/{{$img['0']}}" alt="" style="max-width: 100%; height: 50px;">
                                        </a>
                                        <div class="media-body">
                                            <span class="cart-item-title">
                                                {{$product['item']['name'] }}
                                            </span>
                                            <span class="cart-item-options">
                                                Số lượng:
                                                <span class="cart-value">
                                                    {{$product['qty']}} {{$product['unit']}}
                                                </span>
                                            </span>
                                            <span class="cart-item-amount">
                                                Thành tiền:
                                                <span class="cart-value">
                                                    {{number_format($product['price'])}} đồng
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                             @endforeach
                            @endif
                                <div class="cart-caption">
                                    <div class="cart-total text-right">Tổng tiền:
                                    <span class="cart-total-value">
                                        @if(Session::has('cart'))
                                            {{number_format(Session('cart')->totalPrice)}} đồng
                                        @endif
                                    </span></div>
                                    <div class="clearfix"></div>

                                    <div class="center">
                                        <div class="space10">&nbsp;</div>
                                        <a href="{{route('checkout')}}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .cart -->
                    </div>
                </div>
                <div class="clearfix"></div>
            </div> <!-- .container -->
        </div> <!-- .header-body -->
        <div class="header-bottom">
            @include('front.layout.menu')
        </div> <!-- .header-bottom -->
    </div> <!-- #header -->
