<div class="fullwidthbanner-container">
	<div class="fullwidthbanner">
		<div class="bannercontainer" >
	    	<div class="banner">
	    		<div class="slides-data-for">
					@foreach($slides as $slide)
				        <img src="images/slide/{{ $slide->image }}"/>
		        	@endforeach
				</div>
			</div>
			<div class="slides-data-nav">
				@foreach($slides as $slide)
			        <img src="images/slide/{{ $slide->image }}"/>
	        	@endforeach
			</div>
		<div class="tp-bannertimer"></div>
	</div>
</div>