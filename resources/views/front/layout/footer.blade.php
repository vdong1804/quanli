<div id="footer" class="color-div">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="widget">
						<h4 class="widget-title">Facebook</h4>
						<a href="https://www.facebook.com/dienmaytanhuong/"><i class="fa fa-facebook-square" style="font-size: 150px" aria-hidden="true"></i></a>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="widget">
						<h4 class="widget-title">Thông tin</h4>
						<div>
							<ul>
								@foreach($categoriesFooter as $category)
									<li>
										<a href="{{route('category-product',$category->slug)}}">
											<i class="fa fa-chevron-right"></i> {{$category->name}}
										</a>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="widget">
						<h4 class="widget-title">Tin Tức</h4>
						<div>
							<ul>
								@foreach($newsFooter as $news)
									<li>
										<a href="{{route('read-news-frontend',$news->slug)}}">
											<i class="fa fa-chevron-right"></i> {{$news->title}}
										</a>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
				 <div class="col-sm-10">
					<div class="widget">
						<h4 class="widget-title">Liên hệ chúng tôi</h4>
						<div>
							<div class="contact-info">
								<i class="fa fa-map-marker"></i>
								<p>Địa chỉ: Ngã tư Cống Đặng - Bình Phú - Thạch Thất - Hà Nội</p>
								<br>
								<p>Điện thoại: 0966.55.44.22</p>
							</div>
						</div>
					</div>
				  </div>
				</div>
				{{-- <div class="col-sm-3">
					<div class="widget">
						<h4 class="widget-title">Đăng ký nhận tin</h4>
						<form action="javascript:void(0)" method="post">
							<input type="email" name="your_email" placeholder="example@trungquandev.com">
							<button class="pull-right" type="submit">Subscribe <i class="fa fa-chevron-right"></i></button>
						</form>
					</div>
				</div> --}}
			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- #footer -->
	<div class="copyright">
		<div class="container">
			<p class="pull-left">
				Privacy policy. (&copy;) 2017 Nhóm 5 - All rights reserved.
			</p>
			<p class="pull-right pay-options">
				{{-- <a href="javascript:void(0)">
					<i class="fa fa-cc-paypal"></i>
				</a> --}}
				{{-- <a href="javascript:void(0)">
					<i class="fa fa-cc-visa"></i>
				</a> --}}
				{{-- <a href="javascript:void(0)">
					<i class="fa fa-cc-mastercard"></i>
				</a> --}}
			</p>
			<div class="clearfix"></div>
		</div> <!-- .container -->
	</div> <!-- .copyright -->
