<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Quản lý siêu thị Tân Hường</title>
	<link rel="shortcut icon" href="{{ asset('images/favicon/tanhuong.jpg') }}" />
	<base href="{{ asset('') }}">

	<link href="assets/bootstrap-min-only/bootstrap.min.css" rel="stylesheet">
	<link href="assets/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">

	{{-- slick slider --}}
    <link href="build/css/slick.css" rel="stylesheet">
    <link href="build/css/slick-theme.css" rel="stylesheet">
	<link href="build/css/colorbox.css" rel="stylesheet">
	<link href="build/css/responsive.css" rel="stylesheet">
	<link href="build/css/style.css"  rel="stylesheet" title="style">
	{{-- my custom css --}}
	<link href="css/trungquan-frontend.css" rel="stylesheet" title="style">

</head>
<body>
	{{-- header --}}
		@include('front.layout.header')
	{{-- content --}}
		<div class="rev-slider">
			@yield('content')
			<button id="topBtn" title="Go to top"><i class="fa fa-level-up"></i></button>
		</div>
	{{-- footer --}}
		@include('front.layout.footer')

	<!-- include js files -->
	<script src="assets/jquery/dist/jquery.min.js"></script>
	<script src="assets/jquery-migrate/jquery-migrate.min.js"></script>
	<script src="assets/bootstrap-min-only/bootstrap.min.js"></script>
	<script src="assets/pusher-js/dist/web/pusher.min.js"></script>

	<script src="build/js/jquery.colorbox-min.js"></script>
	<script src="build/js/dug.js"></script>
	<script src="build/js/scripts.min.js"></script>
	<script src="build/js/slick.js"></script>

	<!--my customjs-->
	<script src="js/trungquan-frontend.js"></script>

	<script>
	$(document).ready(function($) { //fix memu khi cuộn trang web
		$(window).scroll(function(){
				if($(this).scrollTop()>170){
					$(".header-bottom").addClass('fixNav');
					$("#topBtn").fadeIn();
					$("#cart-menu-li").fadeIn();
				}else{
					$(".header-bottom").removeClass('fixNav');
					$("#topBtn").fadeOut();
					$("#cart-menu-li").fadeOut();
				}
			}
		);
		$("#topBtn").click(function() {
		    $("html, body").animate({scrollTop: 0}, 1000);
		});
	});
	</script>

	@yield('script')

</body>
</html>
