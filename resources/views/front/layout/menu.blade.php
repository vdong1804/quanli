<div class="container">
	<a class="visible-xs beta-menu-toggle pull-right" href="#"><span class='beta-menu-toggle-text'>Menu</span> <i class="fa fa-bars"></i></a>
	<div class="visible-xs clearfix"></div>
	<nav class="main-menu">
		<ul class="l-inline ov">
			<li><a href="{{route('home')}}">Trang chủ</a></li>
			<li><a href="javascript:void(0)">Danh mục đồ điện tử</a>
				<ul class="sub-menu">
					@foreach($bakeryCategories as $category)
						<li>
							<a href="{{route('category-product',$category->slug)}}">
								{{ $category->name }}
							</a>
						</li>
					@endforeach
				</ul>
			</li>
			<li><a href="javascript:void(0)">Danh mục đồ gia dụng</a>
				<ul class="sub-menu">
					@foreach($fruitCategories as $category)
						<li>
							<a href="{{route('category-product',$category->slug)}}">
								{{ $category->name }}
							</a>
						</li>
					@endforeach
				</ul>
			</li>
			<li><a href="{{route('show-all-news-frontend')}}">Tin Tức</a></li>
			<li><a href="{{route('about-us')}}">Giới thiệu</a></li>
			<li><a href="{{route('contact-us')}}">Liên hệ</a></li>
			<li id="cart-menu-li">
				<a href="{{route('checkout')}}" id="cart-menu-a">
					<span class="cart-value cart-in-menu">
						<i class="fa fa-shopping-cart"></i>
						@if(Session::has('cart'))
							({{ Session('cart')->totalQty }})
						@else
							(Trống)
						@endif
					</span>
				</a>
			</li>
		</ul>
		<div class="clearfix"></div>
	</nav>
</div> <!-- .container -->
